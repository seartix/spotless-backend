<?php

namespace App;

use App\Models\AddService;
use App\Models\AvailableDateForCleaner;
use App\Models\Card;
use App\Models\CleanerReview;
use App\Models\Insurance;
use App\Models\SubCondition;
use App\Models\Subscription;
use App\Models\Language;
use App\Models\Coupon;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use App\Models\Order;
use App\Filters\QueryFilter;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string|null $remember_token
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property string $role
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SubCondition[] $forbiddenAddSubConditions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddSubService[] $forbiddenAddSubServices
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-write mixed $forbidden_add_sub_services
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User cleaners()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;

    const CLIENT_ROLE = 'client';
    const CLEANER_ROLE = 'cleaner';
    const USER_MALE = 'male';
    const USER_FEMALE = 'female';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role',
        'subscription_id', 'type', 'fiscal_code',
        'surname', 'gender', 'date_birthday',
        'phone', 'address', 'house_number',
        'city_id', 'province_id', 'code',
        'domofon', 'domofon_text', 'firm_name',
        'post_index', 'avatar', 'description',
        'skill', 'can_iron_clothes','subscriber'
    ];

    protected $rating;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];

    // functions

    public function getAddress()
    {
        switch ($this->type)
        {
            case 'azienda':
                return [
                    'address' => $this->address,
                    'city_id' => $this->city_id,
                    'province_id' => $this->province_id,
                    'post_index' => $this->post_index,
                    'fiscal_code' => $this->fiscal_code,
                ];
            case 'persona':
                return [
                    'address' => $this->address,
                    'house_number' => $this->house_number,
                    'city_id' => $this->city_id,
                    'province_id' => $this->province_id,
                    'code' => $this->code,
                    'domofon' => $this->domofon,
                    'domofon_text' => $this->domofon_text,
                ];
            default: {
                return [
                    'address' => $this->address,
                    'house_number' => $this->house_number,
                    'city_id' => $this->city_id,
                    'province_id' => $this->province_id,
                    'code' => $this->code,
                    'domofon' => $this->domofon,
                    'domofon_text' => $this->domofon_text,
                ];
            }
        }

    }

    public function getRating()
    {
        $ratingAvg = CleanerReview::query()
            ->groupBy('cleaner_id')
            ->where('cleaner_id', $this->id)
            ->where('show', 1)
            ->select([\DB::raw('avg(rating) as avgRating')])
            ->get();
        return $ratingAvg;
    }

    public function getCountRecommendation()
    {
        $recommendationCount = CleanerReview::query()
            ->groupBy('cleaner_id')
            ->where('cleaner_id', $this->id)
            ->count('recommend');
        return $recommendationCount;
    }

    public function getCountSuccessfullyCleanings()
    {
        $cleaningsCount = Order::query()
            ->where('cleaner_id', $this->id)
            ->where('status', 'successed')
            ->count('id');
        return $cleaningsCount;
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class);
    }
    public function getLanguages()
    {
        $languages = $this->languages()
            ->pluck('title');
        return $languages;
    }
    public function card () {
        return $this->hasOne(Card::class);
    }
    public function updateCard ($data) {

        if ($this->card) {
            $this-> card -> update($data);
        } else {
            $card = new Card([
               'user_id' => $this->id,
               'card_number' => $data['card_number'],
               'exp_date' => $data['exp_date'],
               'exp_year' => $data['exp_year'],
               'holder_name' => $data['holder_name'],
               'cvv' => $data['cvv'],
            ]);
            $card -> save();
        }
    }
    // scopes

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeCleaners(Builder $query)
    {
        return $query->where('role', 'cleaner');
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public function scopeClients(Builder $query)
    {
        return $query->where('role', 'client');
    }

    public function scopeFilter(Builder $builder, QueryFilter $queryFilter)
    {
        return $queryFilter->apply($builder);
    }

    // relations

    public function fservices()
    {
        return $this->belongsToMany(AddService::class); // only for admin panel
    }
    public function forbiddenAddServices()
    {
        return $this->belongsToMany(AddService::class);
    }

    public function fconditions()
    {
        return $this->belongsToMany(SubCondition::class, 'user_forbidden_add_sub_condition',
            'user_id', 'forbidden_add_sub_condition_id'); // only for admin panel
    }
    public function forbiddenAddSubConditions()
    {
        return $this->belongsToMany(SubCondition::class, 'user_forbidden_add_sub_condition',
            'user_id', 'forbidden_add_sub_condition_id');
    }

    public function insurances()
    {
        return $this->belongsToMany(Insurance::class, 'user_insurance', 'user_id', 'insurance_id');
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function cleanerOrders()
    {
        return $this->hasMany(Order::class, 'cleaner_id');
    }

    public function availableDates()
    {
        return $this->hasMany(AvailableDateForCleaner::class, 'cleaner_id');
    }

    public function reviews()
    {
        return $this->hasMany(CleanerReview::class, 'cleaner_id');
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'user_coupons');
    }

    public function active_subscriptions()
    {
        return $this->hasMany(ActiveSubscription::class, 'user_id');
    }
    // mutators
    public static function boot()
    {
        parent::boot();
        static::deleting(function ($item) {
            if($item->orders){
                $item->orders()->delete();
            }
            if($item->active_subscriptions){
                $item->active_subscriptions()->delete();
            }
        });
        static::creating(function ($item){
            $item->date_birthday = Carbon::parse($item->date_birthday)->format('Y-m-d H:i:s');
        });
    }

    public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = bcrypt($password);
    }

    public function setDateBirthdayAttribute($date)
    {

        if(strrpos ($date,'/')){
            $this->attributes['date_birthday'] = Carbon::createFromFormat('d/m/Y',$date)->format('Y-m-d');
        }
        else{
            $this->attributes['date_birthday'] = $date;
        }
    }
//    public function setForbiddenAddSubServicesAttribute($value)
//    {
//        $this->forbiddenAddSubServices()->sync($value);
//    }
//
//    public function setForbiddenSubCondition($value)
//    {
//        $this->forbiddenAddSubConditions()->sync($value);
//    }
}
