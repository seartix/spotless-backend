<?php

namespace App\DTOs;

use Carbon\Carbon;
use Illuminate\Support\Collection;

class CreateOrderDto
{
    private $addSubServices;
    /**
     * @var array
     */
    private $surfaces;
    /**
     * @var array
     */
    private $subConditions;
    /**
     * @var array
     */
    private $insurances;
    /**
     * @var bool
     */
    private $buy_clear_kit;
    /**
     * @var int
     */
    private $user_id;
    /**
     * @var int|null
     */
    private $cleaner_id;
    /**
     * @var int
     */
    private $meters;
    /**
     * @var int
     */
    private $hours;
    /**
     * @var float
     */
    private $amount;
    /**
     * @var string
     */
    private $city_id;
    /**
     * @var Carbon
     */
    private $dateTime;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var int
     */
    private $charge_id;
    /**
     * @var string
     */
    private $status;
    /**
     * @var string
     */
    private $paypal_id;
    /**
     * @var int
     */
    private $subscription_id;

    /**
     * CreateOrderDto constructor.
     * @param Collection $data
     * @internal param array $addSubServices
     * @internal param array $surfaces
     * @internal param array $subConditions
     * @internal param array $insurances
     * @internal param bool $buy_clear_kit
     * @internal param int $user_id
     * @internal param int|null $cleaner_id
     * @internal param int $meters
     * @internal param int $hours
     * @internal param float $amount
     * @internal param int $city_id
     * @internal param Carbon $dateTime
     * @internal param string $comment
     * @internal param string $charge_id
     * @internal param string $paypal_id
     * @internal param int $subscription_id
     */
    public function __construct(Collection $data)
    {
        $this->addSubServices = $data['addSubServices'];
        $this->surfaces = $data['surfaces'];
        $this->subConditions = $data['subConditions'];
        $this->insurances = $data['insurances'];
        $this->buy_clear_kit = $data['buy_clear_kit'];
        $this->user_id = $data['user_id'];
        $this->cleaner_id = $data['cleaner_id'];
        $this->meters = $data['meters'];
        $this->hours = $data['hours'];
        $this->amount = $data['amount'];
        $this->city_id = $data['city_id'];
        $carbon= Carbon::createFromFormat('Y-m-d' ,$data['date']);
        $carbon->hour = explode(':', $data['time'])[0];
        $carbon->minute = explode(':', $data['time'])[1];
        $this->dateTime = $carbon;
        $this->comment = $data['comment'];
        $this->charge_id = $data['charge_id'];
        $this->paypal_id = $data['paypal_id'];
        $this->status = 'process';
        $this->subscription_id = $data['subscription_id'];
    }

    /**
     * @return array
     */
    public function getAddSubServices(): array
    {
        return $this->addSubServices;
    }

    /**
     * @param array $addSubServices
     */
    public function setAddSubServices(array $addSubServices)
    {
        $this->addSubServices = $addSubServices;
    }

    /**
     * @return array
     */
    public function getSurfaces(): array
    {
        return $this->surfaces;
    }

    /**
     * @param array $surfaces
     */
    public function setSurfaces(array $surfaces)
    {
        $this->surfaces = $surfaces;
    }

    /**
     * @return array
     */
    public function getSubConditions(): array
    {
        return $this->subConditions;
    }

    /**
     * @param array $subConditions
     */
    public function setSubConditions(array $subConditions)
    {
        $this->subConditions = $subConditions;
    }

    /**
     * @return array
     */
    public function getInsurances(): array
    {
        return $this->insurances;
    }

    /**
     * @param array $insurances
     */
    public function setInsurances(array $insurances)
    {
        $this->insurances = $insurances;
    }

    /**
     * @return bool
     */
    public function isBuyClearKit(): bool
    {
        return $this->buy_clear_kit;
    }

    /**
     * @param bool $buy_clear_kit
     */
    public function setBuyClearKit(bool $buy_clear_kit)
    {
        $this->buy_clear_kit = $buy_clear_kit;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return int|null
     */
    public function getCleanerId(): ?int
    {
        return $this->cleaner_id;
    }

    /**
     * @param int|null $cleaner_id
     */
    public function setCleanerId(int $cleaner_id = null)
    {
        $this->cleaner_id = $cleaner_id;
    }

    /**
     * @return int
     */
    public function getMeters(): int
    {
        return $this->meters;
    }

    /**
     * @param int $meters
     */
    public function setMeters(int $meters)
    {
        $this->meters = $meters;
    }

    /**
     * @return int
     */
    public function getHours(): int
    {
        return $this->hours;
    }

    /**
     * @param int $hours
     */
    public function setHours(int $hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return float
     */
    public function getAmount(): float
    {
        return $this->amount;
    }

    /**
     * @param float $amount
     */
    public function setAmount(float $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return int
     */
    public function getCityId(): int
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCity(int $city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return Carbon
     */
    public function getDateTime(): Carbon
    {
        return $this->dateTime;
    }

    /**
     * @param Carbon $dateTime
     */
    public function setDateTime(Carbon $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return string|null
     */
    public function getChargeId(): ?string
    {
        return $this->charge_id;
    }

    /**
     * @param string $charge_id
     */
    public function setChargeId(string $charge_id = null)
    {
        $this->charge_id = $charge_id;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getPaypalId(): ?string
    {
        return $this->paypal_id;
    }

    /**
     * @param string $paypal_id
     */
    public function setPaypalId(string $paypal_id = null)
    {
        $this->paypal_id = $paypal_id;
    }

    /**
     * @return int|null
     */
    public function getSubscriptionId(): ?int
    {
        return $this->subscription_id;
    }

    /**
     * @param int|null $subscription_id
     */
    public function setSubscriptionId(int $subscription_id = null)
    {
        $this->subscription_id = $subscription_id;
    }
}