<?php

namespace App\DTOs;


use Carbon\Carbon;
use Illuminate\Support\Collection;

class CreateUserDto
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $fiscal_code;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var string
     */
    private $gender;
    /**
     * @var Carbon
     */
    private $date_birthday;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     */
    private $address;
    /**
     * @var string
     */
    private $house_number;
    /**
     * @var int
     */
    private $city_id;
    /**
     * @var int
     */
    private $province_id;
    /**
     * @var int
     */
    private $code;
    /**
     * @var string
     */
    private $domofon;
    /**
     * @var string
     */
    private $domofon_text;
    /**
     * @var string
     */
    private $firm_name;
    /**
     * @var string
     */
    private $post_index;
    /**
     * @var string
     */
    private $role;
    /**
     * @var string
     */
    private $avatar;

    private $isSubscribe;

    /**
     * @return mixed
     */
    public function getisSubscribe()
    {
        return $this->isSubscribe;
    }

    /**
     * @param mixed $isSubscribe
     */
    public function setIsSubscribe($isSubscribe): void
    {
        $this->isSubscribe = $isSubscribe;
    }

    public function __construct(array $data)
    {
        $this->name = $data['name'];
        $this->email = $data['email'];
        $this->password = $data['password'];
        $this->type = $data['type'];
        $this->fiscal_code = $data['fiscal_code'];
        $this->surname = $data['surname'];
        $this->gender = $data['gender'];
        $this->date_birthday = $data['date_birthday'];
        $this->phone = $data['phone'];
        $this->address = $data['address'];
        $this->house_number = $data['house_number'];
        $this->city_id = $data['city_id'];
        $this->province_id = $data['province_id'];
        $this->code = $data['code'];
        $this->domofon = $data['domofon'];
        $this->domofon_text = $data['domofon_text'];
        $this->firm_name = $data['firm_name'];
        $this->post_index = $data['post_index'];
        $this->isSubscribe = $data['isSubscribe'];
        $this->role = $data['role'];
        if ($data['avatar']) {
            $this->avatar = $data['avatar'];
        }
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFiscalCode(): ?string
    {
        return $this->fiscal_code;
    }

    /**
     * @param string $fiscal_code
     */
    public function setFiscalCode(string $fiscal_code)
    {
        $this->fiscal_code = $fiscal_code;
    }

    /**
     * @return string
     */
    public function getSurname(): ?string
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname(string $surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getGender(): ?string
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getDateBirthday(): ?string
    {
        return $this->date_birthday;
    }

    /**
     * @param string $date_birthday
     */
    public function setDateBirthday(string $date_birthday)
    {
        $this->date_birthday = $date_birthday;
    }

    /**
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress(string $address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getHouseNumber(): ?string
    {
        return $this->house_number;
    }

    /**
     * @param string $house_number
     */
    public function setHouseNumber(string $house_number)
    {
        $this->house_number = $house_number;
    }

    /**
     * @return string
     */
    public function getDomofonText(): ?string
    {
        return $this->domofon_text;
    }

    /**
     * @param string $domofon_text
     */
    public function setDomofonText(string $domofon_text = null)
    {
        $this->domofon_text = $domofon_text;
    }

    /**
     * @return string
     */
    public function getFirmName(): ?string
    {
        if ($this->firm_name) {
            return $this->firm_name;
        }
        return null;
    }

    /**
     * @param string $firm_name
     */
    public function setFirmName(string $firm_name = null)
    {
        $this->firm_name = $firm_name;
    }

    /**
     * @return string
     */
    public function getPostIndex(): ?string
    {
        if ($this->post_index) {
            return $this->post_index;
        }
        return null;
    }

    /**
     * @param string $post_index
     */
    public function setPostIndex(string $post_index = null)
    {
        $this->post_index = $post_index;
    }

    /**
     * @return string
     */
    public function getRole(): ?string
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole(string $role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getDomofon(): ?string
    {
        return $this->domofon;
    }

    /**
     * @param string $domofon
     */
    public function setDomofon(string $domofon = null)
    {
        $this->domofon = $domofon;
    }

    /**
     * @return int
     */
    public function getCityId(): ?int
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId(int $city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return int
     */
    public function getProvinceId(): ?int
    {
        return $this->province_id;
    }

    /**
     * @param int $province_id
     */
    public function setProvinceId(int $province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @return int
     */
    public function getCode(): ?int
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode(int $code)
    {
        $this->code = $code;
    }


    /**
     * @return null|string
     */
    public function getAvatar(): ?string
    {
        return $this->avatar;
    }

    /**
     * @param string|null $avatar
     */
    public function setAvatar(string $avatar = null)
    {
        $this->avatar = $avatar;
    }
}
