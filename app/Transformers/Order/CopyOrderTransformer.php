<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 28.11.17
 * Time: 13:05
 */

namespace App\Transformers\Order;


use App\Models\Order;
use App\User;
use League\Fractal\TransformerAbstract;

class CopyOrderTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order) {
        $status = $order->status;
        $address = User::query()->where('id', $order->user_id)->first()->getAddress();
        switch ($status) {
            case 'process': {
                $status = 'processo';
                break;
            }
            case 'accepted': {
                $status = 'accettato';
                break;
            }
            case 'canceled': {
                $status = 'annullato';
                break;
            }
        }
        return [
            'id' => $order->id,

            'realty_type_id' => $order->realty_type_id,
            'realty_type' => $order->realty_type(),
            'city_id' => $order->city_id,

            'comment' => $order->comment,

            'clean_format_date' => $order->date_time->format('Y-m-d'),
            'clean_format_time' => $order->date_time->format('H:i:s'),

            'created_format_date' => $order->created_at->format('Y-m-d'),
            'created_format_time' => $order->created_at->format('H:i:s'),

            'date' => $order->date_time,
            'cleaner' => $order->cleaner(),
            'hours' => $order->hours,
            'meters' => $order->meters,
            'price' => $order->amount,
            'status' => $status,
            'original_status' => $order->status,

            'add_services' => $order->addServices->map(function ($item) {
                return $item->id;
            }),

            'add_surfaces' => $order->addSurfaces->map(function ($item) {
                return $item->id;
            }),

            'sub_conditions' => $order->addSubConditions->map(function ($item) {
                return $item->id;
            }),

            'insurances' => $order->insurances->map(function ($item) {
                return $item->id;
            }),

            'buy_clear_kit' => $order->buy_clear_kit,

            'address' => $address,
        ];
    }

}