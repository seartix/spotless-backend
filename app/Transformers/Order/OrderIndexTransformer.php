<?php

namespace App\Transformers\Order;

use App\Entities\Order;
use League\Fractal\TransformerAbstract;

class OrderIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Order $order)
    {
        $status = $order->getStatus();
        $statusLang = [
            'accepted' => __('home.accepted'),
            'canceled' => __('home.canceled'),
            'process' => __('home.process'),
            'successed' => __('home.successed'),
            'archived' => __('home.archived'),
            'wait for pay' => __('home.w8pay')
        ];
        $status = $statusLang[$status];
        return [
            'id' => $order->getId(),

            'realty_type_id' => $order->getRealtyTypeId(),
            'realty_type' => $order->getRealtyType(),
            'city_id' => $order->getCityId(),

            'comment' => $order->getComment(),

            'clean_format_date' => $order->getDateTime()->format('d.m.Y'),
            'clean_format_time' => $order->getDateTime()->format('H:i'),

            'created_format_date' => $order->getCreatedAt()->format('Y-m-d'),
            'created_format_time' => $order->getCreatedAt()->format('H:i:s'),

            'date' => $order->getDateTime(),
            'cleaner' => $order->getCleaner(),
            'hours' => $order->getHours(),
            'meters' => $order->getMeters(),
            'price' => $order->getAmount(),
            'status' => $status,
            'original_status' => $order->getStatus(),

            'add_services' => $order->getAddServices(),

            'add_surfaces' => $order->getSurfaces(),

            'sub_conditions' => $order->getSubConditions(),

            'insurances' => $order->getInsurances(),

            'buy_clear_kit' => $order->isBuyClearKit(),

            'address' => $order->getAddress(),

            'secret_place_id' => $order->getSecretPlace(),

            'subscription_id' => $order->getSubscriptionId(),

            'is_created_with_subscription' => $order->getisCreatedWithSubscription()
        ];
    }
}
