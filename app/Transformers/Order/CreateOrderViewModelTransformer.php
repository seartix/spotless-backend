<?php

namespace App\Transformers\Order;

use App\ViewModels\CreateOrderViewModel;
use League\Fractal\TransformerAbstract;

class CreateOrderViewModelTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CreateOrderViewModel $createOrderViewModel)
    {
        return [
            'freeServices' => $createOrderViewModel->getFreeServices(),
            'baseServices' => $createOrderViewModel->getBaseServices(),
            'addServices' => $createOrderViewModel->getAddServices(),
            'surfaces' => $createOrderViewModel->getSurfaces(),
            'conditions' => $createOrderViewModel->getConditions(),
            'basicSets' => $createOrderViewModel->getBasicSets(),
            'insurances' => $createOrderViewModel->getInsurances()
        ];
    }
}
