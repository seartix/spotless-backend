<?php

namespace App\Transformers\RealtyTypeMeterHours;

use App\Entities\RealtyTypeMeterHour;
use League\Fractal\TransformerAbstract;

class RealtyTypeMeterHoursIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(RealtyTypeMeterHour $realtyTypeMeterHour)
    {
        return [
            'realty_type' => $realtyTypeMeterHour->getRealtyType(),
            'price' => $realtyTypeMeterHour->getPrice(),
            'meters' => $realtyTypeMeterHour->getMeters(),
            'hours' => $realtyTypeMeterHour->getHours()
        ];
    }
}
