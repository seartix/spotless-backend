<?php

namespace App\Transformers\User;

use App\Entities\User;
use League\Fractal\TransformerAbstract;

class UserIndexTransfromer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id' => $user->getId(),
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'role' => $user->getRole(),
            'type' => $user->getType(),
            'subscription_id' => $user->getSubscriptionId(),
            'insurances' => $user->getInsurances(),
            'fiscal_code' => $user->getFiscalCode(),
            'surname' => $user->getSurname(),
            'gender' => $user->getGender(),
            'date_birthday' => $user->getDateBirthday(),
            'phone' => $user->getPhone(),
            'address' => $user->getAddress(),
            'house_number' => $user->getHouseNumber(),
            'city_id' => $user->getCityId(),
            'province_id' => $user->getProvinceId(),
            'code' => $user->getCode(),
            'domofon' => $user->getDomofon(),
            'domofon_text' => $user->getDomofonText(),
            'firm_name' => $user->getFirmName(),
            'post_index' => $user->getPostIndex(),
            'avatar' => env('APP_URL') . '/upload/' . $user->getAvatar(),
            'description' => $user->getDescription(),
            'is_subscribe' =>$user->getSubscriber(),
            'facebook_id'=>$user->getFacebook(),
            'instagram_id'=>$user->getInstagram(),
            'google_id'=>$user->getGoogle(),
        ];
    }
}
