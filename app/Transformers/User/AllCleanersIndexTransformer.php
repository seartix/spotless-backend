<?php

namespace App\Transformers\User;

use App\User;
use League\Fractal\TransformerAbstract;

class AllCleanersIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(User $user)
    {
        $rating = new \stdClass();
        $rating->avgRating = 0;
        if ($user->getRating()->count() > 0) {
            $rating = $user->getRating()[0];
        }
        return [
            'id'  => $user->id,
            'name' => $user->name,
            'avatar' => env('APP_URL') . '/upload/' . $user->avatar,
            'rating' => $rating,
            'reviews' => $user->reviews,
            'countRecommendation' => $user->getCountRecommendation(),
            'countSuccessfullyCleanings' => $user->getCountSuccessfullyCleanings(),
            'languages' => $user->getLanguages(),
            'can_office' => $user->can_office,
            'can_home' => $user->can_home,
            'skill' => $user->skill,
            'can_iron_clothes' => $user->can_iron_clothes,
        ];
    }
}
