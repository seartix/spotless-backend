<?php

namespace App\Transformers\Insurance;

use App\Entities\Insurance;
use League\Fractal\TransformerAbstract;

class InsuranceIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Insurance $insurance)
    {
        return [
            'id' => $insurance->getId(),
            'title' => $insurance->getTitle(),
            'description' => $insurance->getDescription(),
            'text' => $insurance->getText(),
            'price' => $insurance->getPrice()
        ];
    }
}
