<?php

namespace App\Transformers\Coupon;

use League\Fractal\TransformerAbstract;
use App\Models\Coupon;

class CouponApplyTransform extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Coupon $coupon)
    {
        return [
            'id' => $coupon->id,
            'percent' => $coupon->percent
        ];
    }
}
