<?php

namespace App\Transformers\Content;

use App\ViewModels\ContentIndexViewModel;
use League\Fractal\TransformerAbstract;

class ContentViewModelIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(ContentIndexViewModel $contentIndexViewModel)
    {
        return [
            'contentSteps' => $contentIndexViewModel->getContentSteps(),
            'additionalSteps' => $contentIndexViewModel->getAdditionalContents(),
            'contentButton' => $contentIndexViewModel->getContentButton(),
            'contentPartners' => $contentIndexViewModel->getContentPartners(),
            'aboutUsContent' => $contentIndexViewModel->getAboutUsContent(),
            'contactContent' => $contentIndexViewModel->getContact(),
            'textContents' => $contentIndexViewModel->getTextContents(),
            'postCodes'=> $contentIndexViewModel->getPostCode(),
            'reviews'=> $contentIndexViewModel->getReviews()
        ];
    }
}
