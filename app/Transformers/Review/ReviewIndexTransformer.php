<?php

namespace App\Transformers\Review;

use App\Models\CleanerReview;
use League\Fractal\TransformerAbstract;

class ReviewIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(CleanerReview $cleanerReview)
    {
        return [
            'text' => $cleanerReview->text,
            'user' => $cleanerReview->user->name,
            'cleaner' => $cleanerReview->cleaner->name,
        ];
    }
}
