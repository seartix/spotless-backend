<?php

namespace App\Transformers\AddSubServiceHour;

use App\Entities\AddSubServiceHour;
use League\Fractal\TransformerAbstract;

class AddSubServiceHourTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(AddSubServiceHour $addSubServiceHour)
    {
        return [
            'id' => $addSubServiceHour->getId(),
            'meters' => $addSubServiceHour->getMeters(),
            'hours' => $addSubServiceHour->getHours(),
            'add_service_id' => $addSubServiceHour->getAddServiceId()
        ];
    }
}
