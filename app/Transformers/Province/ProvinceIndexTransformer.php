<?php

namespace App\Transformers\Province;

use App\Entities\Province;
use League\Fractal\TransformerAbstract;

class ProvinceIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Province $province)
    {
        return [
            'id' => $province->getId(),
            'title' => $province->getTitle(),
        ];
    }
}
