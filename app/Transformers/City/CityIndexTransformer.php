<?php

namespace App\Transformers\City;

use App\Entities\City;
use League\Fractal\TransformerAbstract;

class CityIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param City $city
     * @return array
     */
    public function transform(City $city)
    {
        return [
            'id' => $city->getId(),
            'title' => $city->getTitle(),
        ];
    }
}
