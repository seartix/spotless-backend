<?php

namespace App\Transformers\Subscription;

use App\Entities\Subscription;
use League\Fractal\TransformerAbstract;

class SubscriptionIndexTransfromer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Subscription $subscription)
    {
        return [
            'id' => $subscription->getId(),
            'title' => $subscription->getTitle(),
            'description' => $subscription->getDescription(),
            'price' => $subscription->getPrice(),
            'type' => $subscription->getType(),
            'days' => $subscription->getDays(),
            'addServices' => $subscription->getAdServices()->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'price' => $item->price,
                    'hour' => $item->hour,
                    'realty_type_id' => $item->realty_type_id
                ];
            }),
            'addServicesHouse' => $subscription->getAdServices()->filter(function ($item) {
                if ($item->realty_type_id == 1){
                    return true;
                }
            })->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'price' => $item->price
                ];
            }),
            'addServicesOffice' => $subscription->getAdServices()->filter(function ($item) {
                if ($item->realty_type_id == 2){
                    return true;
                }
            })->map(function ($item) {
                return [
                    'id' => $item->id,
                    'title' => $item->title,
                    'price' => $item->price
                ];
            }),
            'additional_list' => $subscription->getAdditionalList(),
            'buy_clear_kit' => $subscription->isBuyClearKit(),
            'frequency'=>$subscription->getFrequency()
        ];
    }
}
