<?php

namespace App\Transformers\RealtyType;

use App\Entities\RealtyType;
use League\Fractal\TransformerAbstract;

class RealtyTypeIndexTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(RealtyType $realtyType)
    {
        return [
            'id' => $realtyType->getId(),
            'title' => $realtyType->getTitle(),
            'icon' => $realtyType->getIcon(),
            'image' => $realtyType->getImage(),
            'description' => $realtyType->getDescription(),
            'label' => $realtyType->getLabel(),
        ];
    }
}
