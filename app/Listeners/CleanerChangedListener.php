<?php

namespace App\Listeners;

use App\Events\CleanerChangedEvent ;
use App\Models\AdminUser;
use App\Notifications\CleanerChangedNotification;
use App\User;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CleanerChangedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CleanerChangedEvent   $event
     * @return void
     */
    public function handle(CleanerChangedEvent  $event)
    {
        $order_id = $event->getOrderId();
        $cleaner = User::whereId($event->getCleanerId())->first();
        $cleaner_name = ($cleaner) ? $cleaner->name : 'non cleaner';
        $cleaner_image = ($cleaner && $cleaner->avatar) ? env('APP_URL') . '/upload/' .$cleaner->avatar : '';
        $user_id = $event->getUserId();
        $order_date = Carbon::createFromFormat('Y-m-d H:i:s', $event->getOrderDate());
        $notification = new CleanerChangedNotification($order_id, $cleaner_name, $order_date->format('d/m/Y h:i'), $cleaner_image);
//        if ($cleaner) {
//            $cleaner->notify($notification);
//        }
        $admin = AdminUser::query()
            ->where('username', 'admin')
            ->first();
        $admin->notify($notification);

        $user = User::query()
            ->where('id', $user_id)
            ->first();

//        if ($cleaner) {
//            $cleaner->notify($notification);
//        }
        $user->notify($notification);
    }
}
