<?php

namespace App\Listeners;

use App\Events\PasswordResetEvent;
use App\Notifications\UserNotification;
use Faker\Provider\DateTime;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class PasswordResetListener
{

    /**
     * Handle the event.
     *
     * @param  PasswordResetEvent  $event
     * @return void
     */
    public function handle(PasswordResetEvent $event)
    {
        $event->user->notify(new UserNotification($event->token));
        DB::table('password_resets')->insert([
            'email' => $event->user->email,
            'token' => $event->token,
            'created_at' => new \DateTime()
        ]);
    }
}
