<?php

namespace App\Listeners;

use App\Events\NewFeedbackEvent;
use App\Models\AdminUser;
use App\Notifications\AdminNewFeedbackNotification;
use App\Notifications\UserNewFeedbackNotification;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewFeedbackListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewFeedbackEvent  $event
     * @return void
     */
    public function handle(NewFeedbackEvent $event)
    {
        $data = $event->getData();

        $admin = AdminUser::query()
        ->where('username', 'Manager ')
        ->first();
        $admin->notify(new AdminNewFeedbackNotification($data));

        $user = new User($data);
        $user->notify(new UserNewFeedbackNotification($data));
    }
}
