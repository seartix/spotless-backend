<?php

namespace App\Listeners;

use App\Events\OrderCreatedEvent;
use App\Models\AdminUser;
use App\Notifications\CleanerOrderCreatedNotification;
use App\Notifications\OrderNotification;
use App\Notifications\PaymentDoneNotification;
use App\Notifications\UserOrderCreatedNotification;
use App\User;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderCreatedEvent $event
     * @return void
     */
    public function handle(OrderCreatedEvent $event)
    {
        $order = $event->getOrder();
        if (isset($order->cleaner_id)) {
            User::query()
                ->where('id', $order->cleaner_id)
                ->first()
                ->notify(new OrderNotification($order));
        } else {
            User::query()
                ->cleaners()
                ->get()
                ->each(function (User $user) use ($order) {
                    $user->notify(new OrderNotification($order));
                });
        }
        $user = User::query()
            ->where('id', $order->user_id)
            ->first();
        $user->notify(new UserOrderCreatedNotification($order));
        $user->notify(new PaymentDoneNotification($order));
        $admin = AdminUser::query()
            ->where('username', 'admin')
            ->first();
        $admin->notify(new OrderNotification($order));
    }
}
