<?php

namespace App\Listeners;

use App\Events\EmployeeCreatedEvent;
use App\Models\AdminUser;
use App\Models\Employer;
use App\Notifications\EmployeeCreatedAdminNotification;
use App\Notifications\EmployeeNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmployeeCreatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmployeeCreatedEvent  $event
     * @return void
     */
    public function handle(EmployeeCreatedEvent $event)
    {
        $employee = $event->getEmployee();
        $employee = Employer::query() -> where('id', $employee->id)->first();
        $employee->notify(new EmployeeNotification());
        $manager = AdminUser::query()
            ->where('username', 'Manager')
            ->first();
        $manager->notify(new EmployeeCreatedAdminNotification());
    }
}
