<?php

use Illuminate\Routing\Router;

Admin::registerHelpersRoutes();
Route::group([
    'prefix'        => config('admin.prefix'),
    'namespace'     => Admin::controllerNamespace(),
    'middleware'    => ['web', 'admin'],
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->resource('base-services', 'BaseServiceController');

    $router->resource('add-services', 'AddServiceController');
    $router->resource('add-services-hours', 'AddServiceHourController');

    $router->resource('free-services', 'FreeServiceController');
    $router->resource('surfaces', 'SurfaceController');
    $router->resource('conditions', 'ConditionController');
    $router->resource('insurances', 'InsuranceController');
    $router->resource('cities', 'CityController');
    $router->resource('provinces', 'ProvinceController');
    $router->resource('basic-sets', 'BasicSetController');
    $router->resource('subscriptions', 'SubscriptionController');
    $router->resource('realty-types', 'RealtyTypeController');
    $router->resource('realty-types-hours', 'RealtyTypeMeterHourController');
    $router->resource('content-steps', 'StepContentController');
    $router->resource('content-additional', 'AdditionalContentController');
    $router->resource('content-button', 'ButtonContentController');
    $router->resource('text-content', 'TextContentController');
    $router->resource('content-partners', 'PartnerContentController');
    $router->resource('about-us', 'AboutUsContentController');
    $router->resource('contact-us', 'ContactPageController');

    $router->resource('cleaner-reviews', 'CleanerReviewsController');

    $router->resource('users', 'UserController');
    $router->resource('cleaners', 'CleanerController');
    $router->resource('employers', 'EmployerController');

    $router->resource('languages', 'LanguageController');
    $router->resource('coupons', 'CouponController');

    $router->resource('orders', 'OrderController');
    $router->resource('places', 'SecretPlaceController');
    $router->resource('text-pages', 'TextPageController');
    $router->resource('faqcategories', 'FAQCategoriesController');
    $router->resource('faqquestions', 'FAQQuestionsController');

    $router->resource('available-date-for-cleaners', 'AvailableDateForCleanerController');
    $router->resource('post-codes', 'PostCodeController');
    $router->resource('articles', 'ArticlesController');
    $router->resource('comments', 'CommentController');
    $router->resource('category', 'CategoryController');
    $router->resource('reviews', 'ReviewController');
    $router->resource('miscalculation', 'MiscalculationController');
    $router->resource('checkbox-services', 'CheckboxServicesController');
    $router->resource('answers-checkbox-services', 'AnswerCheckboxServicesController');
    $router->resource('subscribe-content', 'SubscribeController');
});
