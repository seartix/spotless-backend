<?php

namespace App\Admin\Controllers;

use App\Models\AdditionalContent;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AdditionalContentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Additional contents'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Additional contents'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Additional contents'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AdditionalContent::class, function (Grid $grid) {

            $grid->short_text(__('home.short text'));
            $grid->text(__('home.text'));

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AdditionalContent::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('short_text',__('home.short text'));
            $form->text('text', __('home.text'));
            $form->text('short_title',__('home.short text'));
            $form->text('title', __('home.title'));
            $form->image('icon', __('home.icon'))->removable();;
        });
    }
}
