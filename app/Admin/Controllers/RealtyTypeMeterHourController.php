<?php

namespace App\Admin\Controllers;

use App\Models\RealtyType;
use App\Models\RealtyTypeMeterHour;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class RealtyTypeMeterHourController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.clean_type_meters'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.clean_type_meters'));

            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.clean_type_meters'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(RealtyTypeMeterHour::class, function (Grid $grid) {

            $grid->column('meters', __('home.meters'));
            $grid->column('hours', __('home.hours'));
            $grid->column('add_price', __('home.add_price'));
            $grid->realtyType()->title(__('home.clean type'));

            $grid->disableExport();

            $grid->model()->orderBy('meters', 'asc');
            $grid->model()->orderBy('hours', 'asc');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(RealtyTypeMeterHour::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->number('meters', __('home.meters'))->rules('required|numeric');
            $form->number('hours', __('home.hours'))->rules('required|numeric');
            $form->number('add_price', __('home.add_price'))->rules('required|numeric');

            $form->select('realty_type_id', __('home.clean type'))->options(
                RealtyType::query()->pluck('title', 'id')
            )->rules('required');
        });
    }
}
