<?php

namespace App\Admin\Controllers;

use App\Models\City;
use App\Models\Province;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ProvinceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header( __('home.provinces'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header( __('home.provinces'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header( __('home.provinces'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Province::class, function (Grid $grid) {

            $grid->column('title', __('home.title'));
            $grid->column('city',__('home.city'));

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Province::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|unique:provinces,title,{$id}");

            $form->select('city_id', __('home.city'))->options(
                City::query()->pluck('title', 'id')
            )->rules('required|numeric');
        });
    }
}
