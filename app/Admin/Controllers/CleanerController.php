<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 27.10.17
 * Time: 12:05
 */

namespace App\Admin\Controllers;


use App\Admin\Extensions\CsvExporter;
use App\Models\AddService;
use App\Models\City;
use App\Models\Language;
use App\Models\Province;
use App\Models\SubCondition;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Controllers\ModelForm;
use Illuminate\Routing\Controller;
use Encore\Admin\Layout\Content;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid;

class CleanerController extends Controller
{
    use ModelForm;

    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.cleaner'));

            $content->body($this->grid());
        });
    }

    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.cleaner'));

            $content->body($this->updateForm($id)->edit($id));
        });
    }

    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.cleaner'));

            $content->body($this->form());
        });
    }

    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {
            $grid->model()->where('role', '=', 'cleaner');
            $grid->column('name', __('home.name'));
            $grid->column('email',__('home.email'));

            $grid->exporter(new CsvExporter('export-cleaners-'));

            $grid->model()->orderBy('created_at', 'desc');
        });
    }

    public function update($id)
    {
        return $this->updateForm($id)->update($id);
    }

    protected function updateForm($id)
    {
        return Admin::form(/**
         * @param Form $form
         */
            User::class, function (Form $form) use ($id) {

            $user = User::query()->find($id);

            $form->display('id', 'ID');

            $form->text('name', __('home.name'))->rules('nullable|string');
            $form->text('surname', __('home.surname'))->rules('nullable|string');
            $form->email('email');
            $form->password('password', __('home.password'));

            $form->hidden('role')->value(User::CLEANER_ROLE);

            $form->select('gender', __('home.gender'))
                ->options([
                    User::USER_MALE => __('Male'),
                    User::USER_FEMALE => __('Female')
                ])
                ->rules('nullable');

            $form->multipleSelect('fservices', __('home.forbidden_services'))->options(
                AddService::query()->pluck('title', 'id')
            );
            $form->multipleSelect('fconditions', __('home.forbidden_condition'))->options(
                SubCondition::query()->pluck('title', 'id')
            );
            $form->multipleSelect('languages', __('home.user_languages'))->options(
                Language::query()->pluck('title', 'id')
            );

            $form->text('description',__('home.description'))->rules('nullable|string');
            $form->dateFormat('date_birthday', __('home.date_birthday'))->rules('required')->format('DD/MM/YYYY')->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y');
            });
            $form->text('phone', __('home.phone'))->rules('nullable');
            $form->text('address', __('home.address'))->rules('required|string');
            $form->text('house_number', __('home.house_number'))->rules('nullable|string');
            $form->select('city_id', __('home.city'), 'City')->options(
                City::query()->pluck('title', 'id')
            )->rules('required|numeric');

            $form->select('province_id', __('home.procince_id'))->options(
                Province::query()->pluck('title', 'id')
            )->rules('required|numeric');
//            $form->number('code')->rules('nullable|digits_between:5,5|numeric');
            $form->image('avatar', __('home.avatar'))->help('Le dimensioni di immagine devono essere da 600 x 600 px')->rules('nullable|image')->removable();;
            $form->text('post_index', __('home.code'))->rules('required|numeric|digits_between:0, 9');
            $form->switch('can_home', __('home.can_clean_home'))->rules('nullable');
            $form->switch('can_office', __('home.can_clean_office'))->rules('nullable');
            $form->switch('can_iron_clothes', __('home.can_iron_clothes'))->rules('nullable');

            $form->number('skill', __('home.skill'))->rules('numeric|min:0');
        });
    }

    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->text('name', __('home.name'))
                ->rules('nullable|string');
            $form->text('surname', __('home.surname'))
                ->rules('nullable|string');
            $form->email('email');
            $form->password('password', __('home.password'));

            $form->hidden('role')
                ->value(User::CLEANER_ROLE);

            $form->select('gender', __('home.gender'))
                ->options([
                    User::USER_MALE => __('Male'),
                    User::USER_FEMALE => __('Female')
                ])
                ->rules('nullable');


            $form->multipleSelect('fservices', __('home.forbidden_services'))->options(
                AddService::query()->pluck('title', 'id')
            );
            $form->multipleSelect('fconditions', __('home.forbidden_condition'))->options(
                SubCondition::query()->pluck('title', 'id')
            );


            $form->multipleSelect('languages',
                __('home.user_languages'))
                ->options(
                    Language::query()->pluck('title', 'id')
                );
            $form->text('description', __('home.description'))
                ->rules('nullable|string');
            $form->dateFormat('date_birthday', __('home.date_birthday'))->rules('required')->format('DD/MM/YYYY')->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y');
            });
            $form->text('phone', __('home.phone'))
                ->rules('nullable');
            $form->text('address', __('home.address'))
                ->rules('required|string');
            $form->text('house_number', __('home.house_number'))
                ->rules('nullable|string');
            $form->select('city_id', __('home.city'))
                ->options(
                    City::query()->pluck('title', 'id')
                )->rules('required|numeric');

            $form->select('province_id', __('home.procince_id'))
                ->options(
                    Province::query()->pluck('title', 'id')
                )->rules('required|numeric');
//            $form->number('code')->rules('nullable|digits_between:5,5|numeric');
            $form->image('avatar', __('home.avatar'))
                ->help('Le dimensioni di immagine devono essere da 600 x 600 px')
                ->rules('required|image')->removable();
            $form->text('post_index', __('home.code'))
                ->rules('required|numeric|digits_between:0, 9');
            $form->switch('can_home', __('home.can_clean_home'))
                ->rules('nullable');
            $form->switch('can_office', __('home.can_clean_office'))
                ->rules('nullable');
            $form->switch('can_iron_clothes', __('home.can_iron_clothes'))->rules('nullable');

            $form->number('skill', __('home.skill'))
                ->rules('numeric|min:0');
        });
    }
}
