<?php

namespace App\Admin\Controllers;

use App\Models\AnswerCheckboxService;
use App\Models\AvailableDateForCleaner;
use App\Models\CheckboxService;
use App\Models\PostCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
class AnswerCheckboxServicesController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Answer Checkbox Services'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('Answer Checkbox Services'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('Answer Checkbox Services'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(AnswerCheckboxService::class, function (Grid $grid) {


            $grid->column('id');
            $grid->column('answer',__('home.answer'));
            $grid->column('checkbox_service',__('home.checkbox-services'))->display(function () {
                $service = CheckboxService::find($this->checkbox_service);
                return  $service->question ?? '';
            });
            $grid->column('price',__('home.price'));
//            $grid->date('Date')->display(function ($data) {
//                return Carbon::parse($data)->format('Y-m-d');
//            });
//
//            $grid->column('time_start');
//            $grid->column('time_end');

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AnswerCheckboxService::class, function (Form $form) {

            $form->textarea('answer', __('home.answer'))->rules('required');
            $form->textarea('description', __('home.Description for "i"'));
            $form->select('checkbox_service', __('home.checkbox-services'))->options(function(){
                return CheckboxService::get()->mapWithKeys(function($service){
                    return [
                      $service->id => $service->question
                    ];
                });
            })->rules('required');
            $form->number('price',__('home.price'));


        });
    }
}
