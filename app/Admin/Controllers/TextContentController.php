<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\TextContent;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;

class TextContentController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.text_content'));

            $content->body($this->grid());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TextContent::class, function (Grid $grid) {

            $grid->key(__('home.key'));
            $grid->value(__('home.value'));

            $grid->disableExport();

        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('Text Content'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TextContent::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('key', __('home.key'))->rules('required');
            $form->text('style', __('home.style'));
            $form->text('value', __('home.value'))->rules('required');
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('Text Content'));

            $content->body($this->form());
        });
    }
}
