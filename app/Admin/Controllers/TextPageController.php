<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 19.02.18
 * Time: 12:18
 */

namespace App\Admin\Controllers;

use App\Models\TextPage;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;


class TextPageController extends Controller
{
    use ModelForm;
    use HtmlTrait;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.text'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.text'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.text'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(TextPage::class, function (Grid $grid) {
            $grid->id('ID')->sortable();
            $grid->title(__('home.title'));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(TextPage::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->hidden('title', __('home.title'))->rules('required|string');
            $form->editor('text', __('home.text'))->rules('required|string');
        });
    }

}
