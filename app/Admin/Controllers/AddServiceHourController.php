<?php

namespace App\Admin\Controllers;

use App\Models\AddService;
use App\Models\AddServiceHour;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class AddServiceHourController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Add Service Hours'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Add Service Hour'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Add Service Hour'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AddServiceHour::class, function (Grid $grid) {

            $grid->column('meters', __('home.meters'));
            $grid->column('hours', __('home.hours'));

            $grid->addService()->title(__('home.title'));

            $grid->disableExport();

            $grid->model()->orderBy('meters', 'asc');
            $grid->model()->orderBy('hours', 'asc');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AddServiceHour::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->number('meters', __('home.meters'))->rules('required|numeric|min:1');
            $form->number('hours', __('home.hours'))->rules('required|numeric|min:1');

            $form->select('add_service_id', __('home.add_services'))->options(
                AddService::query()->pluck('title', 'id')
            );
        });
    }
}
