<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\AboutUsContent;
use App\Models\AdditionalContent;
use App\Models\AddService;
use App\Models\AddServiceHour;
use App\Models\AnswerCheckboxService;
use App\Models\Article;
use App\Models\AvailableDateForCleaner;
use App\Models\BaseService;
use App\Models\BasicSet;
use App\Models\ButtonContent;
use App\Models\Category;
use App\Models\CheckboxService;
use App\Models\City;
use App\Models\CleanerReview;
use App\Models\Comment;
use App\Models\Condition;
use App\Models\ContactPage;
use App\Models\Coupon;
use App\Models\Employer;
use App\Models\FAQCategory;
use App\Models\FAQQuestion;
use App\Models\FreeService;
use App\Models\Insurance;
use App\Models\Language;
use App\Models\Miscalculation;
use App\Models\Order;
use App\Models\PartnerContent;
use App\Models\PostCode;
use App\Models\Province;
use App\Models\RealtyType;
use App\Models\RealtyTypeMeterHour;
use App\Models\Review;
use App\Models\StepContent;
use App\Models\SubscribeContent;
use App\Models\Subscription;
use App\Models\Surface;
use App\Models\TextContent;
use App\Models\TextPage;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Columng;
use Encore\Admin\Layout\Content;
use Encore\Admin\Widgets\InfoBox;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(Request $request, Content $content)
    {
        $dates = AvailableDateForCleaner::query()->where('date','<',Carbon::now()->subDay())->get()->toArray();
        if(count($dates) > 0){
           admin_toastr('E da un po che non aggiorni orari lavorativi dei tuoi addetti di pulizia.', 'warning',['timeOut'=>10000]);
        }
        $currUser = $request->user('admin');
        if ($currUser->username == 'Junior Manager') {
            return Admin::content(function (Content $content) {
                $content->header(__('home.Dashboard'));

                $content->row(function ($row) {
                    $row->column(3, new InfoBox(
                            __('home.order'),
                            'check-square',
                            'red',
                            'orders',
                            Order::query()->count())
                    );
                    $row->column(3, new InfoBox(
                        __('home.date'),
                        'clock-o',
                        'aqua',
                        'available-date-for-cleaners',
                        AvailableDateForCleaner::query()->count()
                    ));
                });
            });
        }
        if ($currUser->username == 'Manager') {
            return Admin::content(function (Content $content) {
                $content->header(__('home.Dashboard'));

                $content->row(function ($row) {
                    $row->column(3, new InfoBox(
                            __('home.order'),
                            'check-square',
                            'red',
                            'orders',
                            Order::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.reviews'),
                            'bars',
                            'orange',
                            'cleaner-reviews',
                            CleanerReview::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.base_services'),
                            'bars',
                            'aqua',
                            'base-services',
                            BaseService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.users'),
                            'user',
                            'aqua',
                            'users',
                            User::query()->where('role', '=','client')->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.cleaners'),
                            'user',
                            'aqua',
                            'cleaners',
                            User::query()->where('role', '=','cleaner')->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.add_services'),
                            'bars',
                            'aqua',
                            'add-services',
                            AddService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.add_services_hours'),
                            'bars',
                            'aqua',
                            'add-services-hours',
                            AddServiceHour::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.free_services'),
                            'bars',
                            'aqua',
                            'free-services',
                            FreeService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.surfaces'),
                            'tags',
                            'aqua',
                            'surfaces',
                            Surface::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.conditions'),
                            'comment',
                            'aqua',
                            'conditions',
                            Condition::query()->count())
                    );
//                    $row->column(3, new InfoBox(
//                        __('home.date'),
//                        'clock-o',
//                        'aqua',
//                        'available-date-for-cleaners',
//                        AvailableDateForCleaner::query()->count()
//                    ));
                    $row->column(3, new InfoBox(
                            __('home.basic_sets'),
                            'bookmark',
                            'aqua',
                            'basic-sets',
                            BasicSet::query()->count())
                    );
                });
            });
        }
        if ($currUser->username == 'admin') {
            return Admin::content(function (Content $content) {
                $content->header(__('home.Dashboard'));
                $content->row('<h2>Tutti i servizi</h2>');
                $content->row(function ($row) {
                    $row->column(3, new InfoBox(
                            'programma',
                            'check-square',
                            'aqua',
                            'schedule',
                            'programma')
                    );

                    $row->column(3, new InfoBox(
                            'content',
                            'check-square',
                            'aqua',
                            'subscribe-send',
                            __('home.Send emails'))
                    );

                    $row->column(3, new InfoBox(
                            __('home.order'),
                            'check-square',
                            'red',
                            'orders',
                            Order::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.base_services'),
                            'bars',
                            'aqua',
                            'base-services',
                            BaseService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.add_services'),
                            'bars',
                            'aqua',
                            'add-services',
                            AddService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.add_services_hours'),
                            'bars',
                            'aqua',
                            'add-services-hours',
                            AddServiceHour::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.free_services'),
                            'bars',
                            'aqua',
                            'free-services',
                            FreeService::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.clean_types'),
                            'building',
                            'aqua',
                            'realty-types',
                            RealtyType::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.coupons'),
                            'bars',
                            'aqua',
                            'coupons',
                            Coupon::query()->count())
                    );
                    $row->column(3, new InfoBox(
                        __('home.checkbox-services'),
                        'bars',
                        'aqua',
                        'checkbox-services',
                        CheckboxService::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                        __('home.answers-checkbox-services'),
                        'bars',
                        'aqua',
                        'answers-checkbox-services',
                        AnswerCheckboxService::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                            __('home.clean_type_meters'),
                            'building',
                            'aqua',
                            'realty-types-hours',
                            RealtyTypeMeterHour::query()->count())
                    );

                    $row->column(3, new InfoBox(
                            __('home.subscribe'),
                            'building',
                            'aqua',
                            'subscribe-content',
                            SubscribeContent::query()->count())
                    );
                });

                $content->row('<h2>Contenuto (blog, articoli, ecc.)</h2>');
                $content->row(function ($row) {
                    $row->column(3, new InfoBox(
                            __('home.reviews'),
                            'bars',
                            'orange',
                            'cleaner-reviews',
                            CleanerReview::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.additional'),
                            'bars',
                            'aqua',
                            'content-additional',
                            AdditionalContent::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.button'),
                            'bars',
                            'aqua',
                            'content-button',
                            ButtonContent::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.text_content'),
                            'bars',
                            'aqua',
                            'text-content',
                            TextContent::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.partners'),
                            'bars',
                            'aqua',
                            'content-partners',
                            PartnerContent::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.about'),
                            'bars',
                            'aqua',
                            'about-us',
                            AboutUsContent::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.contact'),
                            'bars',
                            'aqua',
                            'contact-us',
                            ContactPage::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.faq_categories'),
                            'bars',
                            'aqua',
                            'faqcategories',
                            FAQCategory::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.faq_question'),
                            'bars',
                            'aqua',
                            'faqquestions',
                            FAQQuestion::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.text'),
                            'bars',
                            'aqua',
                            'text-pages',
                            TextPage::query()->count())
                    );
                    $row->column(3, new InfoBox(
                        __('home.articles'),
                        'bars',
                        'aqua',
                        'articles',
                        Article::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                        __('home.category'),
                        'bars',
                        'aqua',
                        'category',
                        Category::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                        __('home.comments'),
                        'bars',
                        'aqua',
                        'comments',
                        Comment::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                        __('home.reviews-site'),
                        'bars',
                        'aqua',
                        'reviews',
                        Review::query()->count()
                    ));
                });



                $content->row('<h2>Altri</h2>');
                $content->row(function ($row) {



                    $row->column(3, new InfoBox(
                            __('home.surfaces'),
                            'tags',
                            'aqua',
                            'surfaces',
                            Surface::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.conditions'),
                            'comment',
                            'aqua',
                            'conditions',
                            Condition::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.insurance'),
                            'credit-card',
                            'aqua',
                            'insurances',
                            Insurance::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.users'),
                            'user',
                            'aqua',
                            'users',
                            User::query()->where('role', '=','client')->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.cleaners'),
                            'user',
                            'aqua',
                            'cleaners',
                            User::query()->where('role', '=','cleaner')->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.employers'),
                            'user',
                            'aqua',
                            'employers',
                            Employer::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.user_languages'),
                            'tags',
                            'aqua',
                            'languages',
                            Language::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.cities'),
                            'institution',
                            'aqua',
                            'cities',
                            City::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.provinces'),
                            'institution',
                            'aqua',
                            'provinces',
                            Province::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.basic_sets'),
                            'bookmark',
                            'aqua',
                            'basic-sets',
                            BasicSet::query()->count())
                    );
                    $row->column(3, new InfoBox(
                            __('home.subscriptions'),
                            'file',
                            'aqua',
                            'subscriptions',
                            Subscription::query()->count())
                    );


                    $row->column(3, new InfoBox(
                            __('home.step'),
                            'bars',
                            'aqua',
                            'content-steps',
                            StepContent::query()->count())
                    );

                    $row->column(3, new InfoBox(
                            __('home.places'),
                            'bars',
                            'aqua',
                            'places',
                            ContactPage::query()->count())
                    );



                    $row->column(3, new InfoBox(
                        __('home.date'),
                        'clock-o',
                        'grey',
                        'available-date-for-cleaners',
                        AvailableDateForCleaner::query()->count()
                    ));
                    $row->column(3, new InfoBox(
                        __('home.post-code'),
                        'bars',
                        'aqua',
                        'post-codes',
                        PostCode::query()->where('approved',true)->count()
                    ));

                    $row->column(3, new InfoBox(
                        __('home.miscalculation'),
                        'bars',
                        'aqua',
                        'miscalculation',
                        Miscalculation::query()->count()
                    ));

                });

            });
        }
    }
}
