<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\CsvExporter;
use App\Models\AddService;
use App\Models\City;
use App\Models\Insurance;
use App\Models\Order;

use App\Models\RealtyType;
use App\Models\Subscription;
use App\Models\Surface;
use App\User;
use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class OrderController extends Controller
{
    use ModelForm;
    use HtmlTrait;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.order'));

            $content->body($this->grid());
        });
    }

    public function update($id)
    {
        return $this->updateForm($id)->update($id);
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.order'));

            $content->body($this->updateForm($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.order'));
            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $self = $this;

        return Admin::grid(Order::class, function (Grid $grid) use ($self) {

            $grid->filter(function($filter){
                $filter->disableIdFilter();
                // Remove the default id filter
                $filter->equal('status')->select(
                    [
                        'archived' => 'Archiviato',
                        'wait for pay' => 'Ordine non completato',
                        'canceled' => 'Cancellato',
                        'accettato' => 'Accettato',
                        'successed' => 'Avere successo',
                        'process' => 'In corso'
                    ]);


            });
            $grid->id()->sortable();

            $grid->status(__('home.status'))->display(function ($value) use ($self) {
                return __('home.'.$value);
            });

            $grid->city()->title(__('home.city'));

            $grid->realty_type()->title(__('home.clean type'));

            $grid->date_time(__('home.clear_datetime'))->display(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            })->sortable();
            $grid->created_at('home.created At')->display(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            })->sortable();
            $grid->meters(__('home.meters'))->sortable();
            $grid->hours(__('home.hours'))->sortable();

            $grid->amount(__('home.price'))->sortable();

            $grid->addServices(__('home.add_services'))->pluck('title')->display(function ($value) use ($self) {
                return $self->html_list($value);
            });

            $grid->addSurfaces(__('home.surfaces'))->pluck('title')->display(function ($value) use ($self) {
                return $self->html_list($value);
            });

            $grid->addSubConditions(__('home.add_sub_conditions'))->pluck('title')->display(function ($value) use ($self) {
                return $self->html_list($value);
            });

            $grid->buy_clear_kit(__('home.clear_kit'))->display(function ($value) use ($self) {
                return $self->html_checkbox($value);
            });

            $grid->user(__('home.user'))->display(function ($user) {
                return $user['name'];
            });

            $grid->cleaner(__('home.cleaner'))->display(function ($cleaner) {
                if ($cleaner) {
                    return $cleaner['name'];
                }

                return __("home.Not selected");
            });

            $grid->insurance(__('home.insurance'))->display(function ($insurance) {
                if ($insurance) {
                    return $insurance['title'];
                }

                return __("home.Not selected");
            });

            $grid->comment(__('home.comment'));

            $grid->model()->orderBy('created_at', 'desc');

            $grid->exporter(new CsvExporter('export-order-'));
            $grid->disableCreation();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function updateForm($id)
    {
        return Admin::form(Order::class, function (Form $form) use ($id) {


            $form->display('id', 'ID');

            $form->textarea('address', 'Indirizzo')->rules('string|nullable');
            $form->switch('buy_clear_kit', __('home.clear_kit'))
                ->rules("required");

            $form->select('realty_type_id', __('home.clean type'))
                ->rules('numeric')
                ->options(
                    RealtyType::query()->pluck('title', 'id')
                );
            $form->select('user_id', __('home.user'))->options(
                User::query()->where('role', 'client')
                    ->get()
                    ->reduce(function ($res, $item) {
                            $res->put($item->id, $item->name);
                            return $res;
                        },
                        collect([ 0 => ' '])));
            $form->textarea('comment', __('home.comment'))->rules("string|nullable");
            $form->number('amount', __('home.amount'))->rules('required|numeric');

            $form->dateFormat('date_time', __('home.date'))->rules('required')->format('DD/MM/YYYY HH:mm:ss')->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            });

            $form->number('meters', __('home.meters'))->rules('required|numeric|min:0');
            $form->number('hours', __('home.hours'))->rules('required|numeric|min:0');
            $form->select('cleaner_id', __('home.cleaner'))->options(
                User::query()->cleaners()->get()->reduce(function ($res, $item) {
                    $res->put($item->id, $item->name);
                    return $res;
                }, collect([ 0 => ' ']))
            )->default(null)->placeholder(__('home.cleaner'));

            $form->multipleSelect('insurances', __('home.insurance'))->options(
                Insurance::query()->pluck('title', 'id')
            );
            $form->multipleSelect('addServices', __('home.add_services'))->options(
                AddService::query()->pluck('title', 'id')
            );
            $form->multipleSelect('addSurfaces', __('home.surface'))->options(
                Surface::query()->pluck('title', 'id')
            );
            $form->select('city', __('home.city'))->options(
                City::query()->pluck('title', 'id')
            );

            $form->select('subscription_id', __('home.subscription'))->options(
                Subscription::query()->get()->reduce(function ($res, $item) {
                    $res->put($item->id, $item->title);
                    return $res;
                }, collect([ 0 => ' ']))
            )->default(null)->rules('nullable|numeric');

            $form->select('status', __('home.status'))->options([
                'accepted' => __('home.accepted'),
                'canceled' => __('home.canceled'),
                'process' => __('home.process'),
                'successed' => __('home.successed'),
                'archived' => __('home.archived'),
                'wait for pay' => __('home.w8pay'),
            ]);
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Order::class, function (Form $form) {
        });
    }
}
