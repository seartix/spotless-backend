<?php

namespace App\Admin\Controllers;

use App\Models\Coupon;

use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CouponController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.coupons'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.coupons'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.coupons'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Coupon::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('code', __('home.code'));
            $grid->column('date_start', __('home.date_start'))->display(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            });
            $grid->column('date_end', __('home.date_end'))->display(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            });
            $grid->column('percent', __('home.percent'));
            $grid->column('price_from', __('home.price'));
            $grid->column('count_using', __('home.count_using'));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Coupon::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('code', __('home.coupon_code'));
            $form->dateFormat('date_start', __('home.date_start'))->format('DD/MM/YYYY HH:mm:ss')->width(160)->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            });
            $form->dateFormat('date_end', __('home.date_end'))->format('DD/MM/YYYY HH:mm:ss')->width(160)->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y H:i:s');
            });
            $form->number('count_using', __('home.count_using'));
            $form->number('price_from', __('home.start_price'));
            $form->number('percent', __('home.percent'));
        });
    }
}
