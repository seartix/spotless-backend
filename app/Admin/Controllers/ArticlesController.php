<?php

namespace App\Admin\Controllers;

use App\Models\AvailableDateForCleaner;
use App\Models\Article;
use App\Models\Category;
use App\Transformers\Content\CategoriesTransformer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;

class ArticlesController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Encore\Admin\Layout\Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Article'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Article'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Article'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(Article::class, function (Grid $grid) {

            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();
                // Add a column filter
                $filter->like('title', __('home.title'));

            });
            $grid->column('id','id');
            $grid->column('title',__('home.title'));
            $grid->column('updated_at',__('home.updated_at'));

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        return Admin::form(Article::class, function (Form $form) {
            $options = [];
            $categories = Category::where('parent_id',0)->with('children')->get();
            foreach ($categories as $category){
                $options[$category->id] = $category->name;
                foreach($category->children as $subcategory){
                    $options[$subcategory->id] = '— '.$subcategory->name;
                }
            }
            $form->select('category_id',__('home.category'))->options($options);
            $form->text('title', __('home.title'))->rules('required');
            $form->text('subtitle', __('home.subtitle'))->rules('required');
            $form->textarea('content', __('home.content'))->rules('required|min:1');
            $form->file('media',__('home.media'))->move('storage',str_random(10).'.jpg')->removable();
            $form->radio('approved',__('home.show'))->options([1 => 'Show', 0=> 'Hide'])->default(0);
            $form->radio('on_main_page',__('home.on main page'))->options([1 => 'yes', 0=> 'no'])->default(0);

        });
    }
}
