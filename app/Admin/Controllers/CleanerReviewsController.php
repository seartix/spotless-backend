<?php

namespace App\Admin\Controllers;

use App\Models\CleanerReview;
use App\User;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class CleanerReviewsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Cleaner Reviews'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Cleaner Review'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Cleaner Review'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CleanerReview::class, function (Grid $grid) {

            $grid->user(__('home.user'))->name('User');
            $grid->cleaner(__('home.cleaner'))->name('Cleaner');
            $grid->column('text', __('home.text'));
            $grid->column('show', __('home.show'))->switch();

            $grid->disableExport();

            $grid->model()->orderBy('created_at', 'DESC');
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(CleanerReview::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->select('user_id', __('home.user'))->options(
                User::query()
                    ->where('role', 'client')
                    ->pluck('name', 'id')
            )->rules('required|numeric|exists:users,id');

            $form->select('cleaner_id', __('home.cleaner'))->options(
                User::query()
                    ->where('role', 'cleaner')
                    ->pluck('name', 'id')
            )->rules('required|numeric|exists:users,id');

            $form->switch('show', __('home.show'));

            $form->number('rating', __('home.rating'));

            $form->textarea('text', __('home.text'))->rules('required|string');

            $form->switch('recommend', __('home.recommend'));


        });
    }
}
