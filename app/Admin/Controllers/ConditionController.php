<?php

namespace App\Admin\Controllers;

use App\Models\Condition;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class ConditionController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.conditions'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.conditions'));

            $content->body($this->updateForm($id)->edit($id));
        });
    }

    public function update($id)
    {

        return $this->updateForm($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.conditions'));

            $content->body($this->createForm());
        });
    }

    public function store()
    {
        return $this->createForm()->store();
    }

    public function destroy($id)
    {
        $this->form($id)->destroy($id);
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Condition::class, function (Grid $grid) {

            $grid->column('title', __('home.title'));
            $grid->subConditions(__('home.add_sub_conditions'))->pluck('title')->implode(', ');

            $grid->disableExport();

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function createForm()
    {
        return Admin::form(Condition::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules('required|string|unique:conditions,title');
            $form->image('icon', __('home.icon'))->rules('required|file|image')->removable();;

            $form->hasMany('subConditions', __('home.add_sub_conditions') , function (Form\NestedForm $form) {
                $form->text('title')->rules('required|string');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function updateForm($id)
    {
        return Admin::form(Condition::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:conditions,title,{$id}");
            $form->image('icon', __('home.icon'))->rules('required|file|image')->removable();;

            $form->hasMany('subConditions',__('home.add_sub_conditions'), function (Form\NestedForm $form) {
                $form->text('title',__('home.title'))->rules('required|string');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id)
    {
        return Admin::form(Condition::class, function (Form $form) use ($id) {

        });
    }
}
