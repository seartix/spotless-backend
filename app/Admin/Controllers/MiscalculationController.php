<?php

namespace App\Admin\Controllers;

use App\Models\Miscalculation;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;

class MiscalculationController extends Controller
{
    use ModelForm;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Miscalculations'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Miscalculations'));

            $content->body($this->form());
        });
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Miscalculations'));

            $content->body($this->form($id)->edit($id));
        });
    }


    protected function grid()
    {
        $image_formats = ['png', 'jpg', 'gif', 'jpeg', 'tiff', 'bmp'];
        return Admin::grid(Miscalculation::class, function (Grid $grid) use ($image_formats) {

            $grid->column('big_house',__('home.type'))->display(function () {
                if ($this->big_house) {
                    return '<span><?xml version="1.0" encoding="UTF-8"?>
<svg width="25px" height="25px" enable-background="new 0 0 687.779 687.779" version="1.1" viewBox="0 0 687.779 687.779" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
	<path d="m345.92 311.72l-166 137v173.1h332v-172.6l-166-137.5zm-2.499 244.1h-61.5v-57.5h61.5v57.5zm0-65h-61.5v-57.5h61.5v57.5zm69.499 65h-61.5v-57.5h61.5v57.5zm0-65h-61.5v-57.5h61.5v57.5z"/>
	<polygon points="345.96 65.962 183.51 197.86 183.51 137.29 123.39 137.29 123.39 247.36 0 348.79 38.25 395.32 121.95 326.47 121.95 551.83 155.16 551.83 155.16 543.19 155.16 428.17 345.96 270.67 345.96 270.67 426.06 337.54 426.06 288.85 467.82 288.85 467.82 371.92 536.58 428.71 536.58 543.19 536.58 551.83 568.62 551.83 568.62 328.72 649.53 395.32 687.78 348.79"/>
</svg>
</span>';
                } else {
                    return '<span><?xml version="1.0" encoding="UTF-8"?>
<svg enable-background="new 0 0 58 58" width="25px" version="1.1" viewBox="0 0 58 58" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">
	<path d="m21 14h-5v-5c0-0.553-0.448-1-1-1s-1 0.447-1 1v5h-5c-0.552 0-1 0.447-1 1s0.448 1 1 1h5v5c0 0.553 0.448 1 1 1s1-0.447 1-1v-5h5c0.552 0 1-0.447 1-1s-0.448-1-1-1z"/>
	<path d="m49 14h-12c-0.552 0-1 0.447-1 1s0.448 1 1 1h12c0.552 0 1-0.447 1-1s-0.448-1-1-1z"/>
	<path d="m30 0h-30v58h58v-58h-28zm-28 2h26v26h-26v-26zm0 54v-26h26v26h-26zm54 0h-26v-26h26v26zm-26-28v-26h26v26h-26z"/>
	<path d="m37 47h12c0.552 0 1-0.447 1-1s-0.448-1-1-1h-12c-0.552 0-1 0.447-1 1s0.448 1 1 1z"/>
	<path d="m37 41h12c0.552 0 1-0.447 1-1s-0.448-1-1-1h-12c-0.552 0-1 0.447-1 1s0.448 1 1 1z"/>
	<path d="M20.707,37.293c-0.391-0.391-1.023-0.391-1.414,0L15,41.586l-4.293-4.293c-0.391-0.391-1.023-0.391-1.414,0   s-0.391,1.023,0,1.414L13.586,43l-4.293,4.293c-0.391,0.391-0.391,1.023,0,1.414C9.488,48.902,9.744,49,10,49   s0.512-0.098,0.707-0.293L15,44.414l4.293,4.293C19.488,48.902,19.744,49,20,49s0.512-0.098,0.707-0.293   c0.391-0.391,0.391-1.023,0-1.414L16.414,43l4.293-4.293C21.098,38.316,21.098,37.684,20.707,37.293z"/>
</svg>
</span>';
                }

            });
            $grid->column('email',__('home.email'));

            $grid->column('zip_code',__('home.zip_code'));
            $grid->column('text',__('home.text'))->display(function (){
                return nl2br($this->text);
            });
            $grid->column('file',__('home.media'))->display(function () use ($image_formats) {
                $name = explode('/', $this->file);
                $name = $name[count($name) - 1];
                    return '<a href="' . env('APP_URL') . $this->file . '" target="_blank">' . $name . '</a>';
            });
            $grid->disableExport();
            $grid->disableActions();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Miscalculation::class, function (Form $form) {

            $form->display('email', __('home.email'));
            $form->display('zip_code', __('home.Post code'));
            $form->display('text', __('home.text'));
        });
    }
}
