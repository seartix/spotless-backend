<?php

namespace App\Admin\Controllers;



use App\Models\Category;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;

class CategoryController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Encore\Admin\Layout\Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Article category'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Article category'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Article category'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(Category::class, function (Grid $grid) {

            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();
                // Add a column filter
                $filter->like('name', __('home.title'));

            });
            $grid->column('id','id');
            $grid->column('name',__('home.name'));
            $grid->column('updated_at',__('home.updated_at'));

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {

        return Admin::form(Category::class, function (Form $form) {
            $categories = Category::where('parent_id',0)->get()->mapWithKeys(function($category){
                return [
                  $category->id=>$category->name
                ];
            })->toArray();
            $categories[0] = 'No parents';
            ksort($categories);
            $form->select('parent_id',__('home.category'))->options($categories);
            $form->text('name', __('home.title'))->rules('required');


        });
    }
}
