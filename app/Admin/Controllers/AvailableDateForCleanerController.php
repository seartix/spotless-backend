<?php

namespace App\Admin\Controllers;

use App\Models\AvailableDateForCleaner;

use App\User;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;

class AvailableDateForCleanerController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Available Date For Cleaners'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Available Date For Cleaners'));
            $content->body($this->form($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Available Date For Cleaners'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(AvailableDateForCleaner::class, function (Grid $grid) {

            $grid->model()->orderBy('date', 'desc');
            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();
                // Add a column filter
                $filter->like('cleaner.name', 'Nome');

            });
            $grid->cleaner()->name(__('home.name'));

            $grid->column('date',__('home.date'))->display(function($item){
                return Carbon::parse($item)->format('d/m/Y');
            });

            $grid->column('time_start','Ora di inizio');
            $grid->column('time_end','Ora di fine');

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(AvailableDateForCleaner::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->select('cleaner_id', __('home.cleaner'))->options(
                User::query()->cleaners()->pluck('name', 'id')
            )->rules("required|exists:users,id");


           $form->dateFormat('date', __('home.date'))->rules("required")->format("DD/MM/YYYY")->formatting(function($item){
               return Carbon::parse($item)->format('d/m/Y');
           });

            $form->time('time_start', __('home.time_start'))->rules("required");
            $form->time('time_end', __('home.time_end'))->rules("required|after:time_start");

        });
    }
}
