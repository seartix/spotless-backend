<?php

namespace App\Admin\Controllers;

use App\Models\AvailableDateForCleaner;
use App\Models\CheckboxService;
use App\Models\PostCode;
use App\Models\RealtyType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
class CheckboxServicesController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Checkbox Services'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Checkbox Services'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Checkbox Services'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(CheckboxService::class, function (Grid $grid) {

          //  $grid->model()->orderBy('', 'desc');

            $grid->column('id');
            $grid->column('question',__('home.question'));
            $grid->column('type',__('home.type'));
            $grid->column('clean_type',__('home.clean-type'))->display(function(){
                $cleanType = RealtyType::find($this->clean_type);
                return  $cleanType->title ?? '';
            });
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(CheckboxService::class, function (Form $form) {

            $form->textarea('question', __('home.question'))->rules('required');
            $form->textarea('description', __('home.description'));
            $form->select('type', __('home.type'))->options(['radio'=>'radio','checkbox'=>'checkbox'])->rules('required');
            $form->select('clean_type', __('home.clean-type'))->options(function(){
                return RealtyType::get()->mapWithKeys(function($cleanType){
                    return [
                        (int)$cleanType->id => $cleanType->title
                    ];
                });
            })->rules('required');
        });
    }
}
