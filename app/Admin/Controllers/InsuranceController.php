<?php

namespace App\Admin\Controllers;

use App\Models\Insurance;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class InsuranceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Insurances'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Insurance'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Insurance'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Insurance::class, function (Grid $grid) {

            $grid->column('title', __('home.title'));
            $grid->column('description', __('home.description'));

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Insurance::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:insurances,title,{$id}");
            $form->textarea('description', __('home.description'))->rules('required|string');
            $form->text('text', __('home.text'))->rules('required|string');
            $form->number('price', __('home.price'))->rules('required|numeric|min:0.01');

        });
    }
}
