<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
class ReviewController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.reviews'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.reviews'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.reviews'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(Review::class, function (Grid $grid) {

            $grid->column('name',__('home.name'));
            $grid->column('text',__('home.text'));
            $grid->column('stars',__('home.stars'));
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Review::class, function (Form $form) {

            $form->text('name', __('home.name'))->rules('required');
            $form->textarea('text', __('home.comment'))->rules('required');
            $form->file('avatar',__('home.avatar'))->move('storage',str_random(10).'.jpg')->removable();;
            $form->number('stars', __('home.stars'))->rules('required|integer|min:0|max:5');

        });
    }
}
