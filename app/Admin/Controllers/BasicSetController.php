<?php

namespace App\Admin\Controllers;

use App\Models\BasicSet;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BasicSetController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Basic Sets'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Basic Set'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Basic Set'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(BasicSet::class, function (Grid $grid) {

            $grid->column(__('home.title'));
            $grid->column('setsText', __('home.text'));

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(BasicSet::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:basic_sets,title,{$id}");
            $form->textarea('setsText', __('home.sets_text'))->rules('required');
            $form->image('icon',__('home.icon'))->rules('required')->removable();;

        });
    }
}
