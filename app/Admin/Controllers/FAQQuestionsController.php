<?php

namespace App\Admin\Controllers;

use App\Models\FAQCategory;
use App\Models\FAQQuestion;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class FAQQuestionsController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.faq_question'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.faq_question'));

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.faq_question'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(FAQQuestion::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->column('title',__('home.title'));
            $grid->column('answer',__('home.answer'));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(FAQQuestion::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('title', __('home.title'))->rules('required');
            $form->textarea('answer', __('home.answer'))->rules('required');
            $form->select('category_id', __('home.faq_categories'))->options(
                FAQCategory::query()->pluck('title', 'id')
            )->rules('required|numeric');

        });
    }
}
