<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\ConfirmEmployer;
use App\Models\Employer;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class EmployerController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.employers'));
            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.employers'));

            $content->body($this->updateForm($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.employers'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Employer::class, function (Grid $grid) {

            $grid->model()->orderBy('id', 'desc');

            $grid->id('ID')->sortable();
            $grid->column('name', __('home.name'));
            $grid->column('surname', __('home.surname'));
            $grid->column('email',__('home.email'));
            $grid->actions((function ($actions) {
                $actions->append(new ConfirmEmployer($actions->getKey(), $actions->row->email));
            }));
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Employer::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', __('home.name'));
            $form->text('surname', __('home.surname'));
            $form->text('email');
            $form->text('phone', __('home.phone'));
            $form->text('fiscal_code', __('home.fiscal_code'))->rules('required|numeric');
            $form->select('gender', __('home.gender'))->options([
                'maschio', 'femmina', 'non importante'
            ]);
            $form->text('iva_code', __('home.iva_code'));
            $form->text('languages', __('home.user_languages'));
            $form->select('skill', __('home.skill'))->options([
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 'più di 5 anni'
            ]);
            $form->file('cv_path', __('home.cv_path'))->removable();;
            $form->file('reference_path', __('home.reference_path'))->removable();;
            $form->file('identificate_path', __('home.identificate_path'))->removable();;
            $form->file('avatar_path', __('home.avatar_path'))->removable();;
            $form->textarea('add_info');
        });
    }
    protected function updateForm()
    {
        return Admin::form(Employer::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', __('home.name'));
            $form->text('surname', __('home.surname'));
            $form->text('email');
            $form->text('phone', __('home.phone'));
            $form->text('fiscal_code', __('home.fiscal_code'))->rules('required|numeric');
            $form->select('gender', __('home.gender'))->options([
                'maschio' => 'Uomo', 'femmina' => 'Donna'
            ]);
            $form->text('iva_code', __('home.iva_code'));
            $form->text('languages', __('home.user_languages'));
            $form->select('skill', __('home.skill'))->options([
                1 => 1,
                2 => 2,
                3 => 3,
                4 => 4,
                5 => 5,
                6 => 'più di 5 anni'
            ]);

            $form->link('cv_path', __('home.cv_path'));
            $form->link('reference_path', __('home.reference_path'));
            $form->link('identificate_path', __('home.identificate_path'));
            $form->link('avatar_path', __('home.avatar_path'));

            $form->textarea('add_info');
        });
    }
}
