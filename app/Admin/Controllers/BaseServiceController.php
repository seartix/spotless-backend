<?php

namespace App\Admin\Controllers;

use App\Models\BaseService;

use App\Models\RealtyType;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Grid\Filter;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class BaseServiceController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Base Service'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Base Service'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Base Service'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(BaseService::class, function (Grid $grid) {

            $grid->column(__('home.title'));

            $grid->filter(function (Filter $filter) {
                $filter->like('title',__('home.title'));
            });
            $grid->column('realty_type_id', __('home.clean-type'));

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(BaseService::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:base_services,title,{$id}");
            $form->ckeditor('text',__('home.text'))->rules('required|string');
            $form->image('image', __('home.image'))->rules('required|file|image')->removable();;

            $form->select('realty_type_id', __('home.clean-type'))
                ->rules('numeric')
                ->options(
                    RealtyType::query()->pluck('title', 'id')
                );
        });
    }
}
