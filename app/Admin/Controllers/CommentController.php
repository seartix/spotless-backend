<?php

namespace App\Admin\Controllers;

use App\Models\Comment;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
class CommentController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Comments'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.Comments'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.Comments'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(Comment::class, function (Grid $grid) {

          //  $grid->model()->orderBy('', 'desc');
            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();
                // Add a column filter
                $filter->like('post_code', __('home.Post Code'));

            });
            $grid->column('article_id',__('home.Article'));
            $grid->column('user_id',__('home.user'));
            $grid->column('text',__('home.text'));


            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Comment::class, function (Form $form) {


            $form->display('text', __('home.comment'));
//
//            $form->select('cleaner_id', __('home.cleaner'))->options(
//                User::query()->cleaners()->pluck('name', 'id')
//            )->rules("required|exists:users,id");
//
//            $form->dateRange('date_start', 'date_end', __('home.date'))->rules("required|date");
//
//            $form->time('time_start', __('home.time_start'))->rules("required");
//            $form->time('time_end', __('home.time_end'))->rules("required|after:time_start");

        });
    }
}
