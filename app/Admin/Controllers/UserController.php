<?php

namespace App\Admin\Controllers;

use App\Admin\Extensions\CsvExporter;
use App\Models\AddService;
use App\Models\AvailableDateForCleaner;
use App\Models\City;
use App\Models\Province;
use App\Models\SubCondition;
use App\Models\Subscription;
use App\User;

use Carbon\Carbon;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Stripe\Collection;

class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(  __('home.users'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(  __('home.users'));

            $content->body($this->updateForm($id)->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(  __('home.users'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(User::class, function (Grid $grid) {
            $grid->model()->where('role', '=', 'client');
            $grid->column('name', __('home.name'));
            $grid->column('email',__('home.email'));
            $grid->column('role', __('home.role'));
            $grid->column('type', __('home.type'));

            $grid->exporter(new CsvExporter('export-clients-'));

            $grid->model()->orderBy('created_at', 'desc');
        });
    }

    public function update($id)
    {
        return $this->updateForm($id)->update($id);
    }


    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function updateForm($id)
    {
        return Admin::form(User::class, function (Form $form) use ($id) {

            $user = User::query()->find($id);

            $form->display('id', 'ID');

            $form->email('email',__('home.email'))->rules('required|email');
            $form->text('name', __('home.name'))
                ->rules('required|string');

            $form->text('surname', __('home.surname'))
                ->rules('required|string');

            if (User::CLIENT_ROLE == $user->role){
                $form->select('subscription_id', __('home.subscription'))->options(
                    Subscription::query()->get()->reduce(function (\Illuminate\Support\Collection $res, $item) {
                        $res->put($item->id, $item->title);
                        return $res;
                    }, collect([ null => ' ']))
                )->rules('nullable|numeric');
            }

            $form->select('gender', __('home.gender'))
                ->options([
                    User::USER_MALE => __('Male'),
                    User::USER_FEMALE => __('Female')
                ])
                ->rules('required');

            $form->dateFormat('date_birthday', __('home.date_birthday'))->rules('required')->format('DD/MM/YYYY')->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y');
            });
            $form->text('phone', __('home.phone'))->rules('required');
            $form->text('address', __('home.address'))->rules('required|string');
            $form->text('house_number', __('home.house_number'))->rules('required|string');

            $form->select('city_id', __('home.city'))->options(
                City::query()->pluck('title', 'id')
            )->rules('required|numeric');

            $form->select('province_id', __('home.procince_id'))->options(
                Province::query()->pluck('title', 'id')
            )->rules('required|numeric');
            $form->image('avatar', __('home.avatar'))->rules('nullable|image')->removable();;

//            $form->number('code')->rules('required|digits_between:5,5|numeric');

            $form->text('firm_name', 'Ragione sociale')->rules('nullable|string');

            $form->text('post_index', __('home.code'))->rules('nullable|string');
            $form->text('domofon', __('home.domofon'))->rules('nullable|string');
            $form->text('domofon_text', __('home.domofon_text'))->rules('nullable|string');

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(User::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', __('home.name'))->rules('nullable|string');
            $form->text('surname', __('home.surname'))->rules('nullable|string');
            $form->email('email')->rules('required|email|unique:users');

            $form->password('password', __('home.password'));

            $form->hidden('role', __('home.role'))->value(User::CLIENT_ROLE);

            $form->select('gender', __('home.gender'))
                ->options([
                    User::USER_MALE => __('Male'),
                    User::USER_FEMALE => __('Female')
                ])
                ->rules('nullable');
            $form->image('avatar', __('home.avatar'))->rules('nullable|image')->removable();;
            $form->dateFormat('date_birthday', __('home.date_birthday'))->rules('required')->format('DD/MM/YYYY')->formatting(function($item){
                return Carbon::parse($item)->format('d/m/Y');
            });
            $form->text('phone', __('home.phone'))->rules('nullable');
            $form->text('address', __('home.address'))->rules('required|string');
            $form->text('house_number', __('home.house_number'))->rules('nullable|string')
            ;
            $form->select('city_id', __('home.city'))->options(
                City::query()->pluck('title', 'id')
            )->rules('required|numeric');

            $form->select('province_id', __('home.provinces'))->options(
                Province::query()->pluck('title', 'id')
            )->rules('required|numeric');
            $form->text('post_index', __('home.code'))->rules('required|numeric|digits_between:0, 9');
        });
    }
}
