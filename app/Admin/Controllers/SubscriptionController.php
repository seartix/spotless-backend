<?php

namespace App\Admin\Controllers;

use App\Models\AddService;
use App\Models\Subscription;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class SubscriptionController extends Controller
{
    use ModelForm;
    use HtmlTrait;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.subscriptions'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(__('home.subscriptions'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(__('home.subscriptions'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $self = $this;

        return Admin::grid(Subscription::class, function (Grid $grid) use ($self) {

            $grid->column('title', __('home.title'));
            $grid->column('description', __('home.description'));
            $grid->column('price', __('home.price'));

            $grid->addServices(__('home.add_services'))->pluck('title')->implode(', ');

//            $grid->column('additional_list', __('home.additional'))->display(function ($list) use ($self) {
//                return $self->html_list($list);
//            });

            $grid->disableExport();
            $grid->disableCreation();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(Subscription::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:subscriptions,title,{$id}");

            $form->text('description', __('home.description'))->rules('required');

            $form->textarea('additional_list_text', __('home.additional'))->rules('nullable|string');

            $form->textarea('frequency', __('home.frequency'))->rules('required|string');

            $form->number('price', __('home.price'))->rules("required|numeric|min:1");

            $form->multipleSelect('services', __('home.add_services_house'))->options(
                AddService::query()->where('realty_type_id', 1)->pluck('title', 'id')
            )->rules('array');
            $form->multipleSelect('services', __('home.add_services_office'))->options(
                AddService::query()->where('realty_type_id', 2)->pluck('title', 'id')
            )->rules('array');
            $form->switch('buy_clear_kit',__('buy_clear_kit_in_sub'))->rules('nullable');
        });
    }
}
