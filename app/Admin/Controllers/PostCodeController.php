<?php

namespace App\Admin\Controllers;

use App\Models\AvailableDateForCleaner;
use App\Models\PostCode;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use Encore\Admin\Controllers\ModelForm;
use Carbon\Carbon;
class PostCodeController extends Controller
{
    use ModelForm;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header( __('home.post-code'));

            $content->body($this->grid());
        });
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header( __('home.post-code'));

            $content->body($this->form());
        });
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header( __('home.post-code'));

            $content->body($this->form($id)->edit($id));
        });
    }



    protected function grid()
    {
        return Admin::grid(PostCode::class, function (Grid $grid) {

          //  $grid->model()->orderBy('', 'desc');
            $grid->filter(function($filter){

                // Remove the default id filter
                $filter->disableIdFilter();
                // Add a column filter
                $filter->like('post_code', 'Post Code');

            });
            $grid->column('post_code',__('home.Post codes'));
            $grid->column('district',__('home.district'));
            $grid->column('approved',__('home.approved'))->display(function () {

                return $this->approved ? 'approvato': 'non approvato';
            });

//            $grid->date('Date')->display(function ($data) {
//                return Carbon::parse($data)->format('Y-m-d');
//            });
//
//            $grid->column('time_start');
//            $grid->column('time_end');

            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(PostCode::class, function (Form $form) {

            $form->number('post_code', __('home.Post codes'))->rules('required');
            $form->text('district', __('home.district'))->rules('required|min:1');
            $form->radio('approved',__('home.approved'))->options([1 => 'approvato', 0=> 'non approvato'])->default(0);
//            $form->display('id', 'ID');
//
//            $form->select('cleaner_id', __('home.cleaner'))->options(
//                User::query()->cleaners()->pluck('name', 'id')
//            )->rules("required|exists:users,id");
//
//            $form->dateRange('date_start', 'date_end', __('home.date'))->rules("required|date");
//
//            $form->time('time_start', __('home.time_start'))->rules("required");
//            $form->time('time_end', __('home.time_end'))->rules("required|after:time_start");

        });
    }
}
