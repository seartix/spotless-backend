<?php

namespace App\Admin\Controllers;

use App\Models\RealtyType;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

/**
 * Class RealtyTypeController
 * @package App\Admin\Controllers
 * Realty type is same as Clean Type
 */
class RealtyTypeController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header(  __('home.clean_types'));

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header(  __('home.clean_types'));

            $content->body($this->form($id)->edit($id));
        });
    }

    public function update($id)
    {
        return $this->form($id)->update($id);
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        return Admin::content(function (Content $content) {

            $content->header(  __('home.clean_types'));

            $content->body($this->form());
        });
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(RealtyType::class, function (Grid $grid) {

            $grid->column('id', 'ID');
            $grid->column('title', __('home.title'));
            $grid->column('price', __('home.price'));
            $grid->column('express_price', __('home.express_price'));
           $grid->disableCreation();
            $grid->actions(function ($actions) {
                $actions->disableDelete();
            });
            $grid->disableExport();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form($id = null)
    {
        return Admin::form(RealtyType::class, function (Form $form) use ($id) {

            $form->display('id', 'ID');

            $form->text('title', __('home.title'))->rules("required|string|unique:realty_types,title,{$id}");;
            $form->number('price', __('home.price'))->rules('required|numeric|min:0');
            $form->number('express_price', __('home.express_price'))->rules('required|numeric|min:0');
            $form->file('icon', __('home.icon'))->move('storage',str_random(10).'.jpg')->removable();
            $form->file('image',__('home.image'))->move('storage',str_random(10).'.jpg');
            $form->text('label_clean_type', __('home.label_clean_type'));
            $form->textarea('description',__('home.description'));
        });
    }
}
