<?php

namespace App\Admin\Controllers;

trait HtmlTrait
{
    public function html_list($list)
    {
        if ($list && count($list) > 0) {
            $data = "<ul style='margin: 0; padding: 0'>";

            foreach ($list as $value) {
                $data .= "<li>{$value}</li>";
            }

            $data .= "</ul>";

            return $data;
        }

        return "Vuoto";
    }

    public function html_checkbox($value)
    {
        if ($value) {
            return "<input type='checkbox' checked='checked' value='1'>";
        } else {
            return "<input type='checkbox' value='0'>";
        }
    }
}
