<?php
/**
 * Created by PhpStorm.
 * User: Vlad
 * Date: 01.05.2019
 * Time: 17:04
 */

namespace App\Admin\Extensions\Form;

use Encore\Admin\Form\Field;


class CKEditor extends Field
{

    //private $cke =  ;
    public static $js = [
        '/js/libs/ckeditor.js',
        '/js/libs/jquery_lib.js',
    ];

    protected $view = 'admin.ckeditor';

    public function render()
    {
        $this->script = "$('textarea').ckeditor();";

        return parent::render();
    }
}
