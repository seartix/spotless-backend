<?php


namespace App\Admin\Extensions;
use Carbon\Carbon;
use Encore\Admin\Grid;
use Encore\Admin\Grid\Exporters\AbstractExporter;
use Maatwebsite\Excel\Facades\Excel;


class CsvExporter extends AbstractExporter
{
    private $name;
    public function __construct($name = '')
    {
        $this->name = $name;
        parent::__construct();
    }

    public function export()
    {
        $this->name .= Carbon::now();

        Excel::create( $this->name, function($excel) {

            $excel->sheet('Sheetname', function($sheet) {
                // This logic get the columns that need to be exported from the table data
                $data = $this->getData();
                $sheet->fromArray($data, null, 'A1', true);

            });
        })->export('xls');
    }
}
