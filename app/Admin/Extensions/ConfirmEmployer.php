<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 31.01.18
 * Time: 14:02
 */

namespace App\Admin\Extensions;

use Encore\Admin\Admin;

class ConfirmEmployer
{
    protected $id;
    protected $email;

    public function __construct($id, $email)
    {
        $this->id = $id;
        $this->email = $email;
    }
    protected function script()
    {
        return <<<SCRIPT

            $('.grid-check-row').on('click', function () {
            
               $.ajax({
                    method: 'post',
                    url: '/api/confirm-employer',
                    data: {
                        id: $(this).data('id'),
                        email: $(this).data('email')
                    },
                    success: function (response) {
                        $.pjax.reload('#pjax-container');
                        toastr.success(response.message);
                        $('.grid-check-row').parent().addClass('active');
                    },
                    error: function (error) {
                        toastr.error(error.responseJSON.email[0])
                    }
                });  
            
            });

SCRIPT;
    }

    protected function render()
    {
        Admin::script($this->script());
        return "<a class='grid-check-row' data-id='{$this->id}' data-email='{$this->email}'><i class='fa fa-check'></i></a>";
    }

    public function __toString()
    {
        return $this->render();
    }
}