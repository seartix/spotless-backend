<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 29.01.18
 * Time: 13:39
 */
namespace App\Admin\Extensions;
use Encore\Admin\Form\Field;

class LinkField extends Field
{
    protected $view = 'admin.link-field';

}