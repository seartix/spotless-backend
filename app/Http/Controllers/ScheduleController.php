<?php

namespace App\Http\Controllers;

use App\Services\UserService;
use Illuminate\Http\Request;

class ScheduleController extends Controller
{
    function create(Request $request, UserService $userService)
    {
        $data = $request->all();
        $cleanerId = $request->input('choosenCleaner');

        unset($data['choosenCleaner']);
        unset($data['_token']);

        $userService->setTimeScheduleForCleaner($cleanerId, $data);

        return redirect()->to('/');
    }

    public function show($cleanerId,Request $request, UserService $userService)
    {
        $days = $userService->getTimeScheduleForCleaner($cleanerId);

        return response()->json($days);
    }
}
