<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\CalculatePriceFromHoursRequest;
use App\Http\Requests\CalculatePriceFromMetersRequest;
use App\Services\RealtyTypeService;
use App\Http\Controllers\Controller;

class RealtyTypePriceHourController extends Controller
{
    public function calculatePriceFromMeters(CalculatePriceFromMetersRequest $request, RealtyTypeService $realtyTypeService)
    {
        $data = $request->only(['meters', 'realty_type_id', 'date', 'time']);
        if(isset($data['date']))
        {
            $price = $realtyTypeService->getPriceFromMeters($data['meters'], $data['realty_type_id'], $data['date'], $data['time']);
        }
        else{
            $price = $realtyTypeService->getPriceFromMetersWithoutDate($data['meters'], $data['realty_type_id']);
        }


        return response([
            'data' => $price
        ]);
    }

    public function calculatePriceFromHours(CalculatePriceFromHoursRequest $request, RealtyTypeService $realtyTypeService)
    {
        $data = $request->only(['hours', 'realty_type_id','date', 'time']);
        if(isset($data['date']))
        {
            $price = $realtyTypeService->getPriceFromHours($data['hours'], $data['realty_type_id'], $data['date'], $data['time']);
        }
        else{

            $price = $realtyTypeService->getPriceFromHoursWithoutDate($data['hours'], $data['realty_type_id']);
        }


        return response([
            'data' => $price
        ]);
    }
}
