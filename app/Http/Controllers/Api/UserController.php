<?php

namespace App\Http\Controllers\Api;

use App\DTOs\CreateUserDto;
use App\Events\NewFeedbackEvent;
use App\Filters\UserFilter;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChooseCleanerRequest;
use App\Http\Requests\CreateReviewRequest;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\PasswordResetRequest;
use App\Http\Requests\SetPasswordRequest;
use App\Models\AdminUser;
use App\Models\Miscalculation;
use App\Notifications\AdminMiscalculationNotification;
use App\Notifications\SubscriptionCanceledNotification;
use App\Services\UserService;
use App\Transformers\Review\ReviewIndexTransformer;
use App\Transformers\User\AllCleanersIndexTransformer;
use App\Transformers\User\UserIndexTransfromer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function show(UserService $userService)
    {
        $user = $userService->getCurrent();
        if ($user) {
            $card = auth()->guard('api')->user()->card;
            $fractal = fractal($user, new UserIndexTransfromer());
            $fractal->addMeta(
                [
                    'availableDates' => auth()->guard('api')->user()->availableDates()->get(),
                   // 'card' => auth()->guard('api')->user()->card
                ]
            );
            return $fractal->respond();
        } else {
            return response([
                'error' => 'user not found',
            ], 402);
        }
    }
    public function create(CreateUserRequest $createUserRequest, UserService $userService)
    {
        $data = $createUserRequest->only(['name', 'email', 'role',
            'password', 'type', 'fiscal_code',
            'surname', 'gender', 'date_birthday',
            'phone', 'address', 'house_number',
            'city_id', 'province_id', 'code',
            'firm_name', 'post_index', 'domofon', 'domofon_text', 'avatar','isSubscribe']);

        if ($data['date_birthday']) {
            $date = $data['date_birthday'];
            $newDate = \DateTime::createFromFormat('d/m/Y', $date);
            $newDate = $newDate->format('Y-m-d');
            $data['date_birthday'] = $newDate;
        }


        $createUserDto = new CreateUserDto($data);

        try {
            $user = $userService->create($createUserDto);
            $fractal = fractal($user, new UserIndexTransfromer());
            return $fractal->respond();
        } catch (\Exception $exception) {
            return response([
                'error' => $exception->getMessage()
            ], 402);
        }
    }

    public function chooseCleaners(ChooseCleanerRequest $chooseCleanerRequest, UserService $userService)
    {
        $data = $chooseCleanerRequest->only([
            'date',
            'time',
            'addSubSeriviceIds',
            'addSubConditionIds',
            'realty_type',
            'hours'
        ]);

        $cleaners = $userService->chooseCleaners($data);

        if ($cleaners) {
            $fractal = fractal($cleaners, new AllCleanersIndexTransformer());
            return $fractal->respond();
        } else {
            return response(
                [
                    'error' => 'not found'
                ], 402);
        }
    }

    public function createReview(CreateReviewRequest $request, UserService $userService)
    {
        if ($user = $request->user('api')) {
            $data = $request->only([
                'text',
                'cleaner_id',
                'rating',
                'recommend'
            ]);

            $userService->createReview($user->id, $data['cleaner_id'], $data['text'], $data['rating'], $data['recommend']);

            return response([]);
        }

        abort(403);
    }

    public function getCleanerReviews(Request $request, UserService $userService)
    {
        $data = $request->only([
            'cleaner_id'
        ]);

        $reviews = $userService->getReviews($data['cleaner_id']);
        $fractal = fractal($reviews, new ReviewIndexTransformer());

        return $fractal->respond();
    }

    public function allCleaners(UserService $userService)
    {
        return $userService->getCleaners();
    }

    public function getUserCleaners(Request $request, UserService $userService)
    {
        if ($user = $request->user('api')) {
            return $userService->getUserCleaners($user->id);
        }
        abort(403);
    }

    public function postUserAddress(Request $request)
    {
        if ($user = $request->user('api')) {
            $data = $request->only([
                'address',
                'house_number',
                'city_id',
                'province_id',
                'code',
                'domofon',
                'domofon_text',
                'post_index'
            ]);
            $user->update($data);
            $user->save();
            $userEntity = new \App\Entities\User($user->id, $user->name, $user->email, $user->role,
                $user->password, null, null, $user->type, $user->fiscal_code,
                $user->surname, $user->gender, $user->date_birthday,
                $user->phone, $user->address, $user->house_number,
                $user->city_id, $user->province_id, $user->code, $user->domofon, $user->domofon_text,
                $user->firm_name, $user->post_index);

            $fractal = fractal($userEntity, new UserIndexTransfromer());
            $fractal->addMeta([
                'availableDates' => $user->availableDates()->get(),
                'card' => auth()->guard('api')->user()->card
            ]);

            return $fractal->respond();
        }

        abort(403);
    }

    public function postUserInfo(Request $request)
    {
        if ($user = $request->user('api')) {
            $this->validate($request, [
                'fiscal_code' => 'string|nullable|size:16',
                'name' => 'string|nullable',
                'surname' => 'string|nullable',
                'email' => 'string|nullable',
                'firm_name' => 'string|nullable',
                'address' => 'string|nullable'
            ]);

            $data = $request->only([
                'fiscal_code',
                'name',
                'surname',
                'gender',
                'date_birthday',
                'phone',
                'email',
                'firm_name',
                'address'
            ]);

            $user->update($data);

            $this->validate($request, [
                'card_number' => 'numeric|nullable',
                'exp_date' => 'numeric|nullable',
                'exp_year' => 'numeric|nullable',
                'holder_name' => 'string|nullable',
                'cvv' => 'numeric|nullable',
                'card_type' => 'numeric|nullable',
            ]);

            $data = $request->only([
                'card_number',
                'exp_date',
                'exp_year',
                'holder_name',
                'cvv',
                'card_type'
            ]);
            $user->updateCard($data);
            $userEntity = new \App\Entities\User($user->id, $user->name, $user->email, $user->role,
                $user->password, null, null, $user->type, $user->fiscal_code,
                $user->surname, $user->gender, $user->date_birthday,
                $user->phone, $user->address, $user->house_number,
                $user->city_id, $user->province_id, $user->code, $user->domofon, $user->domofon_text,
                $user->firm_name, $user->post_index);
            $fractal = fractal($userEntity, new UserIndexTransfromer());
            $fractal->addMeta([
                'availableDates' => $user->availableDates()->get(),
                'card' => $user->card
            ]);

            return $fractal->respond();
        }

        abort(403);
    }

    public function resetPassword(PasswordResetRequest $request, UserService $userService)
    {
        $data = $request->email;

        return $userService->resetPassword($data) ? response('Controlla la tua casella email') : response('error', 422);
    }

    public function setPassword(SetPasswordRequest $request, UserService $userService)
    {
        $token = $request->token;
        $password = $request->password;

        return $userService->setPassword($token, $password) ? response('done', 200) : response('error', 422);
    }

    public function getAllCleaners(UserService $userService, UserFilter $userFilter)
    {
        $cleaners = $userService->getAllCleaners($userFilter);

        if ($cleaners) {
            $fractal = fractal($cleaners, new AllCleanersIndexTransformer());
            return $fractal->respond();
        } else {
            return response(
                [
                    'error' => 'not found'
                ], 402);
        }
    }

    public function cancelSubscription(Request $request)
    {
        if ($user = $request->user('api')) {
            if ($user->subscription_id) {
                $subscription = $user->subscription;
                $user->subscription_id = null;
                $user->save();
                $user->notify(new SubscriptionCanceledNotification($user->name, $subscription->title));
                return response('', 200);
            }
        }
        abort(403);
    }
    public function sendFeedback (Request $request)
    {
        $data = $request->all();
        event(new NewFeedbackEvent($data));
    }
    public function getAvailableDatesForCleaner(Request $request, UserService $userService)
    {
        $dates = $userService->getAvailableDatesForCleaner();
        if ($dates) {
            return $dates;
        }
        abort(403);
    }
    public function getAvailableTimeForCleaner(Request $request, UserService $userService)
    {
        $this->validate($request, [
            //'cleaner_id' => 'required|numeric|exists:users,id',
            'date' => 'required|date',
            'duration' => 'required|numeric'
        ]);
        $data = $request->only([
            //'cleaner_id',
            'date',
            'duration'
        ]);
        $times = $userService->getAvailableTimeFromCleaner($data['date'], $data['duration']);

        return $times;

    }

    public function sendMiscalculation(Request $request,Miscalculation $miscalculation)
    {
        $admin = AdminUser::query()
            ->where('username', 'admin')
            ->first();
        $miscalculation->email = $request->input('email');

        if($request->input('zipcode')){

            $miscalculation->zip_code = $request->input('zipcode');
        }else{
            $miscalculation->big_house = true;
            $miscalculation->zip_code = '-';
        }
        $miscalculation->text = $request->input('text');

        if($request->hasFile('file')) {
            $file = $request->file('file');
            $name = str_replace(' ','-',$file->getClientOriginalName());
            $file->move(public_path() . '/upload/storage/',$name);
            $miscalculation->file = '/upload/storage/'.$name;
        }
        $miscalculation->save();
//        if($miscalculation){
//            $admin->notify(new AdminMiscalculationNotification($miscalculation));
//        }
       return response()->json($miscalculation ? true : false);
    }

    public function setFacebookId(Request $request)
    {
        if($request->has('facebook_id')){
            $facebook_id = $request->input('facebook_id');
            $user = auth()->guard('api')->user();
            $user->facebook_id = $facebook_id;
            $user->save();
            return response()->json(['facebook_id'=>$facebook_id]);
        }
        return response()->json(['error'=>'Invalid facebook id']);
    }

    public function authUserByFacebook(Request $request)
    {
        if($request->has('facebook_id')){
           $user =  User::query()->where('facebook_id',$request->input('facebook_id'))->first();
            if($user){
                $data = $user->createToken('auth');
                $expire = $data->token->expires_at;
                $token = $data->accessToken;
               return response()->json(['token'=>$token,'expires_in'=>Carbon::parse($expire)->format('Y-m-d H:i:s')]);
            }
            return response()->json(['error'=>'User not exist']);
        }

    }

    public function setInstagramId(Request $request)
    {
        if($request->has('instagram_id')){
            $instagram_id = $request->input('instagram_id');
            $user = auth()->guard('api')->user();
            $user->instagram_id = $instagram_id;
            $user->save();
            return response()->json(['instagram_id'=>$instagram_id]);
        }
    }
    public function setGoogleId(Request $request)
    {
        if($request->has('google_id')){
            $google_id = $request->input('google_id');
            $user = auth()->guard('api')->user();
            $user->google_id = $google_id;
            $user->save();
            return response()->json(['google_id'=>$google_id]);
        }
        return response()->json(['error'=>'Invalid google id']);
    }

    public function authUserByGoogle(Request $request)
    {
        if($request->has('google_id')){
            $user =  User::query()->where('google_id',$request->input('google_id'))->first();
            if($user){
                $data = $user->createToken('auth');
                $expire = $data->token->expires_at;
                $token = $data->accessToken;
                return response()->json(['token'=>$token,'expires_in'=>Carbon::parse($expire)->format('Y-m-d H:i:s')]);
            }
            return response()->json(['error'=>'User not exist']);
        }

    }
}
