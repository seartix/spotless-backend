<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\RealtyTypeService;
use App\Transformers\RealtyTypeMeterHours\RealtyTypeMeterHoursIndexTransformer;

class RealtyTypeMeterHourController extends Controller
{
    public function index (RealtyTypeService $realtyTypeService)
    {
        $realtyTypesMeterHour =  $realtyTypeService->getAllRealtyTypeMeterHours();

        if ($realtyTypesMeterHour) {
            $fractal = fractal($realtyTypesMeterHour, new RealtyTypeMeterHoursIndexTransformer());
            return $fractal->respond();
        }
        else {
            return response([
                'error' => 'not found'
            ], 402);
        }
    }
}
