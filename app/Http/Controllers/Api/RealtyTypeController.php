<?php

namespace App\Http\Controllers\Api;

use App\Services\RealtyTypeService;
use App\Http\Controllers\Controller;
use App\Transformers\RealtyType\RealtyTypeIndexTransformer;

class RealtyTypeController extends Controller
{
    public function index (RealtyTypeService $realtyTypeService)
    {
        $realtyTypes = $realtyTypeService->getAll();

        $fractal = fractal($realtyTypes, new RealtyTypeIndexTransformer());
        $fractal->addMeta([
            'count' => $realtyTypes->count()
        ]);

        return $fractal->respond();
    }
}
