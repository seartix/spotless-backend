<?php

namespace App\Http\Controllers\Api;

use App\DTOs\CreateUserDto;
use App\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;


class InstagramController extends Controller
{
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.instagram.com/',
        ]);
    }

    public function facebook()
    {
        return Socialite::driver('facebook')->stateless()->user();
    }

    public function callbackInstagram(Request $request,User $user)
    {
        if($request->has('code')){
            $response = $this->client->request('POST', 'oauth/access_token', [
                'form_params' => [
                    'client_id' => env('CLIENT_ID_INSTAGRAM'),

                    'client_secret' => env('CLIENT_SECRET_INSTAGRAM'),

                    'grant_type' => 'authorization_code',

                    'redirect_uri' => env('REDIRECT_URL_INSTAGRAM'),

                    'code' => $request->input('code')
                ]
            ]);
            $data =  json_decode($response->getBody()->getContents(),true);
            $user = $user->where('instagram_id',$data['user']['id' ])->first();
            if($user){
                $data = $user->createToken('auth');
                $expire = $data->token->expires_at;
                $token = $data->accessToken;
                return redirect('https://spotlessitalia.com/#/?token='.$token.'&expires_in='.Carbon::parse($expire)->format('Y-m-d H:i:s'));
            }
            else{

                return redirect('https://spotlessitalia.com/#/personal-area?token='.$data['user']['id' ]);
            }

        }

    }
}
