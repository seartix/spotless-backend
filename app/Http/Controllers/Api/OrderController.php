<?php

namespace App\Http\Controllers\Api;

use App\Events\OrderCreatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\ChangeOrderRequest;
use App\Http\Requests\CreateOrderRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\ActiveSubscription;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\RealtyType;
use App\Services\OrderService;
use App\Transformers\Order\CopyOrderTransformer;
use App\Transformers\Order\CreateOrderViewModelTransformer;
use App\Transformers\Order\OrderIndexTransformer;
use App\Transformers\Order\OrderShowTransformer;
use App\Transformers\Order\OrderStepAmountDefault;
use App\Transformers\Order\StepAmountDefault;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use SoapClient;

class OrderController extends Controller
{
    public function getViewModel(OrderService $orderService)
    {
        $createOrderViewModel = $orderService->createOrderViewModel();

        $fractal = fractal($createOrderViewModel, new CreateOrderViewModelTransformer());

        return $fractal->respond();
    }

    public function index(OrderService $orderService)
    {

        $orders = $orderService->getAll();

        $fractal = fractal($orders, new OrderIndexTransformer());

        return $fractal->respond();
    }

    public function archived(OrderService $orderService)
    {
        $orders = $orderService->getArchived();

        $fractal = fractal($orders, new OrderIndexTransformer());

        return $fractal->respond();
    }

    public function expiration(OrderService $orderService)
    {
        $user = auth()->guard('api')->user();
        $result = 'error';
        if($orderService->expirationSucription($user)){
           $result = 'success';
        }
        return response()->json($result);
    }

    public function getPriceForRenewSubscription()
    {
        $user = auth()->guard('api')->user();
        $subscription = ActiveSubscription::where('user_id',$user->id)->where('active',true)->first();
        return response()->json(['subscription'=>$subscription,'meters'=>$subscription->orders()->first()->meters ?? 0]);
    }

    public function changeStatus (ChangeOrderRequest $request, OrderService $orderService)
    {
        $data = $request->only(['order_id', 'status']);
        $orderService->changeOrderStatus($data['order_id'], $data['status']);
        $orders = $orderService->getAll();

        $fractal = fractal($orders, new OrderIndexTransformer());

        return $fractal->respond();
    }

    public function acceptOrderAction(Request $request, OrderService $orderService)
    {
        $data = $request->only([
            'order_id', 'cleaner_id'
        ]);
        if ($orderService->acceptOrder($data['order_id'], $data['cleaner_id'])){
            return response()->json([
               'msg' => 'order accepted'
            ])->setStatusCode(200);
        } else {
            return response()->setStatusCode(500);
        }
    }

    public function getOrdersByStatus(Request $request, OrderService $orderService)
    {
        $data = $request->only([
           'cleaner_id', 'status'
        ]);

        $orders = $orderService->getOrdersByStatus($data['cleaner_id'], $data['status']);

        $fractal = fractal($orders, new OrderIndexTransformer());

        return $fractal->respond();
    }

    public function store(Request $request)
    {
        if ($user = $request->user('api')) {

//            $data = $request->only([
//                'token',
//                'type',
//                'date',
//                'time',
//                'city_id',
//                'realty_type_id',
//                'cleaner_id',
//                'meters',
//                'comment',
//                'buy_clear_kit',
//                'secret_place_id',
//
//                'addSubServices',
//                'surfaces',
//                'subConditions',
//                'insurances',
//                'subscription_id',
//                'buySubscription',
//                'coupon_id',
//                'address',
//                'is_created_with_subscription'
//            ]);
           //$result = $orderService->storeOrder($data, $user);
            $order = Order::find($request->input('id'));
            if($request->has('comment')){
                $order->comment = $request->input('comment');
                $order->save();
            }
            return $order;
        }

        abort(403);
    }

    public function copyOrderAccept($id)
    {
        $user = auth()->guard('api')->user();
        $order =  $user->orders()->find($id);
        if($order){
            $order->update(['status'=>'process']);
        }
        return response('true');
    }

    public function copyOrderGestPay($id)
    {
        $data = \request()->only(['couponCode','price_without_subscription']);
        $coupon = $data['couponCode'];
        $price= $data['price_without_subscription'];
        $user = auth()->guard('api')->user();
        $order =  $user->orders()->find($id);
        $date = Carbon::now()->setTime(0,0,0);
        $coupon = Coupon::query()
            ->where('code', $coupon)
            ->where('date_end', '>=', $date)
            ->first();
        $amount = $order->amount;
        if(($order->amount != $price - (floatval($price) * 0.1)) && $user->subscriber){
            $amount = $order->amount - ($order->amount *0.1);
        }
        $amount = $coupon ? $amount * (floatval($coupon->percent)/100) : $amount;

        $client = new SoapClient(env('GESTPAY_URL'));
        $objectResult = null;
        $shopLogin = env('SHOP_LOGIN');

        $params = array(
            'shopLogin' => $shopLogin,
            'apikey'=>env('GESTPAY_API_KEY')
        ,'uicCode' => '242'
        ,'amount' => $amount
        ,'shopTransactionId' => "SPOTLESS-$order->id"
        );
        try {
            $objectResult = $client->Encrypt($params);
        } catch (\SoapFault $exception) {
            Order::query()->where('token', $order->token)->delete();
            return response('fail');
        }
        $result = simplexml_load_string($objectResult->EncryptResult->any);
        $errCode = $result->ErrorCode;
        if ($errCode != 0) {
            Order::query()->where('token', $order->token)->delete();
            return response($result->ErrorDescription);
        }

        $encString = $result->CryptDecryptString;
        $resultString = json_encode([
            'url' => env('PAYMENT_URL')."?a=$shopLogin&b=$encString",
            'token' => $order->token,
            'id' => $order->id
        ]);
        return response($resultString);
    }

    public function deleteSubscription(ActiveSubscription $activeSubscription)
    {
        $user = auth()->guard('api')->user();
        $activeSubscription = $activeSubscription->where('user_id',$user->id)->first();
        if($activeSubscription){
            $orders = $activeSubscription->orders()->update(['status' => 'archived','active_subscription'=>null]);
            $subscriptions = $activeSubscription->delete();
            return response()->json([$orders,$subscriptions]);
        }
    }

    public function gestpay(Request $request, OrderService $orderService)
    {
        if ($user = $request->user('api')) {
            $order = Order::find($request->input('id'));

            if($request->has('comment')){
                $order->comment = $request->input('comment');
                $order->save();
            }


            $client = new SoapClient(env('GESTPAY_URL'));
            $objectResult = null;
            $shopLogin = env('SHOP_LOGIN');

            $params = array(
                'shopLogin' => $shopLogin,
                'apikey' => env('GESTPAY_API_KEY')
            , 'uicCode' => '242'
            , 'amount' => $order->amount
            , 'shopTransactionId' => "SPOTLESS-$order->id"
            );
            try {
                $objectResult = $client->Encrypt($params);
            } catch (\SoapFault $exception) {
                Order::query()->where('token', $order->token)->delete();
                return response('fail');
            }
            $result = simplexml_load_string($objectResult->EncryptResult->any);
            $errCode = $result->ErrorCode;
            if ($errCode != 0) {
                Order::query()->where('token', $order->token)->delete();
                return response($result->ErrorDescription);
            }

            $encString = $result->CryptDecryptString;
            $resultString = json_encode([
                'url' => env('PAYMENT_URL') . "?a=$shopLogin&b=$encString",
                'token' => $order->token,
                'id' => $order->id
            ]);
            return response()->json([
                'result' => $resultString,
                'order' => $order
            ]);
        }
        else{
            abort(403);
        }


    }

    public function saveOrder(Request $request, OrderService $orderService)
    {
        if ($user = $request->user('api')) {
            $data = $request->only([
                'id',
                'count_bathroom',
                'token',
                'type',
                'date',
                'time',
                'city_id',
                'realty_type_id',
                'cleaner_id',
                'meters',
                'comment',
                'buy_clear_kit',
                'secret_place_id',
                'direct_payment',
                'addSubServices',
                'surfaces',
                'subConditions',
                'insurances',
                'subscription_id',
                'buySubscription',
                'coupon_id',
                'address',
                'frequency'
            ]);
            if (!$data['direct_payment']) {
                $order = Order::query()
                    ->find($request->input('id'));
                if ($order == null) {
                    if ($data['subscription_id'] == Order::ABONEMENT) {
                        $order = $orderService->storeOrderSubscription($data, $user);
                    } else {
                        $order = $orderService->storeOrder($data, $user);
                    }
                }
                if ($data['token'] === 'copy') {

                }

                $amount = round($order->amount, 2);


                if (isset($data['subscription_id']) && isset($data['buySubscription'])) {
                    if ($data['buySubscription']) {
                        $amount *= 4;
                    }
                }
            } else {
                $order = $orderService->renewSubscription($user);
                $amount = round($order->amount, 2);
            }
            $coupon = (float)($request->input('couponPercent') / 100);
            $amount = $amount * $coupon;
            return response()->json([
                'order' => $order
            ]);
        } else {
            abort(403);
        }
    }
    public function update(Request $request)
    {
        if ($user = $request->user('api')) {
            $order = Order::query()
                ->find($request->input('id'));

            event(new OrderCreatedEvent($order));

            return [
                'order' => $order,
                'id' => $order->id
            ];
        }
        abort(403);
    }

    public function postChangeOrderGestPay(UpdateOrderRequest $request, OrderService $orderService, $order_id)
    {
        $user = $request->user('api');

        if ($user && $order = $user->orders()->find($order_id)) {

            $data = $request->only([
                'token',
                'type',
                'clean_format_date',
                'clean_format_time',
                'city_id',
                'realty_type_id',
                'cleaner_id',
                'meters',
                'comment',
                'buy_clear_kit',
                'secret_place_id',

                'add_services',
                'add_surfaces',
                'sub_conditions',
                'insurances',
                'subscription_id',
                'address'
            ]);

            if ($data['token'] == 'init') {
                $data['token'] = str_random(20);
            }
            $data['count_bathroom'] = $order->count_bathroom;
            $data['date_time'] = Carbon::parse("{$data['clean_format_date']} {$data['clean_format_time']}");
            unset($data['clean_format_date'], $data['clean_format_time']);

            $data['user_id'] = $user->id;

            $base_amount = $order->amount;

            $order = Order::updateOrder($order, $data);

            $amount = floatval($order->amount) - floatval($base_amount);

            $client = new SoapClient(env('GESTPAY_URL'));
            $objectResult = null;
            $shopLogin = env('SHOP_LOGIN');

            $params = array(
                'shopLogin' => $shopLogin,
                'apikey'=>env('GESTPAY_API_KEY')
            , 'uicCode' => '242'
            , 'amount' => $amount
            , 'shopTransactionId' => "SPOTLESS-$order->id"
            );
            try {
                $objectResult = $client->Encrypt($params);
            } catch (\SoapFault $exception) {
                Order::query()->where('token', $order->token)->delete();
                return response('fail');
            }
            $result = simplexml_load_string($objectResult->EncryptResult->any);
            $errCode = $result->ErrorCode;
            if ($errCode != 0) {
                Order::query()->where('token', $order->token)->delete();
                return response($result->ErrorDescription);
            }

            $encString = $result->CryptDecryptString;
            $resultString = json_encode([
                'url' => env('PAYMENT_URL=')."?a=$shopLogin&b=$encString",//  https://sandbox.gestpay.net/pagam/pagam.aspx?
                'token' => $order->token,
                'id' => $order->id
            ]);
            return response($resultString);
        } else {
            abort(403);
        }

    }

    public function checkGestOrder(Request $request, OrderService $service)
    {
        $data = $request->only(['token', 'id']);
        $response = $service->checkGestOrder($data);

        return response($response);

    }

    public function response()
    {
        $shopLogin = \request()->query->get('a');
        $cryptedString = \request()->query->get('b');
        if ($shopLogin && $cryptedString) {

            $client = new SoapClient(env('GESTPAY_URL'));
            $objectResult = null;
            $params = array('shopLogin' => $shopLogin,'apikey'=>env('GESTPAY_API_KEY'),'CryptedString' => $cryptedString);
            try {
                $objectResult = $client->Decrypt($params);
            } catch (\SoapFault $exception) {
                return response('fail');
            }

            $result = simplexml_load_string($objectResult->DecryptResult->any);

            $err = (string)$result->ErrorCode;
            $errorDescription = (string)$result->ErrorDescription;

            if ($err) {
                $id = (int)preg_replace('/\D/', '', (string)$result->ShopTransactionID);
                $order = Order::query()->where('id', $id)->first();

                if (isset($order->subscription_id)) {
                    Order::query()->where('token', $order->token)->get()->map(function ($item) use ($errorDescription) {
                        $item->token = "FAIL-$errorDescription";
                        $item->save();
                    });
                } else {
                    $order->token = "FAIL-$errorDescription";
                    $order->save();
                }


                return response($result);
            } else {
                $id = (int)preg_replace('/\D/', '', (string)$result->ShopTransactionID);
                $order = Order::query()->where('id', $id)->first();
                if (isset($order->subscription_id)) {
                    Order::query()->where('token', $order->token)->get()->map(function ($item) use ($result) {
                        $item->token = "OK-$result->AuthorizationCode";
                        $item->status = 'process';
                        $item->save();
                    });

                } else {
                    $order->token = "OK-$result->AuthorizationCode";
                    $order->status = 'process';
                    $order->save();

                }


                $order->amount *= 4;
                event(new OrderCreatedEvent($order));
            }
            //$order->user->notify(new SubscriptionCreatedNotification($order->user->name ?? 'User', $order->user->subscription->title ?? ' '));
            return response($result);
        }
        return 'fail';
    }

    public function postChangeOrder(UpdateOrderRequest $request, OrderService $orderService, $order_id)
    {
        $user = $request->user('api');

        if ($user && $order = $user->orders()->find($order_id)) {

            $data = $request->only([
                'token',
                'type',
                'clean_format_date',
                'clean_format_time',
                'city_id',
                'realty_type_id',
                'cleaner_id',
                'meters',
                'comment',
                'buy_clear_kit',
                'secret_place_id',
                'add_services',
                'add_surfaces',
                'sub_conditions',
                'insurances',
                'subscription_id',
                'address',
                'addSubServices'
            ]);

            if (isset($data['addSubServices']) && count($data['addSubServices'])) {
                $data['add_services'] = $data['addSubServices'];
            }
            $data['count_bathroom'] = $order->count_bathroom;
            $data['date_time'] = Carbon::parse("{$data['clean_format_date']} {$data['clean_format_time']}");
            unset($data['clean_format_date'], $data['clean_format_time']);

            $data['user_id'] = $user->id;

            $order = Order::updateOrder($order, $data);

            $orders = $orderService->getAll();

            $fractal = fractal($orders, new OrderIndexTransformer());

            return $fractal->respond();
        }

        abort(403);
    }

    public function postEvalOrder(UpdateOrderRequest $request, OrderService $orderService, $order_id)
    {
        $user = $request->user('api');

        if ($user && $order = $user->orders()->find($order_id)) {

            $data = $request->only([
                'id',
                'clean_format_date',
                'clean_format_time',
                'city_id',
                'realty_type_id',
                'cleaner_id',
                'meters',
                'comment',
                'buy_clear_kit',

                'add_services',
                'addSubServices',
                'add_surfaces',
                'sub_conditions',
                'insurances',
                'subscription_id',
                'address'
            ]);

            $data['coupon_id'] = Order::query()->where('id', $data['id'])->first()->coupon_id;

            $data['count_bathroom'] = $order->count_bathroom;
            $data['date_time'] = Carbon::parse("{$data['clean_format_date']} {$data['clean_format_time']}");
            unset($data['clean_format_date'], $data['clean_format_time']);

            $data['user_id'] = $user->id;

            $evalData = Order::evalOrder($data);

            $price = floatval($evalData['amount']) - floatval($order->amount);
            $evalData['amount'] = $price;

            return response()->json($evalData);
        }

        abort(403);
    }

    public function postCopyOrder(Request $request, OrderService $orderService, $order_id)
    {
        $user = $request->user('api');

        if ($user && $order = $user->orders()->find($order_id)) {

//            $this->validate($request, [
//                'date' => [
//                    'required',
//                    'date_format:Y-m-d',
//                ],
//                'time' => [
//                    'required',
//                    'date_format:H:i',
//                ],
//            ]);

            $data = $request->only([
                'date',
                'time',
                'cleaner'
            ]);

            $date_time = Carbon::parse("{$data['date']} {$data['time'][0]}");
            if($data['cleaner'])$order->cleaner_id = $data['cleaner']['id'];
            $order = $orderService->copyOrder($order, $date_time,str_random(20));
            $order->load(['addServices' => function ($q) {
                $q->select('id');
            }, 'insurances' => function ($q) {
                $q->select('id');
            }, 'addSubConditions' => function ($q) {
                $q->select('id');
            }, 'addSurfaces' => function ($q) {
                $q->select('id');
            }]);

            $fractal = fractal($order, new CopyOrderTransformer());

            return $fractal->respond();
//            return $order->toJson();
        }

        abort(403);
    }

    public function getStepAmount(Request $request, OrderService $orderService)
    {
        $this->validate($request, [
            'meters' => [
                'numeric',
                'nullable'
            ],
            'addSubService' => [
                'numeric'
            ]
        ]);

        $data = $request->only([
            'meters',

            'addSubService',
        ]);

        $addServices = $data['addSubService'];

        $addServicesData = $orderService->calculateAddService($data['meters'], $addServices);

        return $addServicesData;
    }

    public function invoice($id)
    {
        $order = Order::query()
            ->where('id', $id)
            ->with(['city', 'subscription'])
            ->first();
        $year = new Carbon($order->created_at);

        $year = $year->format('Y');

        $orders = Order::select(DB::raw("id, year(created_at) as year"))
            ->get()
            ->groupBy('year');

        $orders = $orders[$year];
        $id = $orders->map(function ($item, $index) use ($order) {
            if ($item->id == $order->id) {
                return $index;
            }
        })->max();
        $id++;
        $user = User::where('id', '=', $order->user_id)->first();
        $realty_type = RealtyType::find($order->realty_type_id);

        $price = $realty_type->getActualPrice(Carbon::parse($order->date_time));
        $name = '';
        $code = '';
        $created_at = $order->created_at->format('d/m/Y');
        $IVA = 0.22;
        $full_price = $order->amount;
        $netto_price = floor(($full_price / (1 + $IVA)) * 100) / 100;
        $IVA_val = floor(($netto_price * $IVA) * 100) / 100;
        $city = $order->city->title;
        $clener_profit = 1;
        if ($order->subscription) {
            $clener_profit = ($order->subscription->id == 1) ? 0.8095 : 0.6296;
        } else {
            $clener_profit = ($price == 12) ? 0.7083 : 0.68;
        }
        $clener_profit = floor(($netto_price * $clener_profit) * 100) / 100;
        $spot_profit = floor(($netto_price - $clener_profit) * 100) / 100;

        if ($user->type == 'persona') {
            $name = $user->name . ' ' . $user->surname;

        } else {
            $name = $user->firm_name;
            $code = $user->fiscal_code;
        }

        return view('report_layout/report',
            compact(
                'name',
                'code',
                'created_at',
                'city',
                'netto_price',
                'IVA_val',
                'full_price',
                'user',
                'spot_profit',
                'clener_profit',
                'orders',
                'id',
                'year'
            )
        );
    }

    public function deleteOrder($order_id, OrderService $orderService)
    {
        $orderService->delete($order_id);

        $orders = $orderService->getAll();

        $fractal = fractal($orders, new OrderIndexTransformer());

        return $fractal->respond();
    }

    public function getCheckboxServices(Request $request,OrderService $orderService)
    {
        $data = $request->only(['clean_type']);
        $services = $orderService->getCheckboxServicesByCleanType($data['clean_type']);
        return response()->json($services);
    }
}
