<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ApplyCouponRequest;
use App\Services\CouponService;
use App\Transformers\Coupon\CouponApplyTransform;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function apply(ApplyCouponRequest $request, CouponService $couponService)
    {
        $data = $request->only(['couponCode', 'price']);
        $couponData = $couponService->apply($data['couponCode'], $data['price'], $request->user('api')->id);

        if ($couponData == 'coupon not found')
            return response()->json('Questo numero di coupon non esiste or coupon are overdue');
        if ($couponData == 'coupon already in user')
            return response()->json('coupon already in user');
        if ($couponData == 'price is less then needed to apply coupon')
            return response()->json('price is less then needed to apply coupon');

        $fractal = fractal($couponData, new CouponApplyTransform());
        return $fractal->respond();
    }
}
