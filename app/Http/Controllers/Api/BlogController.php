<?php

namespace App\Http\Controllers\Api;

use App\Models\Article;
use App\Models\Category;
use App\Models\Comment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    public function index(Category $category,Article $article)
    {
        $categories = $category->where('parent_id',0)->with('children')->get()->map(function($categories){
           return[
               'id'=>$categories->id,
               'name'=>$categories->name,
               'children'=>$categories->children


           ];
        })->toArray();
        $articles = $article->where('approved',true)->with('comments')->get()->map(function($article){
            return[
                'id'=>$article->id,
                'title'=>$article->title,
                'content'=>$article->content,
                'media'=>$article->media,
                'category_id'=>$article->category_id,
                'comments'=>$article->comments->count(),
                'mainPage'=>(boolean)$article->on_main_page
            ];
        })->toArray();
        return response()->json(
            [
                'categories'=>$categories,
                'articles'=>$articles
            ]
        );
    }

    public function getArticle(Article $article,Category $category,$id)
    {
        $article = $article->with('comments.user','categories')->find($id)->toArray();
       foreach ($article['comments'] as $key=>$comment){
           $article['comments'][$key] = [
               'id'=>$comment['id'],
               'text'=>$comment['text'],
               'user'=>[
                   'id'=>$comment['user']['id'],
                   'name'=>$comment['user']['name'],
                   'avatar'=>$comment['user']['avatar']
               ]
           ];
       }
         $categories = $category->where('parent_id',0)->with('children')->get()->map(function($category) {
            return [
                'id' => $category->id,
                'name' => $category->name,
                'children' => $category->children


            ];
        });
        return [
            'article'=>$article,
            'categories'=>$categories,
            'choosen_category'=>$article['categories']['name']
        ];
    }

    public function setComment(Request $request,Comment $comment, Article $article)
    {
        if($request->has('user_id') && $request->has('text')){

            $comment->user_id = $request->input('user_id');
            $comment->text = $request->input('text');
            $comment->article_id = $request->input('article');
            if($comment->save()){
                return [
                    'comment'=>$comment,
                    'user'=>
                        [
                            'id'=>$comment->user->id,
                            'name'=>$comment->user->name,
                            'avatar'=>$comment->user->avatar,
                        ]
                ];

            }
        }
    }

}
