<?php

namespace App\Http\Controllers\Api;

use App\Models\Order;
use App\Notifications\RateCleanerNotification;
use App\Services\TextPageService;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextPageController extends Controller
{
    public function getPage(TextPageService $textPageService, $name) {
        return response()->json($textPageService->getPageByName($name));
    }
    public function test () {
        $date = Carbon::now()->addDays(-1)->format('Y-m-d');
        Order::query()->whereDate('date_time', $date)->get()->each(function ($order) {
            $order->user->notify(new RateCleanerNotification());
        });
    }
}
