<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CityService;
use App\Transformers\City\CityIndexTransformer;

class CityController extends Controller
{
    public function index(CityService $cityService)
    {
        $cities = $cityService->getAll();

        $fractal = fractal($cities, new CityIndexTransformer());
        $fractal->addMeta([
            'count' => $cities->count()
        ]);

        return $fractal->respond();
    }
}
