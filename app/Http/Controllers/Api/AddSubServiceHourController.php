<?php

namespace App\Http\Controllers\Api;

use App\Services\AddSubServiceHourService;
use App\Transformers\AddSubServiceHour\AddSubServiceHourTransformer;
use App\Http\Controllers\Controller;

class AddSubServiceHourController extends Controller
{
    public function index (AddSubServiceHourService $addSubServiceHourService)
    {
        $items = $addSubServiceHourService->getAll();

        $fractal = fractal($items, new AddSubServiceHourTransformer());

        return $fractal->respond();
    }
}
