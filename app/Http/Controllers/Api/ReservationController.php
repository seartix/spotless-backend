<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ReservationRequest;
use App\Models\AdminUser;
use App\Notifications\ReservationCleanerNotification;
use App\Notifications\ReservationNotification;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ReservationController extends Controller
{
    public function reservation(ReservationRequest $request){
        $data = $request->only([
            'cleaner_id',
            'email',
            'clean_date',
            'clean_time',
            'name',
            'surname',
            'phone',
            'address',
            'house_number',
            'city_id',
            'province_id'
            ]);
        $admin = AdminUser::query()->where('username', 'admin')->first();
        $admin->notify(new ReservationNotification($data));
        $cleaner = User::query()->where('id', $data['cleaner_id'])->first();
        $cleaner->notify(new ReservationCleanerNotification($data));
        return response('success');
    }
}
