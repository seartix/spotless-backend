<?php

namespace App\Http\Controllers\Api;

use App\Services\ContentService;
use App\Http\Controllers\Controller;
use App\Transformers\Content\ContentViewModelIndexTransformer;

class ContentController extends Controller
{
    public function index (ContentService $contentService)
    {
        $contents = $contentService->getContent();

        $fractal = fractal($contents, new ContentViewModelIndexTransformer());

        return $fractal->respond();
    }
}
