<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    public function upload(Request $request)
    {
        $path = $request->file('file')->store('file', 'admin');

        return $path;
    }
}
