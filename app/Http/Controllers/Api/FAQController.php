<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 20.10.17
 * Time: 13:17
 */

namespace App\Http\Controllers\Api;


use App\Services\FAQService;
use App\Transformers\FAQ\FAQIndexTransformer;

class FAQController
{
    public function faqIndex(FAQService $FAQService) {

        $faqModel = $FAQService->getCategoriesAndQuestions();

        return $faqModel;
    }

}