<?php

namespace App\Http\Controllers\Api;

use App\Services\ProvinceService;
use App\Transformers\Province\ProvinceIndexTransformer;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function index(ProvinceService $provinceService)
    {
        $provinces = $provinceService->getAll();

        $fractal = fractal($provinces, new ProvinceIndexTransformer());
        $fractal->addMeta([
            'count' => $provinces->count()
        ]);

        return $fractal->respond();
    }
}
