<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 29.09.17
 * Time: 16:17
 */

namespace App\Http\Controllers\Api;


use App\Services\SecretPlacesService;

class SecretPlacesController
{
    public function index(SecretPlacesService $secretPlacesService) {
        $places = $secretPlacesService->getAll();

        return $places;
    }
}