<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Mail\CreateSubscription;
use App\Mail\SubscribeLetter;
use App\Models\EmailSubscibers;
use App\Models\SubscribeContent;
use App\Services\SubscriptionService;
use App\Transformers\Subscription\SubscriptionIndexTransfromer;
use App\User;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index (SubscriptionService $subscriptionService)
    {
        $subscriptions = $subscriptionService->getAll();

        $fractal = fractal($subscriptions, new SubscriptionIndexTransfromer());

        return $fractal->respond();
    }

    public function createSubscribsion(Request $request,User $user)
    {
        $user_id = $request->only('user_id');
        $user = $user->whereId($user_id)->first();
        if($user->subscriber===1){
            $user->update(['subscriber'=>false]);
        }
        else{
            $user->update(['subscriber'=>true]);
        }
        return response()->json(['success'=>$user->subscriber]);
    }

    public function createSubscriptionByEmail(Request $request,EmailSubscibers $subscibers)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:email_subscribers,email',
        ]);
        $template = SubscribeContent::find(2);
        $user = User::find(['email'=>$request->input('email')]);
        $data = $user;
        if($user){
            $user->update(['subscriber'=>true]);
        }
        else{
            $data = $subscibers->create(['email'=>$request->input('email')]);
        }

        Mail::to($data->email)
            ->queue(new CreateSubscription($data->email,$template->content));
        if($data){
            return response()->json(['success'=>true]);
        }
        return response()->json(['success'=>false]);
    }
}
