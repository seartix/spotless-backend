<?php

namespace App\Http\Controllers\Api;

use App\Services\InsuranceService;
use App\Http\Controllers\Controller;
use App\Transformers\Insurance\InsuranceIndexTransformer;

class InsuranceController extends Controller
{
    public function index(InsuranceService $insuranceService)
    {
        $insurances = $insuranceService->getAll();

        $fractal = fractal($insurances, new InsuranceIndexTransformer());
        $fractal->addMeta([
            'count' => $insurances->count()
        ]);

        return $fractal->respond();
    }
}
