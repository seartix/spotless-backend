<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\ConfirmEmployerRequest;
use App\Services\EmployerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateEmployerRequest;

class EmployerController extends Controller
{
    public function create(CreateEmployerRequest $request, EmployerService $employerService)
    {
        $data = $request->only(['name', 'surname', 'email',
            'phone', 'fiscal_code', 'gender',
            'iva_code', 'skill', 'add_info', 'cv_path', 'avatar_path', 'identificate_path', 'reference_path', 'languages']);

        if ($employerService->create($data)) {
            return response('success');
        }
        return response('error to create');
    }
    public function confirmEmployer (ConfirmEmployerRequest $request, EmployerService $employerService) {
        $data = $request -> only ('id');
        $employerService->confirmEmployer($data);
        return response()->json([
            'message' => trans('admin::lang.succeeded')
        ]);
    }
}
