<?php

namespace App\Http\Controllers;

use App\Mail\SubscribeLetter;
use App\Models\EmailSubscibers;
use App\Models\SubscribeContent;
use App\Services\UserService;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SubscribeController extends Controller
{
    public function index(Request $request)
    {
        $users = User::query()->whereNotNull('email')->where('subscriber',true)->get()->toArray();
        $emailSubscribers = EmailSubscibers::select('email')->get()->toArray();
        $users = array_merge($users,$emailSubscribers);
         return view('subscribe',['users'=>$users,'templates'=>SubscribeContent::get()]);
    }

    public function store(Request $request)
    {
        $template = SubscribeContent::find($request->input('template'));
        $users = User::query()->select('email')->whereNotNull('email')->where('subscriber',true)->get()->toArray();
        $emailSubscribers = EmailSubscibers::select('email')->get()->toArray();
        $users = array_merge($users,$emailSubscribers);
        if($users && $template){
            foreach ($users as $user){
                Mail::to($user)
                    ->queue(new SubscribeLetter($template->content,$user));
            }

//            Mail::bcc($users)
//                ->queue(new SubscribeLetter($template->content));

        }

        return redirect()->back();
    }

    public function unsubscribe($id)
    {
       $user = User::find($id);
       if($user){
           $user->update(['subscriber'=>false]);
       }
       return redirect('https://spotlessitalia.com/#/');
    }

    public function unsubscribeEmail($id)
    {
        $user = EmailSubscibers::find($id);
        if($user){
            $user->delete();
        }
        return redirect('https://spotlessitalia.com/#/');
    }


}
