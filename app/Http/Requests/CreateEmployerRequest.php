<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmployerRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $base_rules = [
            'avatar_path' => 'required',
            'cv_path' => 'required',
            'email' => 'required|string',
            'fiscal_code' => 'required|string',
            'gender' => 'required',
            'identificate_path' => 'required',
            'languages' => 'required|string',
            'name' => 'required|string',
            'phone' => 'required|string',
            'reference_path' => 'required',
            'skill' => 'required|numeric',
            'surname' => 'required|string'
        ];
        if ($this->required_iva) {
            $base_rules = array_merge($base_rules, ['iva_code' => 'required']);
        }
        return $base_rules;
    }
    public function messages()
    {
        return [
            'avatar_path.required' => 'Il campo foto è obbligatorio.',
            'cv_path.required' => 'Il campo curriculum è obbligatorio.',
            'email.required' => 'Il campo email è obbligatorio.',
            'identificate_path.required' => 'Il campo documento d\'identita è obbligatorio.',
            'languages.required' => 'Il campo lingue parlate è obbligatorio.',
            'reference_path.required' => 'Il campo referenze è obbligatorio.',
            'skill.required' => 'Il campo anni d\'esperienza è obbligatorio.',
            'iva_code.required' => 'Il campo partiva iva è obbligatorio.'
        ];
    }
}
