<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cleaner_id' => 'required',
            'name' => 'required|string',
            'surname' => 'required|string',
            'phone' => 'required|string',
            'address' => 'required|string',
            'house_number' => 'required|string',
            'city_id' => 'required|integer',
            'province_id' => 'required|integer',
            'clean_date' => 'required|date',
            'clean_time' => 'required|string',
        ];
    }
}
