<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => [
              'numeric'
            ],
            'token' => [
                'required',
                'string',
            ],

            'type' => [
                'required',
                'in:cards,paypal,no',
            ],

            'date' => [
                'required',
                'date_format:Y-m-d',
            ],
            'time' => [
                'required',
                'date_format:H:i',
            ],

            'city_id' => [
                'required',
                'numeric',
                'exists:cities,id',
            ],

            'realty_type_id' => [
                'required',
                'numeric',
                'exists:realty_types,id',
            ],

            'cleaner_id' => [
                'nullable',
                'exists:users,id',
            ],

            'subscription_id' => [
                'nullable',
                'numeric',
                'exists:subscriptions,id',
            ],

            'meters' => [
                'required',
                'numeric',
                'min:0',
            ],

            'addSubServices' => [
                'array',
            ],
            'addSubServices.*' => [
                'numeric',
                'exists:add_services,id',
            ],

            'surfaces' => [
                'array',
            ],
            'surfaces.*' => [
                'numeric',
                'exists:surfaces,id',
            ],

            'subConditions' => [
                'array',
            ],
            'subConditions.*' => [
                'numeric',
                'exists:sub_conditions,id',
            ],

            'insurances' => [
                'array',
            ],
            'insurances.*' => [
                'numeric',
                'exists:insurances,id',
            ],

            'comment' => [
                'nullable',
                'string',
            ],

            'buy_clear_kit' => [
                'required',
                'boolean',
            ],
           // 'is_created_with_subscription' => [
           //     'required'
            //]
        ];
    }
}
