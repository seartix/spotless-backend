<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'clean_format_date' => [
                'required',
                'date_format:d.m.Y',
            ],
            'clean_format_time' => [
                'required',
            ],

            'city_id' => [
                'required',
                'numeric',
                'exists:cities,id',
            ],

            'realty_type_id' => [
                'required',
                'numeric',
                'exists:realty_types,id',
            ],

            'cleaner_id' => [
                'nullable',
                'exists:users,id',
            ],

            'subscription_id' => [
                'nullable',
                'numeric',
                'exists:subscriptions,id',
            ],

            'meters' => [
                'required',
                'numeric',
                'min:0',
            ],

            'add_services' => [
                'array',
            ],
            'add_services.*' => [
                'numeric',
                'exists:add_services,id',
            ],

            'add_surfaces' => [
                'array',
            ],
            'add_surfaces.*' => [
                'numeric',
                'exists:surfaces,id',
            ],

            'sub_conditions' => [
                'array',
            ],
            'sub_conditions.*' => [
                'numeric',
                'exists:sub_conditions,id',
            ],

            'insurances' => [
                'array',
            ],
            'insurances.*' => [
                'numeric',
                'exists:insurances,id',
            ],

            'comment' => [
                'nullable',
                'string',
            ],

            'buy_clear_kit' => [
                'required',
                'boolean',
            ],
            'address' => [
                'nullable',
                'string'
            ]
        ];
    }
}
