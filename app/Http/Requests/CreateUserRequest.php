<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'type' => 'required|string|in:persona,azienda',
            'password' => 'required|string|min:6',
            'email' => 'required|email|max:255||unique:users,email,{$id},id,deleted_at,NULL',
            'fiscal_code' => 'required|string|size:16',
            'city_id' => 'required|numeric|exists:cities,id',
            'province_id' => 'required|numeric|exists:provinces,id',
            'domofon' => 'nullable',
            'domofon_text' => 'nullable',
            'address' => 'required|string|max:255',
        ];

        switch ($this->type) {
            case 'persona':
                $rules = array_merge($rules, [
                    'name' => 'required|string|max:255',
                    'surname' => 'required|string|max:255',
                    'gender' => 'required|string|in:male,female,not important',
                    'date_birthday' => 'required|date|date_format:d/m/Y',
                    'phone' => 'required|numeric',
                    'house_number' => 'required|string|max:255',
                    'post_index' => 'required|numeric'
                ]);
                break;
            case 'azienda':
                $rules = array_merge($rules, [
                    'firm_name' => 'required|string|max:255',
                    'post_index' => 'required|numeric',
                ]);
                break;
        }

        return $rules;
    }
}
