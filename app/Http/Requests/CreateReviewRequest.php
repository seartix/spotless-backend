<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cleaner_id' => [
                'required',
                'numeric',
                'exists:users,id',
            ],
            'text' => [
                'required',
                'string',
                'max:150'
            ],
            'rating' => [
                'required',
                'numeric'
            ],
            'recommend' => [
                'required',
                'boolean'
            ]
        ];
    }
    public function messages()
    {
        return [
            'cleaner_id.required' => 'Il campo foto è obbligatorio.',
            'text.required' => 'Si prega di scrivere qualche recensione.',
            'text.max' => 'La tua recensione non può essere più di :max simboli.',
            'rating.required' => 'Il campo email è obbligatorio.',
            'recommend.required' => 'Il campo documento d\'identita è obbligatorio.',

        ];
    }
}
