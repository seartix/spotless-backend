<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CalculatePriceFromHoursRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'hours' => [
                'required',
                'numeric',
                'min:0',
            ],
            'realty_type_id' => [
                'required',
                'numeric',
                Rule::exists('realty_types', 'id'),
                Rule::exists('realty_types_meters_hours', 'realty_type_id'),
            ]
        ];
    }
}
