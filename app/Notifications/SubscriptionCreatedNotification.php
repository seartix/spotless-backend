<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $user_name;
    private $subscritption;

    /**
     * SubscriptionCreatedNotification constructor.
     * @param $user_name
     * @param $subscritption
     */
    public function __construct($user_name, $subscritption)
    {
        $this->user_name = $user_name;
        $this->subscritption = $subscritption;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Attivazione abbonamento')
            ->greeting("Gentile utente $this->user_name,")
            ->line("Il tuo abbonamento $this->subscritption è stato attivato correttamente, da ora riceverai il servizio di pulizia continuativo nel giorno e nell\'ora che hai stabilito e con l\'operatore Spotless che hai scelto.")
            ->line('Ti ricordiamo che è consigliabile essere presenti sul posto almeno per le prime prenotazioni, e che l\'abbonamento prevede lo svolgimento obbligatorio delle prime 4 pulizie.')
            ->line('Per maggiori informazioni o per cancellare o modificare il tuo abbonamento visita la tua area personale')
            ->action('Profilo', config('app.frontend-url').'/#/personal-area')
            ->attach(public_path('servizi.pdf'), [
            'as' => 'Servizi.pdf',
            'mime' => 'application/pdf'
            ]);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
