<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PaymentDoneNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Order
     */
    private $order;

    /**
     * PaymentDoneNotification constructor.
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order = $this->order;
        $user = $order->user;

        $payment_type = (strpos($order->type, 'paypal') !== false)? 'Carta di credito' : 'PayPal';

        return (new MailMessage)
            ->subject('Il tuo ordine è stato pagato')
            ->greeting('Gentile Utente, ')
            ->line("il tuo ordine $order->id è stato pagato.")
            ->line("$user->name $user->surname")
            ->line('tipo di pagamento: ' . $payment_type)
            ->line('prezzo: '.$order->amount)
            ->line('data di pagamento: '.$order->created_at->format('d/m/y'))
            ->line('Per modificare i parametri di pulizia vai in tuo Profilo ')
            ->action('Profilo', config('app.frontend-url').'/#/personal-area');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
