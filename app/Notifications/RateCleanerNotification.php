<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RateCleanerNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $cleaner_name;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($cleaner_name)
    {
        $this->cleaner_name = $cleaner_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Valuta il tuo addetto di pulizia!')
            ->line('Ti è piaciuto il lavoro di ' . $this->cleaner_name . '?')
            ->line('<img src="'. $this->cleaner_image .'">')
            ->line('Lascia una recensione, per Spotless il tuo parere è la priorità.')
            ->action('Profilo', config('app.frontend-url').'/#/personal-area');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
