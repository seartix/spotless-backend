<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserOrderCreatedNotification extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order = $this->order;
        $services = '';
        $condition = '';
        if($order->realty_type_id === 1){
            $order->addServices->each(function ($item) use (&$services) {
                $services.="$item->title\n";
            });
        }
        else{
            $order->AnswersCheckbox->each(function ($item) use (&$services) {

                $services.="$item->answer , \r\n";
            });
        }
        $order->addSubConditions->each(function ($item) use (&$condition) {
            $condition.="$item->title\n";
        });
        $claner_name = 'Non cleaner';
        if($order->cleaner_id){
            $claner_name = $order->cleaner->name;
        }
        $address = isset($order->address) ? $order->address : $order->user->address;

        return (new MailMessage)
            ->subject('Tuo ordine Spotless')
            ->greeting('Gentile Utente,')
            ->line('La tua prenotazione è andata a buon fine, e la tua richiesta di pagamento è stata autorizzata. ')
            ->line('Ecco il riepilogo del tuo ordine')
            ->line('DATA:'.$order->getDateForMail())
            ->line('ADDETTO Di PULIZIA:'.$claner_name)
            ->line('METRI: '.$order->meters)
            ->line('ORE: '.$order->hours)
            ->line('INDIRIZZO: ' . $address)
            ->line("EXTRA SERVIZI:\n$services")
            ->line("SUPERFICI:\n$condition")
            ->line('COMMENTO: '.$order->comment)
            ->attach(public_path('servizi.pdf'), [
            'as' => 'Servizi.pdf',
            'mime' => 'application/pdf'
            ]);

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
