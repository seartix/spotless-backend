<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderCanceledNotification extends Notification
{
    use Queueable;
    private $order;
    private $isCleaner;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $isCleaner = false)
    {
        $this->order = $order;
        $this->isCleaner = $isCleaner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->line('Il tuo ordine '.$this->order->id.' per il '.$this->order->getDateForMail() .' '. explode(' ',$this->order->date_time)[1] .' è stato cancellato')
            ->line('Controlla i tuoi ordini nel profilo:');
        if ($this->isCleaner) {
            $mail->action('Profilo', config('app.frontend-url').'/#/personal-area');
        }
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
