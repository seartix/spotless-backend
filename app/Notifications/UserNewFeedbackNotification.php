<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UserNewFeedbackNotification extends Notification
{
    use Queueable;

    private $data;

    /**
     * AdminNewFeedbackNotification constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Richiesta dalla pagina Contatti')
            ->greeting('Ciao,')
            ->line('Grazie! La tua richiesta è stata inviata con successo. Attendi la nostra risposta che arriverà alla tua email. ')
            ->line('Nome: ' . $this->data['name'])
            ->line('Cognome: ' . $this->data['lastname'])
            ->line('Email: ' . $this->data['email'])
            ->line('Commento: ' . $this->data['comment']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
