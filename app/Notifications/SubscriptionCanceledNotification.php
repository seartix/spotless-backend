<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SubscriptionCanceledNotification extends Notification
{
    use Queueable;
    private $user_name;
    private $subscription;

    /**
     * SubscriptionCanceledNotification constructor.
     * @param $user_name
     * @param $subscription
     */
    public function __construct($user_name, $subscription)
    {
        $this->user_name = $user_name;
        $this->subscription = $subscription;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Gentile utente $this->user_name,")
            ->line("Ti comunichiamo che il tuo abbonamento $this->subscription è stato cancellato correttamente. Per maggiori informazioni visita la tua area personale")
            ->action('Profilo', config('app.frontend-url').'/#/personal-area');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
