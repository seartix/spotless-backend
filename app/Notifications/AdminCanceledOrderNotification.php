<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class AdminCanceledOrderNotification extends Notification
{
    use Queueable;
    private $isCleaner;
    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($order, $isCleaner = false)
    {
        $this->order = $order;
        $this->isCleaner = $isCleaner;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $mail = (new MailMessage)
            ->greeting('Salve,')
            ->line('ordine numero '.$this->order->id.' è stato cancellato '.$this->order->getDateForMail() .' '. explode(' ',$this->order->date_time)[1])
            ->line('Vedi i dettagli nel tuo pannello');
        $mail->action('Area amministrativa', config('app.backend-url'));
        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
