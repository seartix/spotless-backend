<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CleanerChangedNotification extends Notification
{
    use Queueable;
    private $order_id;
    private $cleaner_name;
    private $order_date;
    private $cleaner_image;

    /**
     * CleanerChangedNotification constructor.
     * @param $order_id
     * @param $cleaner_name
     * @param $order_date
     * @param $cleaner_image
     */
    public function __construct($order_id, $cleaner_name, $order_date, $cleaner_image)
    {
        $this->order_id = $order_id;
        $this->cleaner_name = $cleaner_name;
        $this->order_date = $order_date;
        $this->cleaner_image = $cleaner_image;
    }


    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->greeting("Ciao,")
            ->line('il tuo ordine '
                .$this->order_id
                .' per il '
                .$this->order_date
                .' è stato assegnato a '
                .$this->cleaner_name
                .' dall\'amministratore.')
            ->line('<img src="'. $this->cleaner_image .'">')
            ->line('Puoi vedere il profilo completo sul sito:')
            ->action('Addetti', env('FRONTEND_URL').'/#/employees');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
