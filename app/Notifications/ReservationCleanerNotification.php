<?php

namespace App\Notifications;

use App\Models\City;
use App\Models\Province;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ReservationCleanerNotification extends Notification implements ShouldQueue
{
    use Queueable;
    private $data;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $city = City::query()->where('id', $this->data['city_id'])->first();
        $province = Province::query()->where('id', $this->data['province_id'])->first();
        return (new MailMessage)
            ->subject('Ordine con addetto selezionato')
            ->greeting("Ciao,")
            ->line('è arrivato un’ordine nuovo per addetto '.$this->data['cleaner_id'])
            ->line('Nome: '.$this->data['name'])
            ->line('Cognome: '.$this->data['surname'])
            ->line('Numero di cellulare: '.$this->data['phone'])
            ->line('Email: '.$this->data['email'])
            ->line('Indirizzo dell\'ambiente da pulire'.$this->data['address'].'/'.$this->data['house_number'])
            ->line('Data di pulizia: '.$this->data['clean_date'].' '.$this->data['clean_time'])
            ->line('Città: '.$city->title)
            ->line('Provincia'. $province->title)
            ->line('Scopri maggiori informazioni nel tuo  ')
            ->action('Profilo', env('FRONTEND_URL', '').'/#/personal-area');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
