<?php

namespace App\Notifications;

use App\Models\Order;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderUpdatedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    private $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $order = $this->order;
        $services = '';
        $condition = '';
        $order->addServices->each(function ($item) use (&$services) {
            $services.="$item->title\n";
        });
        $order->addSubConditions()->each(function ($item) use (&$condition) {
            $condition.="$item->title\n";
        });
        $claner_name = 'Non selezionato';
        if($order->cleaner_id){
            $claner_name = $order->cleaner->name;
        }
        return (new MailMessage)
            ->subject('Aggiornamento ordine')
            ->greeting("Ciao,")
            ->line("Ordine $order->id è stato aggiornato")
            ->line('DATA: '.$order->getDateForMail())
            ->line('ADDETTO DI PULIZIA: '.$claner_name)
            ->line('METRI: '.$order->meters)
            ->line('ORE: '.$order->hours)
            ->line('INDIRIZZO: '.($order->address)? $order->address : $order->user->address)
            ->line("EXTRA SERVIZI:\n$services")
            ->line("SUPERFICI:\n$condition")
            ->line('COMMENTO: '.$order->comment)
            ->line('Scopri maggiori informazioni nel tuo  ')
            ->action('Profilo', config('app.frontend-url').'/#/personal-area');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
