<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class CleanerChangedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    private $order_id;
    private $cleaner_id;
    private $order_date;
    private $user_id;

    /**
     * CleanerChangedEvent constructor.
     * @param $order_id
     * @param $cleaner_id
     * @param $order_date
     * @param $user_id
     */
    public function __construct($order_id, $cleaner_id, $order_date, $user_id)
    {
        $this->order_id = $order_id;
        $this->cleaner_id = $cleaner_id;
        $this->order_date = $order_date;
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }



    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return mixed
     */
    public function getCleanerId()
    {
        return $this->cleaner_id;
    }

    /**
     * @return mixed
     */
    public function getOrderDate()
    {
        return $this->order_date;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
