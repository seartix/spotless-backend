<?php

namespace App\Events;

use App\Models\Employer;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class EmployeeCreatedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var Employer
     */
    private $employee;

    /**
     * EmployeeCreatedEvent constructor.
     * @param $employee
     */
    public function __construct($employee)
    {
        $this->employee = $employee;
    }

    /**
     * @return Employer
     */
    public function getEmployee()
    {
        return $this->employee;
    }
    /**
     * Create a new event instance.
     *
     * @return void
     */

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
