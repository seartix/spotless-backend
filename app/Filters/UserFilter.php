<?php

namespace App\Filters;


class UserFilter extends QueryFilter
{
    public function gender($gender) {
        if ($gender == 'all') {
        } else {
            $this->builder->where('gender', $gender);
        }
    }

    public function languages($languages) {
        $this->builder->whereHas('languages', function ($query) use ($languages) {
            $query->whereIn('title', $languages);
        });
    }
}