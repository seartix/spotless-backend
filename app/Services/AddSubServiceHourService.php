<?php

namespace App\Services;



use App\Models\AddServiceHour;
use Illuminate\Support\Collection;

class AddSubServiceHourService
{
    public function getAll () : Collection
    {
        $items = AddServiceHour::query()
            ->orderBy('meters', 'asc')
            ->get()
            ->map(function (AddServiceHour $addSubServiceHour) {
                return new \App\Entities\AddSubServiceHour($addSubServiceHour->id, $addSubServiceHour->meters, $addSubServiceHour->hours, $addSubServiceHour->add_service_id);
            });
        return $items;
    }
}