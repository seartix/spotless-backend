<?php

namespace App\Services;

use App\Entities\RealtyType as RealtyTypeEntity;
use App\Entities\RealtyTypeMeterHour;
use App\Models\AddService;
use App\Models\AddSubService;
use App\Models\AddSubServiceHour;
use App\Models\RealtyType;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class RealtyTypeService
{
    public function getAll(): Collection
    {
        $realtyTypes = RealtyType::query()
            ->get()
            ->map(function (RealtyType $realtyType) {
               return new RealtyTypeEntity($realtyType->id, $realtyType->title,$realtyType->icon,$realtyType->image,$realtyType->description,$realtyType->label_clean_type);
            });

        return $realtyTypes;
    }

    public function getPriceFromMeters(float $inputMeters, int $realtyTypeId, string $date, string $time)
    {
        $carbon = Carbon::createFromFormat('Y-m-d' ,$date);
        $now = new Carbon();
        $diff = $now->diffInDays($carbon);


        $realtyType = \App\Models\RealtyType::findOrFail($realtyTypeId);

        $hoursModel = $realtyType->realtyTypeMetersHours()
            ->byColumnMore('meters', $inputMeters)
            ->first();

        if ($hoursModel) {
            $price = $realtyType->price * $hoursModel->hours;
            if ($diff <= 2) {
                $price = ($realtyType->express_price * $hoursModel->hours);
            }
            return [
                'price' => round($price),
                'hours' => $hoursModel->hours
            ];

        }
        else {
            $hoursModel = $realtyType->realtyTypeMetersHours()
                ->byColumnLess('meters', $inputMeters)
                ->first();
            $price = $realtyType->price * $hoursModel->hours;
            if ($diff <= 2) {
                $price = $realtyType->express_price * $hoursModel->hours;
            }
            return [
                'price' => round($price),
                'hours' => $hoursModel->hours
            ];
        }
    }

    public function getPriceFromMetersWithoutDate(float $inputMeters, int $realtyTypeId)
    {
        $realtyType = \App\Models\RealtyType::findOrFail($realtyTypeId);
        $hoursModel = $realtyType->realtyTypeMetersHours()
            ->byColumnMore('meters', $inputMeters)
            ->first();
        if ($hoursModel) {
            $price = $hoursModel->add_price;
        }
        else{
            $hoursModel = $realtyType->realtyTypeMetersHours()
                ->byColumnLess('meters', $inputMeters)
                ->first();
            $price =  $hoursModel->add_price;
        }
        return [
            'price' => round($price),
            'hours' => $hoursModel->hours
        ];

    }
    public function getPriceFromHours(float $inputHours, int $realtyTypeId, string $date, string $time)
    {
        $carbon = Carbon::createFromFormat('Y-m-d' ,$date);
        $now = new Carbon();
        $diff = $now->diffInDays($carbon);

        $realtyType = \App\Models\RealtyType::findOrFail($realtyTypeId);

        $hoursModel = $realtyType->realtyTypeMetersHours()
            ->byColumnMore('hours', $inputHours)
            ->first();

        if ($hoursModel) {

            $price = $realtyType->price * $hoursModel->hours;

            if ($diff <= 2) {
                $price = $realtyType->express_price * $hoursModel->hours;
            }

            return [
                'price' => $price,
                'meters' => $hoursModel->meters
            ];
        }
        else {
            $hoursModel = $realtyType->realtyTypeMetersHours()
                ->byColumnLess('hours', $inputHours)
                ->first();

            $price = $realtyType->price * $hoursModel->hours;

            if ($diff <= 2) {
                $price = $realtyType->express_price * $hoursModel->hours;
            }

            return [
                'price' => round($price),
                'meters' => $hoursModel->meters
            ];
        }
    }



    public function getPriceFromHoursWithoutDate(float $inputHours, int $realtyTypeId)
    {
       $realtyType = \App\Models\RealtyType::findOrFail($realtyTypeId);

        $hoursModel = $realtyType->realtyTypeMetersHours()
            ->byColumnMore('hours', $inputHours)
            ->first();

        if ($hoursModel) {
            $price = $realtyType->price * $hoursModel->hours;
        }
        else {
            $hoursModel = $realtyType->realtyTypeMetersHours()
                ->byColumnLess('hours', $inputHours)
                ->first();
            $price = $realtyType->price * $hoursModel->hours;
        }
        return [
            'price' => round($price),
            'meters' => $hoursModel->meters
        ];
    }
    public function getAllRealtyTypeMeterHours()
    {
        return RealtyType::query()
            ->with('realtyTypeMetersHours')
            ->get()
            ->map(function (RealtyType $realtyType) {
                return $realtyType->realtyTypeMetersHours->map(function ($realtyTypeMetersHours) use ($realtyType) {
                    return new RealtyTypeMeterHour($realtyType->title, $realtyType->price, $realtyTypeMetersHours->meters, $realtyTypeMetersHours->hours);
                });
            })
            ->collapse();
    }
}
