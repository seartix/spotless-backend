<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 19.02.18
 * Time: 13:11
 */

namespace App\Services;


use App\Models\TextPage;

class TextPageService
{
    public function getPageByName ($name) {
         $page = TextPage::query()->where('title', $name)->pluck('text', 'title');
         return $page;
    }
}