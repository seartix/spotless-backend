<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 29.09.17
 * Time: 16:23
 */

namespace App\Services;


use App\Models\SecretPlace;
use Illuminate\Database\Eloquent\Collection;

class SecretPlacesService
{
    public function getAll() : Collection{
        $places = SecretPlace::query()->select('id','title')->get();
        return $places;
    }
}