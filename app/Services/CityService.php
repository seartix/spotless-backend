<?php

namespace App\Services;

use App\Entities\City;
use Illuminate\Support\Collection;

class CityService
{
    /*
     * Collection|City[]
     */
    public function getAll(): Collection
    {
        $cities = \App\Models\City::query()
            ->select(['id', 'title'])
            ->get()
            ->map(function (\App\Models\City $city) {
                return new City($city->id, $city->title);
            });

        return $cities;
    }
}