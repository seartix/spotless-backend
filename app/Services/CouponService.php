<?php

namespace App\Services;
use App\Models\Coupon;
use App\User;
use Carbon\Carbon;

class CouponService
{
    public function apply(string $couponCode, int $price, int $user_id)
    {
        $date = Carbon::now()->setTime(0,0,0);
        $coupon = Coupon::query()
            ->where('code', $couponCode)
            ->where('date_end', '>=', $date)
            ->first();
        if (!$coupon) {
            return 'coupon not found';
        }
        if ($coupon->price_from <= $price) {
            if ($coupon->count_using == 1) {
                if ($coupon->users()->get()->count() > 0) {
                    return 'coupon already in user';
                }
            }

            $user = User::query()->find($user_id);
            $user->coupons()->attach([$coupon->id]);
            return $coupon;
        }
        return 'price is less then needed to apply coupon';
    }
}