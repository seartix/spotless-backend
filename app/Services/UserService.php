<?php

namespace App\Services;

use App\DTOs\CreateUserDto;
use App\Entities\User;
use App\Events\PasswordResetEvent;
use App\Filters\UserFilter;
use App\Models\AvailableDateForCleaner;
use App\Models\CleanerReview;
use App\Models\Order;
use Carbon\Carbon;
use Faker\Provider\DateTime;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;


class UserService
{
    /**
     * @param CreateUserDto $clientDto
     */
    public function create(CreateUserDto $createUserDto)
    {
        try {
            $user = \App\User::create([
                'name' => $createUserDto->getName(),
                'email' => $createUserDto->getEmail(),
                'password' => $createUserDto->getPassword(),
                'role' => $createUserDto->getRole(),
                'type' => $createUserDto->getType(),
                'fiscal_code' => $createUserDto->getFiscalCode(),
                'surname' => $createUserDto->getSurname(),
                'gender' => $createUserDto->getGender(),
                'date_birthday' => $createUserDto->getDateBirthday(),
                'phone' => $createUserDto->getPhone(),
                'address' => $createUserDto->getAddress(),
                'house_number' => $createUserDto->getHouseNumber(),
                'city_id' => $createUserDto->getCityId(),
                'province_id' => $createUserDto->getProvinceId(),
                'code' => $createUserDto->getCode(),
                'firm_name' => $createUserDto->getFirmName(),
                'post_index' => $createUserDto->getPostIndex(),
                'domofon' => $createUserDto->getDomofon(),
                'domofon_text' => $createUserDto->getDomofonText(),
                'subscriber'=>$createUserDto->getisSubscribe()
            ]);
            return new User($user->id, $user->name, $user->email, $user->role,
                $user->password, $user->subscription_id, $user->insurances, $user->type, $user->fiscal_code,
                $user->surname, $user->gender, $user->date_birthday,
                $user->phone, $user->address, $user->house_number,
                $user->city_id, $user->province_id, $user->code,
                $user->domofon, $user->domofon_text, $user->firm_name, $user->post_index,$user->subscribe);
        } catch (\Exception $exception) {
            throw new \Exception($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return User
     */
    public function getCurrent(): ?User
    {
        $user = auth()->guard('api')->user();

        return $user === null ? null : new User($user->id, $user->name, $user->email, $user->role,
            $user->password, $user->subscription_id, $user->insurances, $user->type, $user->fiscal_code,
            $user->surname, $user->gender, $user->date_birthday,
            $user->phone, $user->address, $user->house_number,
            $user->city_id, $user->province_id, $user->code, $user->domofon, $user->domofon_text,
            $user->firm_name, $user->post_index,$user->subscriber,$user->facebook_id,$user->instagram_id,$user->google_id);
    }

    public function resetPassword($email)
    {
        $user = \App\User::query()->where('email', $email)->first();
        if ($user) {
            $token = str_random(40);
            event(new PasswordResetEvent($token, $user));
            return true;
        } else {
            return null;
        }
    }

    /**
     * @param array $data
     */
    public function chooseCleaners(array $data)
    {
        $date = $data['date'];
        $time = $data['time'];
        $realty_type = $data['realty_type'];
        $hours = $data['hours'];

        $addSeriviceIds = $data['addSubSeriviceIds'] ?? [];
        $addSubConditionIds = $data['addSubConditionIds'] ?? [];

        $cleaners = \App\User::cleaners()
            ->with('forbiddenAddServices', 'forbiddenAddSubConditions', 'cleanerOrders')

            ->cleaners()

            ->whereHas('forbiddenAddServices', function ($query) use ($addSeriviceIds) {
                $query->whereIn('id', $addSeriviceIds);
            }, '<', 1)

            ->whereHas('forbiddenAddSubConditions', function ($query) use ($addSubConditionIds) {
                $query->whereIn('id', $addSubConditionIds);
            }, '<', 1)

            ->whereHas('availableDates', function ($query) use ($date, $time) {
                $query
                    ->where('date', $date)
                    ->where('time_start', '<=', $time)
                    ->where('time_end', '>=', $time);
            })
            ;

        if ($realty_type == 2) {
            $cleaners->where('can_office', 1);
        } else {
            $cleaners->where('can_home', 1);
        }

        if (in_array(28, $addSeriviceIds)) {
            $cleaners
                ->whereNotNull('can_iron_clothes')
                ->where('can_iron_clothes', true);
        }

        $cleanerStartTime = Carbon::createFromFormat('Y-m-d H:i', "{$date} {$time}");
        $cleanerEndTime = $cleanerStartTime->copy()->addHours($hours);

        $cleaners->whereHas('cleanerOrders', function ($query) use ($cleanerStartTime, $cleanerEndTime) {
            $query
                ->where(function ($builder) use ($cleanerStartTime, $cleanerEndTime) {
                    $builder->where('date_time', '>', $cleanerStartTime);
                    $builder->where('date_time', '<', $cleanerEndTime);
                })
                ->orWhere(function ($builder) use ($cleanerStartTime, $cleanerEndTime) {
                    $builder->where('end_date_time', '>', $cleanerStartTime);
                    $builder->where('end_date_time', '<', $cleanerEndTime);
                });
        }, '==', 0);

        return $cleaners->get();
    }

    /**
     * @param int $user_id
     * @param int $cleaner_id
     * @param string $text
     */
    public function createReview(int $user_id, int $cleaner_id, string $text, int $rating, bool $recommend)
    {
        CleanerReview::create([
            'user_id' => $user_id,
            'cleaner_id' => $cleaner_id,
            'text' => $text,
            'rating' => $rating,
            'show' => 0,
            'recommend' => $recommend
        ]);
    }

    /**
     * @param int $cleaner_id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getReviews(?int $cleaner_id)
    {
        $cleanerReviewsQuery = CleanerReview::query()
            ->with('cleaner', 'user');
        $user = \App\User::query()->where('id', $cleaner_id)->first();
        if ($user) {
            if ($user->role == 'cleaner') {
                $cleanerReviewsQuery->where('show', 1)->where('cleaner_id', $cleaner_id);

            } else {
                $cleanerReviewsQuery->where('user_id', $cleaner_id);
            }
        }

        return $cleanerReviewsQuery->get();
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getCleaners(): Collection
    {
        return \App\User::cleaners()->pluck('id', 'name');
    }

    public function getUserCleaners(int $id): Collection
    {
        return Order::where('user_id', $id)->where('cleaner_id', '!=', NULL)->get()->map(function (Order $order) {
            return $order->cleaner()->first();
        })->pluck('id', 'name');
    }

    public function setPassword($token, $password)
    {
        try {
            $email = DB::table('password_resets')
                ->where('token', $token)
                ->first()
                ->email;
            $user = \App\User::where('email', $email)->first();
            $user->password = $password;
            if ($user->save()) {
                DB::table('password_resets')
                    ->where('email', $email)
                    ->delete();
                return true;
            }
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getAllCleaners(UserFilter $userFilter): Collection
    {
        return \App\User::filter($userFilter)->cleaners()->with(['reviews' => function ($query) {
            $query->where('show', 1);
        }])->get();
    }

    public function getAvailableDatesForCleaner()
    {
        $result = [];
        $dates = \App\User::query()
            ->where('role','cleaner')
            ->with('availableDates')
            ->get()
            ->map(function ($item) {
                return [
                    'user'=>$item,
                    'dates'=>$item['availableDates']->map(function ($date){
                        return [
                            'date' => explode(' ', $date['date'])[0],
//                            'time_start' => $date['time_start'],
//                            'time_end' => $date['time_end']
                        ];
                    })->toArray()
                ];
            })->toArray();
        $result = [];
        foreach ($dates as $index=>$user) {
            $result[$index]['user'] = $user;
            foreach ($user['dates'] as $key => $date) {
                $res = [];
                array_map(function($item) use (&$res){
                    $res = $item['time'];
                },$this->getAvailableTimeFromCleaner($date['date'], 2));
                if (count($res) > 1) {
//                    unset  ($dates[$index]['dates'][$key]);
//                    $dates[$index]['dates'] = array_values($dates[$index]['dates']);
                    $result[$index]['dates'][$key]['date'] = $date['date'];
                }
            }
            $result[$index]['dates'] = array_values( $result[$index]['dates'] ?? []);
        }
        if ($result) {
            return $result;
        } else return null;
    }

    public function getAvailableTimeFromCleaner($dateTime, $duration)
    {
        $result = [];
        $cleaners = \App\User::query()->where('role','cleaner')->get();
        foreach ($cleaners as $key => $cleaner){
            $cleaner_id = $cleaner->id;

            $startEnd = AvailableDateForCleaner::query()
                ->where('cleaner_id', $cleaner_id)
                ->whereDate('date', $dateTime)
                ->select(['time_start', 'time_end'])
                ->get()->map(function ($item) {
                    return [
                        'time_start' => $item['time_start'],
                        'time_end' => $item['time_end']
                    ];
                })->first();

            $skips = Order::select(['date_time', 'end_date_time'])
                ->where('cleaner_id', $cleaner_id)
                ->whereDate('date_time', $dateTime)
                ->get()
                ->map(function ($item) {
                    $start = new \DateTime($item->date_time);
                    $end = new \DateTime($item->end_date_time);
                    return [
                        $start->format('H:i'),
                        $end->format('H:i')
                    ];
                })->sortBy(0);

            $timeArr = [];
            $excludeTimeArr = [];

            if ($startEnd !== null && count($startEnd) > 0) {
                $startDay = $startEnd['time_start'];
                $endDay = $startEnd['time_end'];
                $start = $startDay;
                foreach ($skips as $skip) {
                    $exclude_start = $skip[0];
                    $exclude_end = $skip[1];
                    $intEnd = new Carbon($exclude_end);
                    $intEnd = $intEnd->addHours(1);
                    $exclude_end = $intEnd->format('H:i');
                    $intEnd = $intEnd->hour;

                    $intStart = new Carbon($exclude_start);
                    $intStart = $intStart->subHour(1);
                    $exclude_start = $intStart->format('H:i');
                    $intStart = $intStart->hour;
                    if ($intEnd - $intStart > $duration) {
                        $excludeTimeArr = array_merge($excludeTimeArr, $this->fillTime($exclude_start, $exclude_end));
                    }
                }
                $endDay = new Carbon($endDay);
                $endDay = $endDay->addHours(($duration * -1));
                $intEnd = $endDay->hour;
                $intStart = new Carbon($start);
                $intStart = $intStart->hour;
                if ($intEnd - $intStart > $duration) {
                    $timeArr = array_merge($timeArr, $this->fillTime($start, $endDay->format('H:i')));
                    if(isset($excludeTimeArr))
                    $timeArr = array_values(array_diff($timeArr,$excludeTimeArr));
                    $result[$key]['cleaner'] = $cleaner;
                    $result[$key]['time'] = $timeArr;
                }
            }
        }
        return $result ?? [];
    }

    private function fillTime($start, $end)
    {
        $timeArr = [];
        $start = new Carbon($start);
        $end = new Carbon($end);
        for ($i = $start; $i <= $end; $i->addMinutes(60)) {
            array_push($timeArr, $i->format('H') . ':00');
//            if ($start != $end) {
//                array_push($timeArr, $i->format('H') . ':30');
//            }
        }
        return $timeArr;
    }

    public function setTimeScheduleForCleaner(int $cleanerId, array $data)
    {

        $cleaner = \App\User::query()->find($cleanerId);
        $count = 20;
        $europeDates = [1,2,3,4,5,6,0];
        $result = [];

        foreach ($data['days'] as $key => $value) {
            if ($value) {
                $newDay = Carbon::parse('2019-06-20')->next($europeDates[$key]);

                for ($i = 0; $i < $count; $i++) {
                    $day = $newDay->copy()->addWeek($i);

                    $from = $data['from'][$key];
                    $to = $data['to'][$key];

                    $result[] = [
                        'day' => $day,
                        'from' => Carbon::parse($from),
                        'to' => Carbon::parse($to),
                    ];
                }
            }
        }

        AvailableDateForCleaner::query()
            ->where('cleaner_id', $cleanerId)
            ->where('manual', false)
            ->delete();

        foreach ($result as $key => $value) {
            AvailableDateForCleaner::create([
                'cleaner_id' => $cleanerId,
                'time_start' => $value['from'],
                'time_end' => $value['to'],
                'date' => $value['day'],
                'manual' => false,
            ]);
        }
    }

    public function getTimeScheduleForCleaner(int $cleanerId)
    {
        $cleaner = \App\User::query()->find($cleanerId);
        $dates = AvailableDateForCleaner::query()
            ->where('cleaner_id', $cleanerId)
            ->where('manual', false)
            ->get();
        $daysOfweeek = [];
        foreach ($dates as $date) {
            $dt = Carbon::parse($date->date);
            $daysOfweeek [$dt->dayOfWeek] = [
                'start'=> Carbon::parse($date->time_start)->format('H:i'),
                'end'=>Carbon::parse($date->time_end)->format('H:i'),
            ];

        }
        return $daysOfweeek;
    }

}
