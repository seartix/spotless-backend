<?php
namespace App\Services;

use App\Models\City;
use App\Models\Employer;
use App\Models\Province;
use App\User;

class EmployerService
{
    public function create (array $data)
    {
        return Employer::create($data);
    }

    public function confirmEmployer($data)
    {
        $employer = Employer::where('id' , $data['id'])->first();
        $gender = $employer->gender == 'maschio'? 'male' : 'female';
        $province_id = Province::all()->first()->id;
        $city_id = City::all()->first()->id;
        $data = [
            'name' => $employer->name,
            'surname' => $employer->surname,
            'email' => $employer->email,
            'fiscal_code' => $employer->fiscal_code,
            'gender' => $gender,
            'code' => $employer->iva_code,
            'skill' => $employer->skill,
            'phone' => $employer->phone,
            'address' => 'no address',
            'avatar' => $employer->avatar_path,
            'city_id' => $city_id,
            'province_id' => $province_id,
            'password' => '123456',
            'role' => 'cleaner'

        ];
        User::create($data);
    }
}