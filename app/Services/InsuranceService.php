<?php

namespace App\Services;

use App\Entities\Insurance;
use Illuminate\Support\Collection;

class InsuranceService
{
    public function getAll(): Collection
    {
        $insurances = \App\Models\Insurance::query()
            ->select(['id', 'title', 'description', 'text', 'price'])
            ->get()
            ->map(function (\App\Models\Insurance $insurance) {
                return new Insurance($insurance->id, $insurance->title, $insurance->description, $insurance->text, $insurance->price);
            });

        return $insurances;
    }
}