<?php

namespace App\Services;


use App\Models\Subscription;
use Illuminate\Support\Collection;

class SubscriptionService
{
    public function getAll () : Collection
    {
        $subscriptions = Subscription::query()
            ->with('addServices')
            ->get()
            ->map(function (Subscription $subscription) {
                return new \App\Entities\Subscription(
                    $subscription->id,
                    $subscription->title,
                    $subscription->description,
                    $subscription->price,
                    $subscription->addServices,
                    $subscription->type,
                    $subscription->days,
                    $subscription->additional_list,
                    $subscription->buy_clear_kit,
                    $subscription->frequency
                );
            });

        return $subscriptions;
    }
}
