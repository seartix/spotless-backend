<?php

namespace App\Services;


use App\Models\AboutUsContent;
use App\Models\AdditionalContent;
use App\Models\ButtonContent;
use App\Models\ContactPage;
use App\Models\PartnerContent;
use App\Models\PostCode;
use App\Models\Review;
use App\Models\StepContent;
use App\Models\TextContent;
use App\ViewModels\ContentIndexViewModel;

class ContentService
{
    /**
     * @return ContentIndexViewModel
     */
    public function getContent () : ContentIndexViewModel
    {
        $stepContents = StepContent::query()
            ->get()
            ->map(function (StepContent $stepContent) {
                return [
                    'text' => $stepContent->text,
                    'description' => $stepContent->description,
                    'icon' => env('APP_URL') . '/storage/' . $stepContent->icon
                ];
            });
        $additionalContents = AdditionalContent::query()
            ->get()
            ->map(function (AdditionalContent $additionalContent) {
                return [
                    'short_text' => $additionalContent->short_text,
                    'text' => $additionalContent->text,
                    'short_title' => $additionalContent->short_title,
                    'title' => $additionalContent->title,
                    'icon' => env('APP_URL') . '/storage/' . $additionalContent->icon
                ];
            });
        $buttonContents = ButtonContent::query()
            ->get()
            ->map(function (ButtonContent $buttonContent) {
                return [
                    'link' => $buttonContent->link,
                    'title' => $buttonContent->title,
                    'text' => $buttonContent->text
                ];
            });
        $textContents = TextContent::query()
            ->get()
            ->mapWithKeys(function (TextContent $textContent) {
                return [
                    $textContent->key => [
                        'style'=>$textContent->style,
                        'value'=> $textContent->value
                    ]

                ];
            });
        $partnerContent = PartnerContent::query()
            ->get()
            ->map(function (PartnerContent $partnerContent) {
                return [
                    'title' => $partnerContent->title,
                    'icon' => env('APP_URL') . '/storage/' . $partnerContent->icon
                ];
            });

        $aboutUsContents = AboutUsContent::query()
            ->get()
            ->map(function (AboutUsContent $aboutUsContent) {
                return [
                    'text' => $aboutUsContent->text,
                    'image' => env('APP_URL') . '/storage/' . $aboutUsContent->image
                ];
            });

        $contactContents = ContactPage::query()
            ->get()
            ->map(function (ContactPage $contactPage) {
                return [
                    'text' => $contactPage->text,
                    'email' => $contactPage->email,
                    'phone' => $contactPage->phone,
                ];
            });
        $postCodes = PostCode::query()
            ->where('approved',true)
            ->get()
            ->mapWithKeys(function (PostCode $postCode) {
                return [
                    $postCode->post_code => $postCode->district
                ];
            });
        $reviews = Review::get();

        return new ContentIndexViewModel($stepContents, $additionalContents, $buttonContents, $partnerContent,
            $aboutUsContents, $contactContents, $textContents,$postCodes,$reviews);
    }
}
