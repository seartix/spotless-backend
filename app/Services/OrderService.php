<?php

namespace App\Services;

use App\DTOs\CreateOrderDto;
use App\Models\ActiveSubscription;
use App\Models\AddService;
use App\Models\AdminUser;
use App\Models\BaseService;
use App\Models\BasicSet;
use App\Models\CheckboxService;
use App\Models\Condition;
use App\Models\FreeService;
use App\Models\Insurance;
use App\Models\Order;
use App\Models\SubCondition;
use App\Models\Surface;
use App\Notifications\AdminCanceledOrderNotification;
use App\Notifications\OrderCanceledNotification;
use App\Notifications\SubscriptionCreatedNotification;
use App\User;
use App\ViewModels\CreateOrderViewModel;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use phpDocumentor\Reflection\Types\Boolean;
use Stripe\Charge;
use Stripe\Stripe;
use App\Notifications\OrderNotification;
use App\Events\OrderCreatedEvent;

class OrderService
{
    public function copyOrder($order, $date_time,$token) : Order
    {
        return $order->getClone($date_time,$token);
    }
    public function calculateAddService($meters, $addServiceId){

        $addServicesAmount = 0;
        $addServicesHours = 0;

        $addService = AddService::query()->find($addServiceId);


        if ($addService->hour == 0)
        {
            $hourMeter = $addService
                ->hours()
                ->where('meters', '>=', $meters)
                ->orderBy('meters', 'ASC')
                ->first();

            if (!$hourMeter)
            {
                $hourMeter = $addService
                    ->hours()
                    ->orderBy('meters', 'DESC')
                    ->first();
            }

            $addServicesHours += $hourMeter->hours;
            $addServicesAmount += $addService->price * $hourMeter->hours;
        } else {
            $addServicesHours += $addService->hour;
            $addServicesAmount += $addService->price;
        }

        return [
            'hours' => $addServicesHours,
            'amount' => $addServicesAmount,
        ];
    }

    /**
     * @return CreateOrderViewModel
     */
    public function createOrderViewModel(): CreateOrderViewModel
    {
        $freeServices = FreeService::query()
            ->select(['title', 'text', 'icon', 'realty_type_id'])
            ->get()
            ->map(function (FreeService $freeService) {
                return [
                    'title' => $freeService->title,
                    'text' => $freeService->text,
                    'icon' => env('APP_URL') . '/upload/' . $freeService->icon,
                    'realty_type_id' => $freeService->realty_type_id,
                ];
            });

        $baseServices = BaseService::query()
            ->select(['title', 'text', 'image', 'realty_type_id'])
            ->get()
            ->map(function (BaseService $baseService) {
                return [
                    'title' => $baseService->title,
                    'text' => $baseService->text,
                    'image' => env('APP_URL') . '/upload/' .  $baseService->image,
                    'realty_type_id' => $baseService->realty_type_id,
                ];
            });

        $addServices = AddService::query()
            ->select(['id', 'title', 'icon', 'price', 'hour', 'realty_type_id'])
            ->get()
            ->map(function (AddService $addService) {
                return [
                    'id' => $addService->id,
                    'title' => $addService->title,
                    'icon' => env('APP_URL') . '/upload/' .  $addService->icon,
                    'price' => $addService->price,
                    'hour' => $addService->hour,
                    'realty_type_id' => $addService->realty_type_id
                ];
            });

        $surfaces = Surface::query()
            ->select(['id', 'title'])
            ->get()
            ->map(function (Surface $surface) {
                return [
                    'id' => $surface->id,
                    'title' => $surface->title
                ];
            });

        $conditions = Condition::query()
            ->select(['id', 'title', 'icon'])
            ->with('subConditions')
            ->get()
            ->map(function (Condition $condition) {
                return [
                    'id' => $condition->id,
                    'title' => $condition->title,
                    'icon' => env('APP_URL') . '/storage/' .  $condition->icon,
                    'subConditions' => $condition->subConditions
                        ->map(function (SubCondition $subCondition) {
                          return [
                              'id' => $subCondition->id,
                              'title' => $subCondition->title
                          ];
                        })
                ];
            });

        $basicSets = BasicSet::query()
            ->select(['title', 'sets', 'icon'])
            ->get()
            ->map(function (BasicSet $basicSet) {
                return [
                    'title' => $basicSet->title,
                    'icon' => env('APP_URL') . '/storage/' .   $basicSet->icon,
                    'sets' => $basicSet->sets
                ];
            });

        $insurances = Insurance::query()
            ->select(['id', 'title', 'description', 'text', 'price'])
            ->get()
            ->map(function (Insurance $insurance) {
                return [
                    'id' => $insurance->id,
                    'title' => $insurance->title,
                    'text' => $insurance->text,
                    'price' => $insurance->price,
                    'description' => $insurance->description
                ];
            });

        $createOrderViewModel = new CreateOrderViewModel($freeServices, $baseServices, $addServices, $surfaces,
                                                        $conditions, $basicSets, $insurances);

        return $createOrderViewModel;
    }

    /**
     * @param Collection $data
     * @return Charge
     */
    public function createCharge(Collection $data): Charge
    {
        Stripe::setApiKey('sk_test_UsXlfpOs4ccnpeGrcWcXCwlW');
        return Charge::create([
            'amount' => round($data['amount']) * 100,
            'currency' => 'eur',
            'capture' => false,
            'source' => $data['token']
        ]);
    }

    /**
     * @param CreateOrderDto $createOrderDto
     */
    public function createOrder(CreateOrderDto $createOrderDto)
    {
        $order = Order::create([
           'buy_clear_kit' => $createOrderDto->isBuyClearKit(),
            'amount' => $createOrderDto->getAmount(),
            'date_time' => $createOrderDto->getDateTime(),
            'comment' => $createOrderDto->getComment(),
            'meters' => $createOrderDto->getMeters(),
            'hours' => $createOrderDto->getHours(),
            'user_id' => $createOrderDto->getUserId(),
            'cleaner_id' => $createOrderDto->getCleanerId(),
            'charge_id' => $createOrderDto->getChargeId(),
            'status' => $createOrderDto->getStatus(),
            'paypal_id' => $createOrderDto->getPaypalId()
        ]);

        $order->addServices()->attach($createOrderDto->getAddSubServices());
        $order->addSurfaces()->attach($createOrderDto->getSurfaces());
        $order->addSubConditions()->attach($createOrderDto->getSubConditions());
        $order->save();

        $user = User::query()->find($order->user_id);
        $user->insurances()->attach($createOrderDto->getInsurances());
        $user->subscription_id = $createOrderDto->getSubscriptionId();
        $user->save();
    }

    public function storeOrder($data, $user){


        $data['date_time'] = Carbon::parse("{$data['date']['date']} {$data['date']['time'][0]}");
        //$test = $data['date_time']->format('Y-m-d H:i:s');

        unset($data['date']);
        if($data['token'] == 'init'){
            $data['token'] = str_random(20);
        }
        $data['user_id'] = $user->id;
        $order = Order::createOrder($data);
//        if($data['type'] != 'cards' && !$buySubscription)
//            event(new OrderCreatedEvent($order));

        return $order;

    }

    public function renewSubscription($user)
    {
        $dates = [];
        $new_dates = [];
        $token = str_random(20);
        $orders = Order::query()->with('active_subscription')->where('user_id',$user->id)->whereNotNull('active_subscription')->orderBy('date_time')->get();
     //   $count = Order::query()->with('active_subscription')->where('user_id',$user->id)->whereNotNull('active_subscription')->orderBy('date_time')->count();
      //  $price = $orders[0] ? $orders[0]->amount : 0;
      //  $priceForOne = $price/$count;
        foreach ($orders as $item){
//            $item->status = 'archived';
//            $item->save();
            $dates [(int)Carbon::parse($item->date_time)->format('N')] = Carbon::parse($item->date_time)->addWeek(1)->format('Y-m-d H:i:s');
        }
        foreach ($dates as $date){
            $new_dates [] = Carbon::parse($date)->format('Y-m-d H:i:s');
            for($i = 0;$i<3;$i++){
                $new_dates [] = Carbon::parse($date)->addWeek($i + 1)->format('Y-m-d H:i:s');
            }
        }
      //  $order = $orders->first();
      //  $order->amount = $priceForOne * 4;
        foreach ($new_dates as $date){
            $orders [] = $this->copyOrder($orders->first(),$date,$token);
        }

        return $orders[0];
    }
    public function storeOrderSubscription($data,$user)
    {
        $dates = $data['time'];
        $data['user_id'] = $user->id;
        if($data['token'] == 'init'){
            $data['token'] = str_random(20);
        }
        $active_subscription = ActiveSubscription::where('user_id',$user->id)->where('active',true)->first();
        if($active_subscription == null){
            $active_subscription = ActiveSubscription::create([
                'count_clean'=> count($data['date']),
                'user_id'=> $data['user_id'],
                'tarif'=> (float)Order::getTarif($data),
                'price'=> 0,
                'active'=> true,
            ]);
        }


        foreach ($data['date'] as $date){
            $notFormatDate = $date['date'].' '.$date['time'][0];
            $data['date_time'] =  Carbon::createFromFormat('d/m/Y H:i',$notFormatDate);
            $order [] = Order::createOrder($data,$date,$active_subscription->id);
        }
        $active_subscription->price = $order[0]->amount;
        $active_subscription->save();
        if($data['type'] != 'cards') {
           // $user->subscription_id = $data['subscription_id'];
           // $user->save();
            $user->notify(new SubscriptionCreatedNotification($user->name, $user->subscription->title));
            $order->amount *= 4;
            event(new OrderCreatedEvent($order));
        }
        return $order[0] ?? null;
    }
    /**
     * @return Collection|Orders[]
     */
    public function getAll(): Collection
    {
        /* никогда так не делай !!! это другой слой логики */
        $user = auth()->guard('api')->user();
        $ordersQuery = $orders = Order::query()
            ->with('addServices', 'addSurfaces', 'addSubConditions', 'insurances')
            ->where('status', '<>', 'archived')
            ->where('status', '<>', 'wait for pay');

        if ($user->role === 'client') {
            $orders = $ordersQuery
                ->where('user_id', $user->id)
                ->orderBy('date_time', 'ASC')
                ->with('user')
                ->get();
        }

        else if ($user->role === 'cleaner') {
            $orders = $ordersQuery
                ->orderBy('date_time', 'ASC')
                ->with('user')
                ->where(function ($builder) use ($user) {
                    $builder
                        ->where('cleaner_id', $user->id)
                        ->orWhere('cleaner_id', null);
                })
                ->get();

        } else {
            $orders = collect([]);
        }
        $address = '';
        return $orders->map(function (Order $order) use($address, $user) {
            if ($order->address == '') {
                $address = $user->address;
            } else {
                $address = $order->address;
            }
            return new \App\Entities\Order($order->id, $order->buy_clear_kit, $order->amount, new Carbon($order->date_time),
                $order->comment, $order->meters, $order->hours, $order->user_id, $order->cleaner_id, $order->status,
                $order->created_at, $order->addServices, $order->addSubConditions, $order->addSurfaces, $order->insurances, $address,
                $order->city_id, $order->realty_type_id ,$order->realty_type()->first()->title, $order->secret_place_id, $order->subscription_id, $order->is_created_with_subscription ?? 0);
        });
    }

    public function getArchived(): Collection
    {
        /* никогда так не делай !!! это другой слой логики */
        $user = auth()->guard('api')->user();

        $address = $user->getAddress();

        $ordersQuery = $orders = Order::query()
            ->with('addServices', 'addSurfaces', 'addSubConditions', 'insurances')
            ->where('status', '=', 'archived');

        if ($user->role === 'client') {
            $orders = $ordersQuery
                ->where('user_id', $user->id)
                ->orderBy('created_at', 'DESC')
                ->get();
        }
        else if ($user->role === 'cleaner') {
            $orders = $ordersQuery
                ->where(function ($builder) use ($user) {
                    $builder
                        ->where('cleaner_id', $user->id)
                        ->orWhere('cleaner_id', null);
                })
                ->get();
        }
        else {
            $orders = collect([]);
        }

        return $orders->map(function (Order $order) use ($address) {
            // $address = User::query()->where('id', $order->user_id)->first()->getAddress();
            return new \App\Entities\Order($order->id, $order->buy_clear_kit, $order->amount, new Carbon($order->date_time),
                $order->comment, $order->meters, $order->hours, $order->user_id, $order->cleaner_id, $order->status,
                $order->created_at, $order->addServices, $order->addSubConditions, $order->addSurfaces, $order->insurances, $order->address,
                $order->city_id, $order->realty_type_id, null);
        });
    }

    public function changeOrderStatus(int $order_id, string $status)
    {
        $order = Order::query()->find($order_id);
        $user = $order->user()->first();
        $admin = AdminUser::query()
            ->where('username', 'admin')
            ->first();
        if ($status == 'user_canceled'){
            $cleaner = $order->cleaner()->first();
            $order->update([
                'cleaner_id' => null,
                'status' => 'canceled'
            ]);
            $order->save();
            if($cleaner) {
                $cleaner->notify(new OrderCanceledNotification($order, true));
            }
            $user->notify(new OrderCanceledNotification($order));
            $admin->notify(new AdminCanceledOrderNotification($order));
        } elseif ($status == 'canceled') {
            $order->update([
                'cleaner_id' => null,
            ]);
            $order->save();
            $admin->notify(new AdminCanceledOrderNotification($order));
        } else {
            $order->update([
                'status' => $status
            ]);
            $order->save();

            $user->notify(new OrderNotification($order));
        }
    }

    public function acceptOrder(int $order_id, int $cleaner_id): bool
    {
        $order = Order::query()->find($order_id);
        $order->cleaner_id = $cleaner_id;

        if ($order->save()) {
            return true;
        } else {
            return false;
        }
    }

    public function getOrdersByStatus(int $cleaner_id, string $status)
    {
        return Order::query()
            ->where('status', $status)
            ->where('cleaner_id', $cleaner_id)
            ->get()
            ->map(function (Order $order) {
                $address = User::query()->where('id', $order->user_id)->first()->getAddress();
                return new \App\Entities\Order($order->id, $order->buy_clear_kit, $order->amount, new Carbon($order->date_time),
                    $order->comment, $order->meters, $order->hours, $order->user_id, $order->cleaner_id,
                    $order->status, $order->created_at, $order->addServices, $order->addSubConditions,
                    $order->addSurfaces, $order->insurances, $address,
                    $order->city_id, $order->realty_type_id, null);
            });
    }

    public function checkGestOrder($data)
    {
        $id = $data['id'];
        $token = Order::select('token')->where('id', $id)->first();
        if (strstr($token, 'OK')){
            return 'ok';
        } else {
            if (strstr($token, 'FAIL')){
                return 'stop';
            }
            return 'wait';
        }
    }
    public function delete ($order_id) {
        $order = Order::find($order_id);
        if($order->active_subscription){
            Order::query()->where('active_subscription',$order->active_subscription)->delete();
        }else{
            Order::query()->where('id', $order_id)->first()->delete();
        }

    }

    public function getCheckboxServicesByCleanType($id)
    {
        return CheckboxService::query()->where('clean_type',$id)->with('answers')->get()->map(function($service){
            return [
                    'id'=> $service->id,
                    'question'=>$service->question,
                    'description'=>$service->description,
                    'type'=>$service->type,
                    'answers'=> $service->answers->map(function($children){
                        return [
                            'id'=>$children->id,
                            'value'=>$children->answer,
                            'price'=>$children->price,
                            'description'=>$children->description,
                        ];
                    })
            ];
        })->toArray();
    }

    public function expirationSucription($user)
    {
        $active_subscription = ActiveSubscription::where('user_id',$user->id)->where('active',true)->first();
        $last_order = $active_subscription->orders()->orderBy('date_time','DESC')->first();
        $date = Carbon::now();
        if($date->diffInDays($last_order->date_time) == 0){
           return true;
        }
        return false;
    }
}
