<?php

namespace App\Services;


use App\Entities\Province;
use Illuminate\Support\Collection;

class ProvinceService
{
    /*
     * Collection|Province[]
     */
    public function getAll(): Collection
    {
        $provinces = \App\Models\Province::query()
            ->select(['id', 'title'])
            ->get()
            ->map(function (\App\Models\Province $province) {
                return new Province($province->id, $province->title);
            });

        return $provinces;
    }
}