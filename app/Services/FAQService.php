<?php
/**
 * Created by PhpStorm.
 * User: serega
 * Date: 20.10.17
 * Time: 13:17
 */

namespace App\Services;


use App\Models\FAQCategory;

class FAQService
{
    public function getCategoriesAndQuestions (){

        $faqModel = FAQCategory::query()->with('question')->get();

        return $faqModel;
    }
}