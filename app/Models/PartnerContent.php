<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PartnerContent extends Model
{
    protected $table = 'partner_content';

    protected $fillable = ['title', 'icon'];
}
