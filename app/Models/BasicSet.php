<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BasicSet
 *
 * @property int $id
 * @property string $title
 * @property array $sets
 * @property string $icon
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property mixed $sets_text
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereSets($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BasicSet whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BasicSet extends Model
{
    protected $table = 'basic_sets';

    protected $fillable = ['title', 'sets', 'setsText', 'icon'];

    protected $casts = ['sets' => 'json'];

    protected $appends = ['setsText'];

    // mutators

    public function setSetsTextAttribute($value)
    {
        $arr = explode(PHP_EOL, $value);

        foreach ($arr as $key => $value){
            $arr[$key] = str_replace("\r", '', $value);
        }

        $this->sets = $arr;
    }

    public function getSetsTextAttribute()
    {
        return implode(PHP_EOL, $this->sets);
    }
}
