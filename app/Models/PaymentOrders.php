<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentOrders extends Model
{
    protected $table = 'payment_orders';

    protected $fillable = ['order_id', 'type', 'token'];

    // relations

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
