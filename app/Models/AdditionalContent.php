<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdditionalContent extends Model
{
    protected $table = 'additionals_content';

    protected $fillable = ['short_text', 'text', 'icon', 'short_title', 'title'];
}
