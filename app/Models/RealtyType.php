<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

/**
 * App\Models\RealtyType
 *
 * @property int $id
 * @property string $title
 * @property int $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyType whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyType wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyType whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyType whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RealtyType extends Model
{
    protected $table = 'realty_types';

    protected $fillable = ['title', 'price', 'express_price'];

    const STANDARD_CLEAN = 1;

    // relations

    public function realtyTypeMetersHours()
    {
        return $this->hasMany(RealtyTypeMeterHour::class);
    }

    // functions

    public function getActualPrice(Carbon $date)
    {

//        if (Carbon::now()->diffInDays($date) < 2) {
//            return $this->express_price;
//        }

        return $this->price;
    }
}
