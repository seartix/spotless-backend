<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextPage extends Model
{
    protected $table = 'text_pages';
    protected $fillable = [
        'title',
        'text'
    ];
}
