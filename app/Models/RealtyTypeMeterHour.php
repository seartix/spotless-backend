<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\RealtyTypeMeterHour
 *
 * @property int $id
 * @property int $realty_type_id
 * @property float $meters
 * @property float $hours
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\RealtyType $realtyType
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereRealtyTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\RealtyTypeMeterHour whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class RealtyTypeMeterHour extends Model
{
    protected $table = 'realty_types_meters_hours';

    protected $fillable = ['realty_type_id', 'meters', 'hours'];

    // relations

    public function realtyType()
    {
        return $this->belongsTo(RealtyType::class);
    }

    // scopes

    public function scopeByColumnMore(Builder $builder, $column, $values)
    {
        $builder->where($column, '>=', $values)
            ->orderBy($column, 'ASC');
    }

    public function scopeByColumnLess(Builder $builder, $column, $values)
    {
        $builder->where($column, '<=', $values)
            ->orderBy($column, 'DESC');
    }
}
