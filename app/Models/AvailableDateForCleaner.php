<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Database\Eloquent\Model;

class AvailableDateForCleaner extends Model
{
    protected $table = 'available_date_for_cleaners';

    protected $fillable = ['date', 'time_start', 'time_end', 'cleaner_id', 'manual'];
    public $date_start;
    public $date_end;

    // relations

    public function cleaner()
    {
        return $this->belongsTo(User::class,'cleaner_id');
    }

    public function setDateAttribute ($date_start){
        if(strrpos ($date_start,'/')){
            $dateArray = explode('/',$date_start);
            $date = Carbon::createFromDate($dateArray[2],$dateArray[1],$dateArray[0]);
            $this->attributes['date'] = $date->format('Y-m-d H:i:s');
        }
        else{
            $this->attributes['date'] = $date_start;
        }


    }
    public static function boot(){
        parent::boot();

        static::creating(function($availableDate){
            if ($availableDate->date_start && $availableDate->date_end) {
                $interval = DateInterval::createFromDateString('1 day');

                $period = new DatePeriod(new \DateTime($availableDate->date_start), $interval, new \DateTime($availableDate->date_end));

                foreach ($period as $dt) {
                    $newAvailableDate = $availableDate->replicate();
                    $newAvailableDate->date = $dt;
                    $newAvailableDate->save();

                }
                $availableDate->date = new \DateTime($availableDate->date_end);
            }
        });
    }
}
