<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CheckboxService extends Model
{
   protected $table='checkbox_services';

    public function answers()
    {
        return $this->hasMany(AnswerCheckboxService::class,'checkbox_service');
    }
}
