<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActiveSubscription extends Model
{
    protected $table = 'user_active_subscriptions';

    protected $fillable = [
        'count_clean',
        'tarif',
        'price',
        'active',
        'user_id'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class,'active_subscription');
    }
}
