<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FAQQuestion extends Model
{
    protected $table = 'faq_questions';

    public function category (){
        return $this->belongsTo(FAQCategory::class, 'id', 'category_id');
    }
}
