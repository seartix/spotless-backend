<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CleanerReview extends Model
{
    protected $table = 'cleaners_review';

    protected $fillable = ['text', 'cleaner_id', 'user_id', 'show', 'rating', 'recommend'];

    // relations

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cleaner()
    {
        return $this->belongsTo(User::class, 'cleaner_id');
    }
}
