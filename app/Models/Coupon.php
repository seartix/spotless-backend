<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use App\User;

class Coupon extends Model
{
    protected $table = 'coupons';

    protected $fillable = ['code', 'date_start', 'date_end', 'count_using', 'percent', 'price_from'];

    // relations

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_coupons');
    }
    public function setDateStartAttribute($date)
    {
        if(strrpos ($date,'/')){
            $this->attributes['date_start'] = Carbon::createFromFormat('d/m/Y H:i:s',$date)->format('Y-m-d H:i:s');
        }
        else{
            $this->attributes['date_start'] = $date;
        }
    }
    public function setDateEndAttribute($date)
    {
        if(strrpos ($date,'/')){
            $this->attributes['date_end'] = Carbon::createFromFormat('d/m/Y H:i:s',$date)->format('Y-m-d H:i:s');
        }
        else{
            $this->attributes['date_end'] = $date;
        }
    }
}
