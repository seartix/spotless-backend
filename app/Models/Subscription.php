<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property int $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddService[] $addServices
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Subscription whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Subscription extends Model
{
    protected $table = 'subscriptions';

    protected $fillable = [
        'title',
        'description',
        'price',
        'type',
        'days',
        'additional_list',
        'additional_list_text',
    ];

    // relations

    public function addServices()
    {
        return $this->belongsToMany(AddService::class, 'subscription_add_service');
    }
    public function services()
    {
        return $this->belongsToMany(AddService::class, 'subscription_add_service');
    }

    protected $casts = ['additional_list' => 'json','frequency'=>'json'];

    protected $appends = ['additional_list_text'];

    // mutators

    public function setAdditionalListTextAttribute($value)
    {
        $arr = explode(PHP_EOL, $value);

        foreach ($arr as $key => $value){
            $arr[$key] = str_replace("\r", '', $value);
        }

        $this->additional_list = $arr;
    }

    // getters

    public function getAdditionalListTextAttribute()
    {
        $result = null;

        if ($this->additional_list) {
            $result = implode(PHP_EOL, $this->additional_list);
        }

        return $result;
    }


}
