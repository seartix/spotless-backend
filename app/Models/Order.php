<?php

namespace App\Models;

use App\Events\CleanerChangedEvent;
use App\Events\OrderUpdatedEvent;
use App\User;
use Illuminate\Database\Eloquent\Model;
use DB;
use Carbon\Carbon;

/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $buy_clear_kit
 * @property float $amount
 * @property int $user_id
 * @property string $date_time
 * @property string $comment
 * @property string $address
 * @property float $meters
 * @property float $hours
 * @property int secret_place_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddSubService[] $addSubConditions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddSubService[] $addSubServices
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddSubService[] $addSurfaces
 * @property-read \App\Models\Insurance $insurances
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereBuyClearKit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereDateTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereInsuranceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereMeters($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Order whereUserId($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    protected $table = 'orders';
    const ABONEMENT = 3;
    protected $fillable = [
        'token',
        'date_time',
        'secret_place_id',
        'city_id',
        'realty_type_id',

        'cleaner_id',
        'user_id',

        'meters',
        'hours',
        'amount',

        'buy_clear_kit',

        'comment',

        'buy_clear_kit',

        'status',
        'subscription_id',
        'coupon_id',
        'end_date_time',
        'address',

        'active_subscription',
        'count_bathroom'
    ];

    // functions
    public function getClone($date_time,$token) : Order
    {
        $attributes = $this->attributes;
//        if (!$buy_subscription) {
//            $attributes['type'] = null;
//            $attributes['token'] = null;
//            $attributes['cleaner_id'] = null;
//        }
        $attributes['status'] = 'wait for pay';
        $attributes['date_time'] = $date_time;
        $attributes['end_date_time'] = Carbon::parse($date_time)->addHour($this->hours);
        $attributes['token'] = $token;
        $this->attributes = $attributes;

        unset($attributes['created_at']);
        unset($attributes['id']);
        unset($attributes['updated_at']);

        DB::beginTransaction();

        $order = self::create($attributes);

        $order->addServices()->sync($this->addServices()->pluck('id'));
        $order->addSurfaces()->sync($this->addSurfaces()->pluck('id'));
        $order->addSubConditions()->sync($this->addSubConditions()->pluck('id'));
        $order->insurances()->sync($this->insurances()->pluck('id'));

        DB::commit();

        return $order;
    }

    public static function createOrder($data,$date = null,$subscription_id = null)
    {
        $addServices = $data['addSubServices'];
        unset($data['addSubServices']);

        $surfaces = $data['surfaces'];
        unset($data['surfaces']);

        $subConditions = $data['subConditions'];
        unset($data['subConditions']);

        $insurances = $data['insurances'];
        unset($data['insurances']);

        // TODO
        if(is_array($date)){
            $evalData = self::evalAmountSubscribtion($data, $addServices, $insurances);
            $data['hours'] = $evalData['hours'];
            $data['amount'] = $evalData['amount'];
            $data['active_subscription'] = $subscription_id;
        }else{
            $evalData = self::evalAmount($data, $addServices, $insurances);
            $data['hours'] = $evalData['hours'];
            $data['amount'] = $evalData['amount'];
        }



        $endTime = Carbon::createFromFormat('Y-m-d H:i:s', $data['date_time'])->addHours($data['hours']);
//        if (strpos($data['hours'], '.')) {
//            $endTime->addMinutes('30');
//        }
        $data['end_date_time'] = $endTime;
        if ($data['type'] != 'cards')
            $data['status'] = 'process';
        else
            $data['status'] = 'wait for pay';
        $user = User::find($data['user_id']);
//        if($user->subscriber){
//            $data['amount'] = $data['amount'] - $data['amount'] * 0.1;
//        }
        DB::beginTransaction();

        $order = self::create($data);
        if($data['realty_type_id'] === RealtyType::STANDARD_CLEAN){
            $order->addServices()->sync($addServices);
        }
        else{
            $order->AnswersCheckbox()->sync($addServices);
        }
        $order->addSurfaces()->sync($surfaces);
        $order->addSubConditions()->sync($subConditions);
        $order->insurances()->sync($insurances);

        DB::commit();

        return $order;
    }

    public static function updateOrder($order, $data)
    {
        $addServices = $data['add_services'];
        unset($data['add_services']);

        $surfaces = $data['add_surfaces'];
        unset($data['add_surfaces']);

        $subConditions = $data['sub_conditions'];
        unset($data['sub_conditions']);

        $insurances = $data['insurances'];
        unset($data['insurances']);

        // TODO

        $evalData = self::evalAmount($data, $addServices, $insurances);

        $data['hours'] = $evalData['hours'];
        $data['amount'] = $evalData['amount'];

        $data['status'] = 'process';

        DB::beginTransaction();

        $order->update($data);
        $order->addServices()->sync($addServices);
        $order->addSurfaces()->sync($surfaces);
        $order->addSubConditions()->sync($subConditions);
        $order->insurances()->sync($insurances);

        DB::commit();

        return $order;
    }

    public static function evalOrder($data)
    {
        $addServices = $data['addSubServices'];
        if (isset($data['add_services'])) {
            $addServices = $data['add_services'];
        }
        unset($data['addSubServices']);
        unset($data['add_services']);

        $insurances = $data['insurances'];
        unset($data['insurances']);

        $evalData = self::evalAmount($data, $addServices, $insurances);


        return $evalData;
    }
    public static function evalBathroom($bathrooms,$count = 1){
        return ($bathrooms * 8)*$count;
    }
    public static function evalAmount($data, $addServices, $insurances)
    {
        $amount = 0;
        $hours = 0;

        $user = User::query()
            ->where('id', $data['user_id'])
            ->with('subscription.addServices')
            ->first();

        $realtyTypeData = self::evalRealtyTypeAmount($data['realty_type_id'], $data['meters'], $data['date_time']);
        $amount += $realtyTypeData['amount'];
        $hours += $realtyTypeData['hours'];

        $hasClearKit = $data['buy_clear_kit'];
        $amount += self::evalBathroom($data['count_bathroom']);
        if($data['realty_type_id'] === RealtyType::STANDARD_CLEAN){
            $addServicesData = self::evalAddServicesAmount($data['meters'], $addServices);
        }
        else{
            $addServicesData = self::evalCheckboxServicesAmount($data['meters'], $addServices);
        }
        $amount += $addServicesData['amount'];
        $hours += $addServicesData['hours'];

        $amount += self::evalInsuranceAmount($insurances);
        if ($hasClearKit) {
            $amount += 5;
        }
        if (isset($data['coupon_id'])) {
            $percent = Coupon::select('percent')->where('id', $data['coupon_id'])->first()->percent;
            $percent = 1 - $percent / 100;
            $amount *= $percent;
        }

        return [
            'hours' => $hours,
            'amount' => $amount,
        ];
    }

    public static function evalInsuranceAmount($insuranceIds)
    {
        $insuranceAmount = 0;

        if (count($insuranceIds))
        {
            foreach ($insuranceIds as $insuranceId)
            {
                $insurance = Insurance::query()->find($insuranceId);

                $insuranceAmount += $insurance->price;
            }
        }

        return $insuranceAmount;
    }

    public static function evalCheckboxServicesAmount($meters, $checkboxServicesIds)
    {
        $сheckboxServicesAmount = 0;
        $CheckboxServices = 0;

        if (count($checkboxServicesIds))
        {
            foreach ($checkboxServicesIds as $checkboxServiceId){
                $checkboxService = AnswerCheckboxService::with('question')->find($checkboxServiceId);
                $сheckboxServicesAmount += $checkboxService->price;
            }
        }
        return [
            'hours' => 0,
            'amount' => $сheckboxServicesAmount,
        ];
    }
    public static function evalAddServicesAmount($meters, $addServicesIds, $count_clean = 1)
    {
        $addServicesAmount = 0;
        $addServicesHours = 0;

        if (count($addServicesIds))
        {
            foreach ($addServicesIds as $addServiceId)
            {
                $addService = AddService::query()->find($addServiceId);
                if ($addService->hours()->count() > 0)
                {
                    $hourMeter = $addService
                        ->hours()
                        ->where('meters', '>=', $meters)
                        ->orderBy('meters', 'ASC')
                        ->first();

                    if (!$hourMeter)
                    {
                        $hourMeter = $addService
                            ->hours()
                            ->orderBy('meters', 'DESC')
                            ->first();
                    }

                    $addServicesHours += $hourMeter->hours;
                    $addServicesAmount += $addService->price * $hourMeter->hours;
                } else {
                    $addServicesHours += $addService->hour;
                    //$addServicesAmount += $addService->price * $addService->hour;
                    $addServicesAmount += $addService->price * $count_clean;
                }
            }
        }

        return [
            'hours' => $addServicesHours,
            'amount' => $addServicesAmount,
        ];
    }

    private static function evalRealtyTypeAmount($realty_type_id, $meters, $date_time)
    {
        $realty_type = RealtyType::find($realty_type_id);

        $price = $realty_type->getActualPrice($date_time);

        $hourMeter = $realty_type
            ->realtyTypeMetersHours()
            ->where('meters', '>=', $meters)
            ->orderBy('meters', 'ASC')
            ->first();

        if (!$hourMeter)
        {
            $hourMeter = $realty_type
                ->realtyTypeMetersHours()
                ->orderBy('meters', 'DESC')
                ->first();
        }

        return [
            'hours' => $hourMeter->hours,
            'amount' => $hourMeter->add_price,
        ];
    }

    public static function evalAmountSubscribtion($data, $addServices, $insurances)
    {
        $amount = 0;
        $hours = 0;

        $user = User::query()
            ->where('id', $data['user_id'])
            ->with('subscription.addServices')
            ->first();

        $realtyTypeData = self::evalRealtyTypeAmount($data['realty_type_id'], $data['meters'], $data['date_time']);
       // $amount += $realtyTypeData['amount'];

        $data['frequency_price'] = self::getTarif($data);
        $hours += $realtyTypeData['hours'];
        $amount += $data['frequency_price'] * $hours * count($data['date']);
        $hasClearKit = $data['buy_clear_kit'];
        $amount += self::evalBathroom($data['count_bathroom'],count($data['date']));
        $addServicesData = self::evalAddServicesAmount($data['meters'], $addServices,count($data['date']));
        $amount += $addServicesData['amount'];
        $hours += $addServicesData['hours'];

        $amount += self::evalInsuranceAmount($insurances);
        if ($hasClearKit) {
            $amount += 5;
        }
        if (isset($data['coupon_id'])) {
            $percent = Coupon::select('percent')->where('id', $data['coupon_id'])->first()->percent;
            $percent = 1 - $percent / 100;
            $amount *= $percent;
        }

        return [
            'hours' => $hours,
            'amount' => $amount,
        ];
    }

    public static function getTarif($data)
    {
        $subscription = Subscription::find(self::ABONEMENT);
        $frequencies = explode("\r\n",$subscription->frequency);
        foreach ($frequencies as $frequency){
            if(strpos($frequency,'/'.$data['frequency'])){
                return explode('/',$frequency)[1];

            }
        }
    }

    public function getFrequency($tarif)
    {
        $subscription = Subscription::find(self::ABONEMENT);
        $frequencies = explode("\r\n",$subscription->frequency);
        foreach ($frequencies as $frequency){
            if(strpos($frequency,'/'.$tarif)){
                return explode('/',$frequency)[2];

            }
        }
    }

    public function getDateForMail()
    {
        $carbon = new Carbon($this->attributes['date_time']);
        return $carbon->format('d/m/y');
    }
    // relations


    public function addServices()
    {
        return $this->belongsToMany(AddService::class, 'order_add_service');
    }

    public function AnswersCheckbox()
    {
        return $this->belongsToMany(AnswerCheckboxService::class, 'order_answer_checkbox','order_id','answer_checkbox_id');
    }
    public function addSurfaces()
    {
        return $this->belongsToMany(Surface::class);
    }

    public function addSubConditions()
    {
        return $this->belongsToMany(SubCondition::class, 'order_sub_condition', 'order_id', 'sub_condition_id');
    }

    public function insurances()
    {
        return $this->belongsToMany(Insurance::class, 'order_insurance');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cleaner()
    {
        return $this->belongsTo(User::class, 'cleaner_id');
    }

    public function realty_type()
    {
        return $this->belongsTo(RealtyType::class);
    }

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function active_subscription()
    {
        return $this->belongsTo(ActiveSubscription::class,'active_subscription');
    }
    public static function boot()
    {
        parent::boot();
        static::updating(function (self $item){
            if ($item->attributes['cleaner_id'] == 0) {
                $item->attributes['cleaner_id'] = null;
            }
            if ($item->attributes['subscription_id'] == 0) {
                $item->attributes['subscription_id'] = null;
            }
            if ($item->attributes['user_id'] == 0) {
                $item->attributes['user_id'] = null;
            }
            if ($item->attributes['cleaner_id'] != $item->original['cleaner_id']) {
                event(new CleanerChangedEvent(
                    $item->id,
                    $item->attributes['cleaner_id'],
                    $item->attributes['date_time'],
                    $item->attributes['user_id']
                ));
            }

            if (strpos($item->original['token'], 'PAY-') && strpos($item->original['token'], 'OK-')) {
                event(new OrderUpdatedEvent($item));
            }
        });

    }

    public function scopeActive($query)
    {
        return $query
            ->where('status', 'process')
            ->orWhere('status', 'wait for pay');
    }

    public function setDateTimeAttribute($date)
    {
        if(strrpos ($date,'/')){
            $this->attributes['date_time'] = Carbon::createFromFormat('d/m/Y H:i:s',$date)->format('Y-m-d H:i:s');
        }
        else{
            $this->attributes['date_time'] = $date;
        }
    }
}
