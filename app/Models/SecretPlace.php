<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SecretPlace extends Model
{
    protected $table = 'secret_places';
}
