<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Condition
 *
 * @property int $id
 * @property string $title
 * @property string $icon
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property mixed $sub_conditions
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\SubCondition[] $subConditions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Condition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Condition whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Condition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Condition whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Condition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Condition extends Model
{
    protected $table = 'conditions';

    protected $appends = ['subConditions'];

    protected $fillable = ['title', 'icon'];

    // relations

    public function subConditions()
    {
        return $this->hasMany(SubCondition::class);
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class, ' 	order_sub_condition');
    }

    // mutators

    public function getSubConditionsAttribute()
    {
        return $this->subConditions()->get();
    }

    public function setSubConditionsAttribute($value)
    {
        $this->subConditions()->sync($value);
    }
}
