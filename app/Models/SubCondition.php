<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SubCondition
 *
 * @property int $id
 * @property int $condition_id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\Condition $condition
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\User[] $forbiddenUsers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCondition whereConditionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCondition whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCondition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCondition whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\SubCondition whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SubCondition extends Model
{
    protected $table = 'sub_conditions';

    protected $fillable = ['title'];

    // relations

    public function condition()
    {
        return $this->belongsTo(Condition::class);
    }

    public function forbiddenUsers()
    {
        return $this->belongsToMany(User::class, 'user_forbidden_sub_condition');
    }
}
