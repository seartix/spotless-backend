<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table='articles';


    public function comments()
    {
        return $this->hasMany(Comment::class,'article_id');
    }

    public function categories()
    {
        return $this->belongsTo(Category::class,'category_id');
    }
}
