<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AddSubServiceHour
 *
 * @property int $id
 * @property int $add_sub_service_id
 * @property int $count
 * @property int $hours
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \App\Models\AddSubService $addSubService
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereAddSubServiceId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereCount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereHours($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddSubServiceHour whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AddServiceHour extends Model
{
    protected $table = 'add_services_hours';

    protected $fillable = [
        'add_service_id',
        'meters',
        'hours',
    ];

    // relations

    public function addService()
    {
        return $this->belongsTo(AddService::class);
    }
}
