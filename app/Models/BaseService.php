<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\BaseService
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $image
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\BaseService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class BaseService extends Model
{
    protected $table = 'base_services';

    protected $fillable = [
        'title',
        'text',
        'image',
    ];
}
