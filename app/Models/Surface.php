<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Surface
 *
 * @property int $id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Surface whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Surface whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Surface whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Surface whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Surface extends Model
{
    protected $table = 'surfaces';

    protected $fillable = ['title'];

    public function orders()
    {
        return $this->belongsToMany(Order::class, ' 	order_surface');
    }
}
