<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = ['title', 'user_id', 'employer_id'];

    // relations

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
