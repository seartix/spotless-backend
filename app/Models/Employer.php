<?php

namespace App\Models;
use App\Events\EmployeeCreatedEvent;
use App\Models\Language;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Employer extends Model
{
    use Notifiable;
    protected $table = 'employers';

    protected $fillable = [
        'name', 'surname', 'email',
        'phone', 'fiscal_code', 'gender',
        'iva_code', 'skill', 'cv_path',
        'reference_path', 'identificate_path', 'avatar_path',
        'add_info', 'languages'
    ];
    public function getCvPathAttribute(){
        return asset('upload/'.$this->attributes['cv_path']);
    }
    public function getReferencePathAttribute(){
        return asset('upload/'.$this->attributes['reference_path']);
    }
    public function getIdentificatePathAttribute(){
        return asset('upload/'.$this->attributes['identificate_path']);
    }
    public function getAvatarPathAttribute(){
        return asset('upload/'.$this->attributes['avatar_path']);
    }
    protected static function boot() {
        parent::boot();

        static::created(function($item) {
            event(new EmployeeCreatedEvent($item));
        });
    }
}
