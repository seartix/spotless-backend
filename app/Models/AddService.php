<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AddService
 *
 * @property int $id
 * @property string $title
 * @property string $help
 * @property string $icon
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AddSubService[] $addSubServices
 * @property mixed $add_sub_services
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscription[] $subscriptions
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereHelp($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AddService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AddService extends Model
{
    protected $table = 'add_services';

    protected $fillable = [
        'title',
        'help',
        'icon',
        'price',
        'hour'
    ];

    // relations

    public function subscriptions()
    {
        return $this->belongsToMany(Subscription::class, 'subscription_add_service');
    }

    public function hours()
    {
        return $this->hasMany(AddServiceHour::class);
    }
    public function user(){
        return $this->belongsToMany(User::class);
    }
}
