<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AnswerCheckboxService extends Model
{
    protected $table='answers_checkbox_services';

    public function question()
    {
        return $this->belongsTo(CheckboxService::class,'checkbox_service');
    }
}
