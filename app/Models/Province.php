<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Province extends Model
{
    protected $table = 'provinces';

    protected $fillable = ['title', 'city_id'];

    // relations

    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
