<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Card extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'user_id',
        'card_number',
        'exp_date',
        'exp_year',
        'holder_name',
        'cvv',
        'card_type'
    ];
    public function user () {
        return $this->belongsTo(User::class);
    }
}
