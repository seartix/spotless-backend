<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Tests\Models\User;

/**
 * App\Models\Insurance
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $text
 * @property float $price
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Insurance whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Insurance extends Model
{
    protected $table = 'insurances';

    protected $fillable = ['title', 'description', 'text', 'price'];

    // relations
    public function users()
    {
        return $this->belongsToMany(User::class, 'user_insurance');
    }
}
