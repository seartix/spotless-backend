<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ButtonContent extends Model
{
    protected $table = 'button_content';

    protected $fillable = ['link', 'title', 'text'];
}
