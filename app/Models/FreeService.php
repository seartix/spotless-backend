<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\FreeService
 *
 * @property int $id
 * @property string $title
 * @property string $text
 * @property string $icon
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereIcon($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\FreeService whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class FreeService extends Model
{
    protected $table = 'free_services';

    protected $fillable = ['title', 'text', 'icon'];
}
