<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StepContent extends Model
{
    protected $table = 'steps_content';

    protected $fillable = ['text', 'description', 'icon'];
}
