<?php

namespace App\ViewModels;

use Illuminate\Support\Collection;

class CreateOrderViewModel
{
    /**
     * @var Collection
     */
    private $freeServices;
    /**
     * @var Collection
     */
    private $baseServices;
    /**
     * @var Collection
     */
    private $addServices;
    /**
     * @var Collection
     */
    private $surfaces;
    /**
     * @var Collection
     */
    private $conditions;
    /**
     * @var Collection
     */
    private $basicSets;
    /**
     * @var Collection
     */
    private $insurances;

    public function __construct(Collection $freeServices, Collection $baseServices, Collection $addServices,
                                Collection $surfaces, Collection $conditions, Collection $basicSets,
                                Collection $insurances)
    {
        $this->freeServices = $freeServices;
        $this->baseServices = $baseServices;
        $this->addServices = $addServices;
        $this->surfaces = $surfaces;
        $this->conditions = $conditions;
        $this->basicSets = $basicSets;
        $this->insurances = $insurances;
    }

    /**
     * @return Collection
     */
    public function getFreeServices(): Collection
    {
        return $this->freeServices;
    }

    /**
     * @param Collection $freeServices
     */
    public function setFreeServices(Collection $freeServices)
    {
        $this->freeServices = $freeServices;
    }

    /**
     * @return Collection
     */
    public function getBaseServices(): Collection
    {
        return $this->baseServices;
    }

    /**
     * @param Collection $baseServices
     */
    public function setBaseServices(Collection $baseServices)
    {
        $this->baseServices = $baseServices;
    }

    /**
     * @return Collection
     */
    public function getAddServices(): Collection
    {
        return $this->addServices;
    }

    /**
     * @param Collection $addServices
     */
    public function setAddServices(Collection $addServices)
    {
        $this->addServices = $addServices;
    }

    /**
     * @return Collection
     */
    public function getSurfaces(): Collection
    {
        return $this->surfaces;
    }

    /**
     * @param Collection $surfaces
     */
    public function setSurfaces(Collection $surfaces)
    {
        $this->surfaces = $surfaces;
    }

    /**
     * @return Collection
     */
    public function getConditions(): Collection
    {
        return $this->conditions;
    }

    /**
     * @param Collection $conditions
     */
    public function setConditions(Collection $conditions)
    {
        $this->conditions = $conditions;
    }

    /**
     * @return Collection
     */
    public function getBasicSets(): Collection
    {
        return $this->basicSets;
    }

    /**
     * @param Collection $basicSets
     */
    public function setBasicSets(Collection $basicSets)
    {
        $this->basicSets = $basicSets;
    }

    /**
     * @return Collection
     */
    public function getInsurances(): Collection
    {
        return $this->insurances;
    }

    /**
     * @param Collection $insurances
     */
    public function setInsurances(Collection $insurances)
    {
        $this->insurances = $insurances;
    }
}