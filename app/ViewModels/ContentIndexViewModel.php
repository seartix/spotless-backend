<?php

namespace App\ViewModels;


use Illuminate\Support\Collection;

class ContentIndexViewModel
{
    /**
     * @var Collection
     */
    private $contentSteps;
    /**
     * @var Collection
     */
    private $additionalContent;
    /**
     * @var Collection
     */
    private $contentButton;
    /**
     * @var Collection
     */
    private $contentPartners;
    /**
     * @var Collection
     */
    private $aboutUsContent;
    /**
     * @var Collection
     */
    private $contact;
    /**
     * @var Collection
     */
    private $textContents;

    private $postCodes;


    private $reviews;
    /**
     * ContentIndexViewModel constructor.
     * @param Collection $contentSteps
     * @param Collection $additionalContent
     * @param Collection $contentButton
     * @param Collection $contentPartners
     * @param Collection $aboutUsContent
     * @param Collection $contact
     */
    public function __construct(
        Collection $contentSteps,
        Collection $additionalContent,
        Collection $contentButton,
        Collection $contentPartners,
        Collection $aboutUsContent,
        Collection $contact,
        Collection $textContents,
        Collection $postCodes,
        Collection $reviews
    )
    {
        $this->contentSteps = $contentSteps;
        $this->additionalContent = $additionalContent;
        $this->contentButton = $contentButton;
        $this->contentPartners = $contentPartners;
        $this->aboutUsContent = $aboutUsContent;
        $this->contact = $contact;
        $this->textContents = $textContents;
        $this->postCodes = $postCodes;
        $this->reviews = $reviews;
    }

    /**
     * @return Collection
     */
    public function getContentSteps(): Collection
    {
        return $this->contentSteps;
    }

    /**
     * @param Collection $contentSteps
     */
    public function setContentSteps(Collection $contentSteps)
    {
        $this->contentSteps = $contentSteps;
    }

    /**
     * @return Collection
     */
    public function getAdditionalContents(): Collection
    {
        return $this->additionalContent;
    }

    /**
     * @param Collection $additionalSteps
     */
    public function setAdditionalContents(Collection $additionalContent)
    {
        $this->additionalContent = $additionalContent;
    }

    /**
     * @return Collection
     */
    public function getContentButton(): Collection
    {
        return $this->contentButton;
    }

    /**
     * @param Collection $contentButton
     */
    public function setContentButton(Collection $contentButton)
    {
        $this->contentButton = $contentButton;
    }

    /**
     * @return Collection
     */
    public function getTextContents(): Collection
    {
        return $this->textContents;
    }

    /**
     * @param Collection $textContents
     */
    public function setTextContents(Collection $textContents)
    {
        $this->textContents = $textContents;
    }

    /**
     * @return Collection
     */
    public function getContentPartners(): Collection
    {
        return $this->contentPartners;
    }

    /**
     * @param Collection $contentPartners
     */
    public function setContentPartners(Collection $contentPartners)
    {
        $this->contentPartners = $contentPartners;
    }

    /**
     * @return Collection
     */
    public function getAboutUsContent(): Collection
    {
        return $this->aboutUsContent;
    }

    /**
     * @param Collection $aboutUsContent
     */
    public function setAboutUsContent(Collection $aboutUsContent)
    {
        $this->aboutUsContent = $aboutUsContent;
    }

    /**
     * @return Collection
     */
    public function getContact(): Collection
    {
        return $this->contact;
    }

    /**
     * @param Collection $contact
     */
    public function setContact(Collection $contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return Collection
     */
    public function getPostCode(): Collection
    {
        return $this->postCodes;
    }

    public function getReviews(): Collection
    {
        return $this->reviews;
    }

}
