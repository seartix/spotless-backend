<?php

namespace App\Mail;

use App\Models\EmailSubscibers;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSubscription extends Mailable
{
    use Queueable, SerializesModels;

    private $content;
    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$content)
    {
        $this->content = $content ?? 'GRAZIE PER ESSERTI ISCRITTO ALLA NOSTRA NEWSLETTER!';
        $this->subject = 'Congratulazioni, sei iscritto alla newsletter di SPOTLESS!';
        $user['user'] = User::where('email',$email)->where('subscriber',true)->first();
        if(!$user['user']){
            $user['email_user'] = EmailSubscibers::where('email',$email)->first();
        }
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.default',['content'=>$this->content,'user'=>$this->user]);
    }
}
