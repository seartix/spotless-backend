<?php

namespace App\Mail;

use App\Models\EmailSubscibers;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class SubscribeLetter extends Mailable
{
    use Queueable, SerializesModels;


    private $content;
    private $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($content,$email)
    {
        $this->content = $content;
        $user['user'] = User::where('email',$email)->where('subscriber',true)->first();
        if(!$user['user']){
            $user['email_user'] = EmailSubscibers::where('email',$email)->first();
        }
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.default',['content'=>$this->content,'user'=>$this->user]);
    }
}
