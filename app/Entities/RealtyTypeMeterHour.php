<?php

namespace App\Entities;


class RealtyTypeMeterHour
{
    /**
     * @var string
     */
    private $realty_type;
    /**
     * @var float
     */
    private $price;
    /**
     * @var int
     */
    private $meters;
    /**
     * @var float
     */
    private $hours;

    public function __construct(string $realty_type, float $price, int $meters, float $hours)
    {

        $this->realty_type = $realty_type;
        $this->price = $price;
        $this->meters = $meters;
        $this->hours = $hours;
    }

    /**
     * @return string
     */
    public function getRealtyType(): string
    {
        return $this->realty_type;
    }

    /**
     * @param string $realty_type
     */
    public function setRealtyType(string $realty_type)
    {
        $this->realty_type = $realty_type;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getMeters(): int
    {
        return $this->meters;
    }

    /**
     * @param int $meters
     */
    public function setMeters(int $meters)
    {
        $this->meters = $meters;
    }

    /**
     * @return float
     */
    public function getHours(): float
    {
        return $this->hours;
    }

    /**
     * @param float $hours
     */
    public function setHours(float $hours)
    {
        $this->hours = $hours;
    }
}