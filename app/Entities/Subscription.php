<?php

namespace App\Entities;


use Illuminate\Support\Collection;

class Subscription
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $description;
    /**
     * @var float
     */
    private $price;
    /**
     * @var Collection
     */
    private $adServices;
    /**
     * @var string
     */
    private $type;
    /**
     * @var array
     */
    private $days;

    private $additional_list;
    /**
     * @var bool
     */
    private $buy_clear_kit;

    private $frequency;

      /**
     * Subscription constructor.
     * @param int $id
     * @param string $title
     * @param string $description
     * @param float $price
     * @param Collection $adServices
     * @param string $type
     * @param array $days
     * @param $additional_list
     * @param bool $buy_clear_kit
     */
    public function __construct($id, $title, $description, $price, Collection $adServices, $type, $days, $additional_list, $buy_clear_kit,$frequency)
    {
        $this->id = $id;
        $this->title = $title;
        $this->description = $description;
        $this->price = $price;
        $this->adServices = $adServices;
        $this->type = $type;
        $this->days = $days;
        $this->additional_list = $additional_list;
        $this->buy_clear_kit = $buy_clear_kit;
        $this->frequency = $frequency;
    }

    /**
     * @return mixed
     */
    public function getAdditionalList()
    {
        return $this->additional_list;
    }

    /**
     * @param mixed $additional_list
     */
    public function setAdditionalList($additional_list)
    {
        $this->additional_list = $additional_list;
    }

    /**
     * @return bool
     */
    public function isBuyClearKit()
    {
        return $this->buy_clear_kit;
    }

    /**
     * @param bool $buy_clear_kit
     */
    public function setBuyClearKit($buy_clear_kit)
    {
        $this->buy_clear_kit = $buy_clear_kit;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price)
    {
        $this->price = $price;
    }

    /**
     * @return Collection
     */
    public function getAdServices(): Collection
    {
        return $this->adServices;
    }

    /**
     * @param Collection $adServices
     */
    public function setAdServices(Collection $adServices)
    {
        $this->adServices = $adServices;
    }

    /**
     * @return string
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    /**
     * @return array
     */
    public function getDays(): ?array
    {
        return $this->days;
    }

    /**
     * @param array $days
     */
    public function setDays(array $days)
    {
        $this->days = $days;
    }

    /**
     * @return mixed
     */
    public function getFrequency()
    {
        $rows = explode("\r\n",$this->frequency);
        foreach ($rows as $key => $row){
            $result[$key]['frequency'] = explode("/",$row)[0];
            $result[$key]['price'] = explode("/",$row)[1];
            $result[$key]['count'] = explode("/",$row)[2];
        }
        return $result;
    }

    /**
     * @param mixed $frequency
     */
    public function setFrequency($frequency): void
    {
        $this->frequency = $frequency;
    }
}
