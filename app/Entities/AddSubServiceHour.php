<?php

namespace App\Entities;


class AddSubServiceHour
{
    /**
     * @var int
     */
    private $add_service_id;
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $meters;
    /**
     * @var float
     */
    private $hours;

    /**
     * AddSubServiceHour constructor.
     * @param int $add_service_id
     * @param int $id
     * @param int $meters
     * @param float $hours
     */
    public function __construct($id, $meters, $hours, $add_service_id)
    {
        $this->add_service_id = $add_service_id;
        $this->id = $id;
        $this->meters = $meters;
        $this->hours = $hours;
    }

    /**
     * @return int
     */
    public function getAddServiceId()
    {
        return $this->add_service_id;
    }

    /**
     * @param int $add_service_id
     */
    public function setAddServiceId($add_service_id)
    {
        $this->add_service_id = $add_service_id;
    }



    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }


    /**
     * @return float
     */
    public function getHours(): float
    {
        return $this->hours;
    }

    /**
     * @return int
     */
    public function getMeters()
    {
        return $this->meters;
    }

    /**
     * @param int $meters
     */
    public function setMeters($meters)
    {
        $this->meters = $meters;
    }

    /**
     * @param int $hours
     */
    public function setHours(int $hours)
    {
        $this->hours = $hours;
    }
}