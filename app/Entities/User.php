<?php

namespace App\Entities;

use phpDocumentor\Reflection\Types\Boolean;

class User
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var string
     */
    private $email;
    /**
     * @var string
     */
    private $role;
    /**
     * @var string
     */
    private $password;
    /**
     * @var int
     */
    private $subscription_id;
    /**
     * @var int
     */
    private $insurances;
    /**
     * @var string
     */
    private $type;
    /**
     * @var string
     */
    private $fiscal_code;
    /**
     * @var string
     */
    private $surname;
    /**
     * @var string
     */
    private $gender;
    /**
     * @var string
     */
    private $date_birthday;
    /**
     * @var string
     */
    private $phone;
    /**
     * @var string
     */
    private $address;
    /**
     * @var string
     */
    private $house_number;
    /**
     * @var int
     */
    private $city_id;
    /**
     * @var int
     */
    private $province_id;
    /**
     * @var int
     */
    private $code;
    /**
     * @var string
     */
    private $domofon;
    /**
     * @var string
     */
    private $domofon_text;
    /**
     * @var string
     */
    private $firm_name;
    /**
     * @var string
     */
    private $post_index;
    /**
     * @var int
     */
    private $id;
    /**
     * @var null|string
     */
    private $avatar;
    /**
     * @var null|string
     */
    private $description;

    /**
     * @var null|boolean
     */
    private $subscriber;

    private $facebook;

    private $instagram;

    private $google;
    /**
     * User constructor.
     * @param int $id
     * @param string|null $name
     * @param string $email
     * @param string $role
     * @param string $password
     * @param int|null $subscription_id
     * @param int|null $insurance_id
     * @param string|null $type
     * @param string|null $fiscal_code
     * @param string|null $surname
     * @param string|null $gender
     * @param string|null $date_birthday
     * @param string|null $phone
     * @param string $address
     * @param string|null $house_number
     * @param int $city_id
     * @param int $province_id
     * @param int|null $code
     * @param string|null $domofon
     * @param string|null $domofon_text
     * @param string|null $firm_name
     * @param string|null $post_index
     * @param string|null $avatar
     * @param string|null $description
     */
    public function __construct($id, $name = null, $email, $role,
                                $password, $subscription_id = null, $insurances = null,
                                $type = null, $fiscal_code = null, $surname = null,
                                 $gender = null, $date_birthday = null, $phone = null,
                                $address, $house_number = null, $city_id,
                                $province_id, $code = null, $domofon = null,
                                $domofon_text = null, $firm_name = null,
                                $post_index = null,$subscriber=null, $facebook = null, $instagram = null, $google = null)
    {
        $this->name = $name;
        $this->email = $email;
        $this->role = $role;
        $this->password = $password;
        $this->subscription_id = $subscription_id;
        $this->insurances = $insurances;
        $this->type = $type;
        $this->fiscal_code = $fiscal_code;
        $this->surname = $surname;
        $this->gender = $gender;
        $this->date_birthday = $date_birthday;
        $this->phone = $phone;
        $this->address = $address;
        $this->house_number = $house_number;
        $this->city_id = $city_id;
        $this->province_id = $province_id;
        $this->code = $code;
        $this->domofon = $domofon;
        $this->domofon_text = $domofon_text;
        $this->firm_name = $firm_name;
        $this->post_index = $post_index;
        $this->id = $id;
//        $this->avatar = $avatar;
//        $this->description = $description;
//        $this->available_dates = $available_dates;
        $this->subscriber = $subscriber;
        $this->facebook = $facebook;
        $this->instagram = $instagram;
        $this->google = $google;
    }

    public function getInsurances()
    {
        return $this->insurances;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param string $role
     */
    public function setRole($role)
    {
        $this->role = $role;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return int
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     * @param int $subscription_id
     */
    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;
    }

    /**
     * @return int
     */
    public function getInsuranceId()
    {
        return $this->insurances;
    }

    /**
     * @param int $insurances
     */
    public function setInsurances($insurances)
    {
        $this->insurances = $insurances;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type = null)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getFiscalCode()
    {
        return $this->fiscal_code;
    }

    /**
     * @param string $fiscal_code
     */
    public function setFiscalCode($fiscal_code)
    {
        $this->fiscal_code = $fiscal_code;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param string $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return string
     */
    public function getDateBirthday()
    {
        return $this->date_birthday;
    }

    /**
     * @param string $date_birthday
     */
    public function setDateBirthday($date_birthday)
    {
        $this->date_birthday = $date_birthday;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getHouseNumber()
    {
        return $this->house_number;
    }

    /**
     * @param string $house_number
     */
    public function setHouseNumber($house_number)
    {
        $this->house_number = $house_number;
    }

    /**
     * @return int
     */
    public function getCityId()
    {
        return $this->city_id;
    }

    /**
     * @param int $city_id
     */
    public function setCityId($city_id)
    {
        $this->city_id = $city_id;
    }

    /**
     * @return int
     */
    public function getProvinceId()
    {
        return $this->province_id;
    }

    /**
     * @param int $province_id
     */
    public function setProvinceId($province_id)
    {
        $this->province_id = $province_id;
    }

    /**
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getDomofon()
    {
        return $this->domofon;
    }

    /**
     * @param string $domofon
     */
    public function setDomofon($domofon)
    {
        $this->domofon = $domofon;
    }

    /**
     * @return string
     */
    public function getDomofonText()
    {
        return $this->domofon_text;
    }

    /**
     * @param string $domofon_text
     */
    public function setDomofonText($domofon_text)
    {
        $this->domofon_text = $domofon_text;
    }

    /**
     * @return string
     */
    public function getFirmName()
    {
        return $this->firm_name;
    }

    /**
     * @param string $firm_name
     */
    public function setFirmName($firm_name)
    {
        $this->firm_name = $firm_name;
    }

    /**
     * @return string
     */
    public function getPostIndex()
    {
        return $this->post_index;
    }

    /**
     * @param string $post_index
     */
    public function setPostIndex($post_index)
    {
        $this->post_index = $post_index;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return null|string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param null|string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return null|string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return null
     */
    public function getSubscriber()
    {
        return (bool)$this->subscriber;
    }

    /**
     * @param null $subscriber
     */
    public function setSubscriber($subscriber): void
    {
        $this->subscriber = $subscriber;
    }

    /**
     * @return null
     */
    public function getInstagram()
    {
        return $this->instagram;
    }

    /**
     * @param null $instagram
     */
    public function setInstagram($instagram): void
    {
        $this->instagram = $instagram;
    }

    /**
     * @return null
     */
    public function getGoogle()
    {
        return $this->google;
    }

    /**
     * @param null $google
     */
    public function setGoogle($google): void
    {
        $this->google = $google;
    }

    /**
     * @return null
     */
    public function getFacebook()
    {
        return $this->facebook;
    }

    /**
     * @param null $facebook
     */
    public function setFacebook($facebook): void
    {
        $this->facebook = $facebook;
    }
}
