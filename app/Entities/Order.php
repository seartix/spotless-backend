<?php

namespace App\Entities;

use Carbon\Carbon;

class Order
{
    /**
     * @var bool
     */
    private $buy_clear_kit;
    /**
     * @var int
     */
    private $amount;
    /**
     * @var Carbon
     */
    private $dateTime;
    /**
     * @var string
     */
    private $comment;
    /**
     * @var string
     */
    private $meters;
    /**
     * @var float
     */
    private $hours;
    /**
     * @var int
     */
    private $user_id;
    /**
     * @var int|null
     */
    private $cleaner_id;
    /**
     * @var string
     */
    private $charge_id;
    /**
     * @var string
     */
    private $status;
    /**
     * @var int
     */
    private $id;
    /**
     * @var Carbon
     */
    private $createdAt;
    private $addServices;
    private $subConditions;
    private $surfaces;
    private $insurances;
    private $address;
    private $secret_place;
    private $subscription_id;
    private $is_created_with_subscription;

    /**
     * Order constructor.
     * @param int $id
     * @param bool $buy_clear_kit
     * @param int $amount
     * @param Carbon $dateTime
     * @param string $comment
     * @param float $meters
     * @param float $hours
     * @param int $user_id
     * @param int $cleaner_id|null
     * @param string $charge_id
     * @param string $status
     * @internal param int $insurances
     */
    public function __construct($id, $buy_clear_kit, $amount, $dateTime,
                                $comment, $meters, $hours,
                                $user_id, $cleaner_id , $status,
                                $createdAt, $addServices, $subConditions, $surfaces, $insurances, $address,
                                $city_id, $realty_type_id ,$realty_type, $secret_place = null, $subscription_id = null,
                                $is_created_with_subscription = null
    )
    {
        $this->buy_clear_kit = $buy_clear_kit;
        $this->amount = $amount;
        $this->dateTime = $dateTime;
        $this->comment = $comment;
        $this->meters = $meters;
        $this->hours = $hours;
        $this->user_id = $user_id;
        $this->cleaner_id = $cleaner_id;
        $this->status = $status;
        $this->id = $id;
        $this->createdAt = $createdAt;
        $this->addServices = $addServices;
        $this->subConditions = $subConditions;
        $this->surfaces = $surfaces;
        $this->insurances = $insurances;
        $this->address = $address;
        $this->city_id = $city_id;
        $this->realty_type = $realty_type;
        $this->realty_type_id = $realty_type_id;
        $this->secret_place = $secret_place;
        $this->subscription_id = $subscription_id;
        $this->is_created_with_subscription = $is_created_with_subscription;
    }

    /**
     * @return int|null
     */
    public function getCleanerId()
    {
        return $this->cleaner_id;
    }

    /**
     * @return int
     */
    public function getSecretPlace()
    {
        return $this->secret_place;
    }

    public function getCityId()
    {
        return $this->city_id;
    }

    public function getRealtyTypeId()
    {
        return $this->realty_type_id;
    }
    public function getRealtyType()
    {
        return $this->realty_type;
    }

    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @return Carbon
     */
    public function getCreatedAt(): Carbon
    {
        return $this->createdAt;
    }

    /**
     * @return mixed
     */
    public function getAddServices()
    {
        return $this->addServices;
    }

    /**
     * @return mixed
     */
    public function getSubConditions()
    {
        return $this->subConditions;
    }

    /**
     * @return mixed
     */
    public function getSurfaces()
    {
        return $this->surfaces;
    }

    /**
     * @return mixed
     */
    public function getInsurances()
    {
        return $this->insurances;
    }

    /**
     * @return bool
     */
    public function isBuyClearKit(): bool
    {
        return $this->buy_clear_kit;
    }

    /**
     * @param bool $buy_clear_kit
     */
    public function setBuyClearKit(bool $buy_clear_kit)
    {
        $this->buy_clear_kit = $buy_clear_kit;
    }

    /**
     * @return int
     */
    public function getAmount(): int
    {
        return $this->amount;
    }

    /**
     * @param int $amount
     */
    public function setAmount(int $amount)
    {
        $this->amount = $amount;
    }

    /**
     * @return string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return float
     */
    public function getHours(): float
    {
        return $this->hours;
    }

    /**
     * @param float $hours
     */
    public function setHours(float $hours)
    {
        $this->hours = $hours;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->user_id;
    }

    /**
     * @param int $user_id
     */
    public function setUserId(int $user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return \App\User|null
     */
    public function getCleaner(): ?\App\User
    {
        return \App\User::cleaners()->find($this->cleaner_id);
    }

    /**
     * @param int|null $cleaner_id
     */
    public function setCleanerId(int $cleaner_id = null)
    {
        $this->cleaner_id = $cleaner_id;
    }

    /**
     * @return Carbon
     */
    public function getDateTime(): Carbon
    {
        return $this->dateTime;
    }

    /**
     * @param Carbon $dateTime
     */
    public function setDateTime(Carbon $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function getChargeId(): string
    {
        return $this->charge_id;
    }

    /**
     * @param string $charge_id
     */
    public function setChargeId(string $charge_id)
    {
        $this->charge_id = $charge_id;
    }

    /**
     * @return float
     */
    public function getMeters(): float
    {
        return $this->meters;
    }

    /**
     * @param float $meters
     */
    public function setMeters(float $meters)
    {
        $this->meters = $meters;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * @return null
     */
    public function getSubscriptionId()
    {
        return $this->subscription_id;
    }

    /**
     * @param null $subscription_id
     */
    public function setSubscriptionId($subscription_id)
    {
        $this->subscription_id = $subscription_id;
    }

    /**
     * @return null
     */
    public function getisCreatedWithSubscription()
    {
        return $this->is_created_with_subscription;
    }

    /**
     * @param null $is_created_with_subscription
     */
    public function setIsCreatedWithSubscription($is_created_with_subscription)
    {
        $this->is_created_with_subscription = $is_created_with_subscription;
    }
}