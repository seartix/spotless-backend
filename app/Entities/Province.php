<?php
/**
 * Created by PhpStorm.
 * User: earth
 * Date: 7/18/17
 * Time: 3:56 PM
 */

namespace App\Entities;


class Province
{
    /**
     * @var string
     */
    private $title;
    /**
     * @var int
     */
    private $id;

    public function __construct(int $id, string $title)
    {
        $this->title = $title;
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
}