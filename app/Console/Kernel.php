<?php

namespace App\Console;

use App\Models\ActiveSubscription;
use App\Models\Order;
use App\Notifications\UserOrderCreatedNotification;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Tests\Models\User;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [

    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
       // $schedule->command('email:rate')->dailyAt('09:00');

        $schedule->call(function(){
            $date_time = Carbon::now();
            $orders = Order::query()->where('status','process')->get();
            foreach ($orders as $order){
                if($date_time->diffInDays($order->end_date_time,false) <= 0){
                    $order->status  = 'archived';
                    $order->save();
                }
            }
        })->dailyAt('4:00');

        $schedule->call(function(){
            $subscriptions = ActiveSubscription::query()->where('active',true)->get();
            $date_time = Carbon::now();
            foreach ($subscriptions as $subscription){
                $order = $subscription->orders()->where('status','process')->orderBy('end_date_time','DESC')->first();
                if($date_time->diffInDays($order->end_date_time) < 5){
                    $user = User::find($order->user_id);
                    $user->notify(new UserOrderCreatedNotification($order));
                }
            }
        });
        if (stripos((string)shell_exec('ps xf | grep \'[q]ueue:work\''), 'artisan queue:work') === false) {
            $schedule->command('queue:work --tries=3')->appendOutputTo(storage_path() . '/logs/laravel-' . Carbon::today()->toDateString() . '-cron.log');
        } else {
            $schedule->command('queue:restart')->appendOutputTo(storage_path() . '/logs/laravel-' . Carbon::today()->toDateString() . '-cron.log');
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {


        require base_path('routes/console.php');
    }
}
