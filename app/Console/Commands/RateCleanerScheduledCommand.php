<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Notifications\RateCleanerNotification;
use Carbon\Carbon;
use Illuminate\Console\Command;

class RateCleanerScheduledCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:rate';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send notifications for user who had cleaning yesterday';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $date = Carbon::now()->addDays(-1)->format('Y-m-d');
        Order::query()->whereDate('date_time', $date)->with(['user', 'cleaner'])->get()->each(function (Order $order) {
            if ($order->user) {
                dump($order->user->name);
                if ($order->cleaner) {
                    $order->user->notify(new RateCleanerNotification($order->cleaner->name));
                }
            }
        });

    }
}
