<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\PasswordResetEvent' => [
            'App\Listeners\PasswordResetListener',
        ],
        'App\Events\EmployeeCreatedEvent' => [
            'App\Listeners\EmployeeCreatedListener',
        ],
        'App\Events\OrderCreatedEvent' => [
            'App\Listeners\OrderCreatedListener',
        ],
        'App\Events\OrderUpdatedEvent' => [
            'App\Listeners\OrderUpdatedListener',
        ],
        'App\Events\CleanerChangedEvent' => [
            'App\Listeners\CleanerChangedListener',
        ],
        'App\Events\NewFeedbackEvent' => [
            'App\Listeners\NewFeedbackListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
