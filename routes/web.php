<?php


use App\Notifications\SubscriptionCreatedNotification;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([
], function (Router $router){

    $router->resource('/subscribe-send','SubscribeController');

    $router->get('/schedule', function (\Illuminate\Http\Request $request) {

        if ($request->input('_pjax', false)) {
            return view('schedule_pajax_container', [
                'cleaners' => \App\User::query()->cleaners()->get()
            ]);
        }

        return view('schedule', [
            'cleaners' => \App\User::query()->cleaners()->get()
        ]);
    });
    Route::get('test-mail',function(){
        $user = \App\User::whereId(9)->first();
        $user->notify(new SubscriptionCreatedNotification($user->name ?? 'User', $user->subscription->title ?? ' '));
        return 1;
    });
    Route::post('schedule', 'ScheduleController@create')->name('createSchedule');
    Route::get('/schedule/{id}','ScheduleController@show')->name('getSchedule');


    Route::get('/unsubscribe/{id}','SubscribeController@unsubscribe')->name('unsubscribe');
    Route::get('/unsubscribe-email/{id}','SubscribeController@unsubscribeEmail')->name('unsubscribe-email');
});
