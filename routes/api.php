<?php

use Illuminate\Http\Request;
use Illuminate\Routing\Router;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => ['auth:api']
], function (Router $router){

    $router->get('/user', function (Request $request) {
        return $request->user();
    });

});

Route::group([
   'middleware' => ['api'],
], function (Router $router) {
    $router->get('subservice-hours-meters','Api\AddSubServiceHourController@index');
    $router->get('cities', 'Api\CityController@index');
    $router->get('provinces', 'Api\ProvinceController@index');
    $router->get('realty-types', 'Api\RealtyTypeController@index');
    $router->get('insurances', 'Api\InsuranceController@index');
    $router->post('reservation', 'Api\ReservationController@reservation');
    $router->get('realty-types-price-hours-meters', 'Api\RealtyTypeMeterHourController@index');
    $router->get('subscriptions', 'Api\SubscriptionController@index');
    $router->get('subscribe-user', 'Api\SubscriptionController@createSubscribsion');
    $router->post('subscribe-user-email', 'Api\SubscriptionController@createSubscriptionByEmail');
    $router->get('secret-places', 'Api\SecretPlacesController@index');

    $router->get('get-order-view-model', 'Api\OrderController@getViewModel');

    $router->get('gestpay-response', 'Api\OrderController@response');

    $router->post('get-step-amount', 'Api\OrderController@getStepAmount');

    $router->get('orders-archived', 'Api\OrderController@archived');
    $router->get('orders/subscription', 'Api\OrderController@expiration');
    $router->get('order/subscription-renew', 'Api\OrderController@renewSubscription');
    $router->get('order/subscription-price', 'Api\OrderController@getPriceForRenewSubscription');
    $router->get('order/subscription-delete', 'Api\OrderController@deleteSubscription');
    $router->get('content', 'Api\ContentController@index');

    $router->get('get-price-from-meters', 'Api\RealtyTypePriceHourController@calculatePriceFromMeters');
    $router->get('get-price-from-hours', 'Api\RealtyTypePriceHourController@calculatePriceFromHours');

    $router->post('users', 'Api\UserController@create');
    $router->post('user/forgot', 'Api\UserController@resetPassword');
    $router->post('user/setpassword', 'Api\UserController@setPassword');
    $router->get('choose-cleaners', 'Api\UserController@chooseCleaners');
    $router->get('faq', 'Api\FAQController@faqIndex');
    $router->get('languages', 'Api\LanguageController@index');
    $router->get('all-cleaners', 'Api\UserController@getAllCleaners');
    $router->post('image', 'Api\ImageController@upload');
    $router->post('employer', 'Api\EmployerController@create');
    $router->post('apply-coupon', 'Api\CouponController@apply');
    $router->get('user-cleaners', 'Api\UserController@getUserCleaners');
    $router->get('text-page/{name}', 'Api\TextPageController@getPage');
    $router->get('test', 'Api\TextPageController@test');
    $router->post('send-feedback', 'Api\UserController@sendFeedback');
    $router->post('confirm-employer', 'Api\EmployerController@confirmEmployer');
    $router->get('blog', 'Api\BlogController@index');
    $router->get('article/{id}', 'Api\BlogController@getArticle');
    $router->post('article/comment', 'Api\BlogController@setComment');
    $router->post('miscalculation', 'Api\UserController@sendMiscalculation');
    $router->get('get-checkbox-sevices', 'Api\OrderController@getCheckboxServices');

    $router->get('get-facebook-id', 'Api\UserController@authUserByFacebook');
    $router->get('get-google-id', 'Api\UserController@authUserByGoogle');
    $router->get('instagram', 'Api\InstagramController@callbackInstagram');

});
Route::group([
    'middleware' => ['auth:api']
], function (Router $router) {
    //$router->get('available-dates-for-cleaner', 'Api\UserController@getAvailableDatesForCleaner');
   // $router->get('available-times-for-cleaner', 'Api\UserController@getAvailableTimeForCleaner');
    $router->get('user', 'Api\UserController@show');
    $router->get('cleaners', 'Api\UserController@allCleaners');
    $router->post('orders', 'Api\OrderController@store');
    $router->put('orders', 'Api\OrderController@update');
    $router->post('create-order', 'Api\OrderController@store');
    $router->get('check-gestpay-order', 'Api\OrderController@checkGestOrder');
    $router->post('gestpay', 'Api\OrderController@gestpay');
    $router->post('save-order', 'Api\OrderController@saveOrder');
    $router->post('change-status', 'Api\OrderController@changeStatus');
    $router->post('accept-order', 'Api\OrderController@acceptOrderAction');
    $router->get('orders-by-status', 'Api\OrderController@getOrdersByStatus');
    $router->post('change-order/{order_id}', 'Api\OrderController@postChangeOrder');
    $router->post('change-order/{order_id}/gestpay', 'Api\OrderController@postChangeOrderGestPay');
    $router->post('eval-order/{order_id}', 'Api\OrderController@postEvalOrder');
    $router->post('copy-order/{order_id}', 'Api\OrderController@postCopyOrder');
    $router->get('copy-order-accept/{order_id}', 'Api\OrderController@copyOrderAccept');
    $router->get('gestpay-copy-order/{order_id}', 'Api\OrderController@copyOrderGestPay');
    $router->delete('delete-order/{order_id}', 'Api\OrderController@deleteOrder');
    $router->get('orders', 'Api\OrderController@index');
    $router->post('review', 'Api\UserController@createReview');
    $router->get('reviews', 'Api\UserController@getCleanerReviews');
    $router->post('user-address', 'Api\UserController@postUserAddress');
    $router->post('user-info', 'Api\UserController@postUserInfo');
    $router->get('invoice/{id}', 'Api\OrderController@invoice');
    $router->delete('cancel-subscription', 'Api\UserController@cancelSubscription');

    /**SOCIALS
     *
     */
    $router->get('set-facebook-id', 'Api\UserController@setFacebookId');
    $router->get('set-instagram-id', 'Api\UserController@setInstagramId');
    $router->get('set-google-id', 'Api\UserController@setGoogleId');
});

Route::get('login', ['as' => 'login', 'uses' => 'Auth\LoginController@login']);
Route::get('available-times-for-cleaner', 'Api\UserController@getAvailableTimeForCleaner');
Route::get('available-dates-for-cleaner', 'Api\UserController@getAvailableDatesForCleaner');
