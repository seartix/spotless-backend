Start queue notification on background: <br>
nohup php /var/www/spotless/backend/artisan queue:listen --tries=3 > /dev/null 2>&1 &
php /var/www/spotless/backend/artisan schedule:run >> /dev/null 2>&1
supervisorctl restart all