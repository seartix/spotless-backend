<?php

use App\Models\AddSubServiceHour;
use App\Models\AddSubService;
use Illuminate\Database\Seeder;

class AddSubServiceHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AddSubServiceHour::class, 3)
            ->create();
    }
}
