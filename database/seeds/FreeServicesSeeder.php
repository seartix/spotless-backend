<?php

use App\Models\FreeService;
use Illuminate\Database\Seeder;

class FreeServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FreeService::create([
            'title' => 'Lavaggio piatti',
            'text' => 'some description',
            'icon' => 'image/service-icon1.png'
        ]);

        FreeService::create([
            'title' => 'Pulizia piastre e griglie piccole',
            'text' => 'some description',
            'icon' => 'image/service-icon4.png'
        ]);

        FreeService::create([
            'title' => 'Pulizia forno a microonde',
            'text' => 'some description',
            'icon' => 'image/service-icon2.png'
        ]);

        FreeService::create([
            'title' => 'Pulizia e lucidatura piano cottura ',
            'text' => 'some description',
            'icon' => 'image/service-icon5.png'
        ]);
        FreeService::create([
            'title' => 'Pulizia macchinetta del cafe',
            'text' => 'some description',
            'icon' => 'image/service-icon3.png'
        ]);

        FreeService::create([
            'title' => 'Pulizia esterna armadi della cucina',
            'text' => 'some description',
            'icon' => 'image/service-icon6.png'
        ]);
    }
}
