<?php

use App\Models\BasicSet;
use Illuminate\Database\Seeder;

class BasicSetsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BasicSet::create([
            'title' => 'detersivi',
            'setsText' => File::get(storage_path('app/public/detersivisets.txt')),
            'icon' => 'image/clean-icon1.png'
        ]);

        BasicSet::create([
            'title' => 'panni',
            'sets' => File::get(storage_path('app/public/pannisets.txt')),
            'icon' => 'image/clean-icon4.png'
        ]);

        BasicSet::create([
            'title' => 'aspirapolvere',
            'setsText' => '',
            'icon' => 'image/clean-icon2.png'
        ]);

        BasicSet::create([
            'title' => 'per la polvere',
            'setsText' => File::get(storage_path('app/public/perlapolveresets.txt')),
            'icon' => 'image/clean-icon5.png'
        ]);

        BasicSet::create([
            'title' => 'per il pavimento',
            'setsText' => File::get(storage_path('app/public/perilpavimentosets.txt')),
            'icon' => 'image/clean-icon3.png'
        ]);

        BasicSet::create([
            'title' => 'spugna',
            'setsText' => '',
            'icon' => 'image/clean-icon6.png'
        ]);
    }
}
