<?php

use App\Models\SubCondition;
use Illuminate\Database\Seeder;

class ConditionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Condition::class, 3)
            ->create()
            ->each(function ($as) {
                $as->subConditions()->saveMany(factory(SubCondition::class, 3)->make());
            });
    }
}
