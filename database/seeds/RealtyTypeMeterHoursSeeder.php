<?php

use App\Models\RealtyTypeMeterHour;
use Illuminate\Database\Seeder;

class RealtyTypeMeterHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(RealtyTypeMeterHour::class, 3)
            ->create();
    }
}
