<?php

use App\Models\RealtyType;
use Illuminate\Database\Seeder;

class RealtyTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        RealtyType::create([
            'title' => 'Casa',
            'price' => 20
        ]);

        RealtyType::create([
            'title' => 'Uffico',
            'price' => 30
        ]);
    }
}
