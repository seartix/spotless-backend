<?php

use Illuminate\Database\Seeder;
use App\Models\AddService;
use App\Models\AddSubService;

class AddServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AddService::class, 3)
            ->create()
            ->each(function ($as) {
                $as->addSubServices()->saveMany(factory(AddSubService::class, 3)->make());
            });
    }
}
