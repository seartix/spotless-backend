<?php

use App\Models\AddService;
use App\Models\Subscription;
use Illuminate\Database\Seeder;

class SubscriptionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Subscription::class, 3)
            ->create()
            ->each(function ($as) {
                $as->addServices()->saveMany(factory(AddService::class, 3)->make());
            });
    }
}
