<?php

use App\Models\AvailableDateForCleaner;
use Illuminate\Database\Seeder;

class AvailableDateForCleanersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(AvailableDateForCleaner::class, 3)
            ->create();
    }
}
