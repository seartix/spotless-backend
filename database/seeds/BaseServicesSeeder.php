<?php

use App\Models\BaseService;
use Illuminate\Database\Seeder;

class BaseServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BaseService::create([
            'title' => 'cucina',
            'text' => 'some description',
            'image' => 'image/kitchen.png'
        ]);

        BaseService::create([
            'title' => 'bagno',
            'text' => 'some description',
            'image' => 'image/bath.png'
        ]);

        BaseService::create([
            'title' => 'camera da letto',
            'text' => 'some description',
            'image' => 'image/bedroom.png'
        ]);

        BaseService::create([
            'title' => 'soggiorno & altre camere',
            'text' => 'some description',
            'image' => 'image/livingroom.png'
        ]);
    }
}
