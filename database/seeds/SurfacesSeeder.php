<?php

use App\Models\Surface;
use Illuminate\Database\Seeder;

class SurfacesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Surface::create([
           'title' => 'legno'
        ]);
        Surface::create([
            'title' => 'tappeti'
        ]);
        Surface::create([
            'title' => 'marmo'
        ]);
        Surface::create([
            'title' => 'argenteria'
        ]);
    }
}
