<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AddSubServiceHoursSeeder::class);
        $this->call(RealtyTypesSeeder::class);
        $this->call(SubscriptionsSeeder::class);
        $this->call(BasicSetsSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SurfacesSeeder::class);
        $this->call(InsurancesSeeder::class);
        $this->call(FreeServicesSeeder::class);
        $this->call(BaseServicesSeeder::class);
        $this->call(AddServicesSeeder::class);
        $this->call(ConditionsSeeder::class);
        $this->call(CitiesSeeder::class);
        $this->call(AvailableDateForCleanersSeeder::class);
    }
}
