<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubConditionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_conditions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('condition_id')->index();
            $table->string('title')->unique();
            $table->timestamps();

            $table->foreign('condition_id')
                ->references('id')->on('conditions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_conditions');
    }
}
