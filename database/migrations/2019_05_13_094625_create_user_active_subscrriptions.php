<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserActiveSubscrriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_active_subscriptions', function (Blueprint $table) {

            $table->increments('id');
            $table->unsignedInteger('count_clean');
            $table->unsignedInteger('user_id');
            $table->float('tarif');
            $table->float('price');
            $table->boolean('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_active_subscriptions');
    }
}
