<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('buy_clear_kit');
            $table->double('amount');
            $table->unsignedInteger('insurance_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->dateTime('date_time');
            $table->text('comment');
            $table->double('meters');
            $table->double('hours');
            $table->timestamps();

            $table->foreign('insurance_id')
                ->references('id')->on('insurances')
                ->onDelete('restrict');

            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
