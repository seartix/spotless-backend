<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionAddServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscription_add_service', function (Blueprint $table) {
            $table->unsignedInteger('subscription_id');
            $table->unsignedInteger('add_service_id');

            $table->primary([
                'subscription_id',
                'add_service_id',
            ]);

            $table->foreign('subscription_id')
                ->references('id')->on('subscriptions')
                ->onDelete('cascade');

            $table->foreign('add_service_id')
                ->references('id')->on('add_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscription_add_service');
    }
}
