<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaymentOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('token');
            $table->dropColumn('type');
        });

        Schema::create('payment_orders', function (Blueprint $table) {
           $table->increments('id');
           $table->unsignedInteger('order_id')->index();
           $table->enum('type', [
               'cards',
               'paypal'
           ]);
           $table->string('token');

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
