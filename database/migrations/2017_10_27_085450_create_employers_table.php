<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('email');
            $table->string('phone');
            $table->string('fiscale_code');
            $table->enum('gender', ['maschio', 'femmina', 'non importante']);
            $table->integer('iva_code');
            $table->integer('skill');
            $table->string('cv_path');
            $table->string('reference_path');
            $table->string('identificate_path');
            $table->string('avatar_path');
            $table->string('add_info');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employers');
    }
}
