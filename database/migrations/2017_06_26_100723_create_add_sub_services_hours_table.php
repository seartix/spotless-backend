<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddSubServicesHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_sub_services_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('add_sub_service_id');
            $table->integer('count');
            $table->integer('hours');
            $table->timestamps();

            $table->foreign('add_sub_service_id')
                ->references('id')->on('add_sub_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_sub_services_hours');
    }
}
