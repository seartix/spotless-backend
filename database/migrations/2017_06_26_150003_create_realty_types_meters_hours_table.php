<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRealtyTypesMetersHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('realty_types_meters_hours', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('realty_type_id');
            $table->double('meters')->index();
            $table->double('hours')->index();
            $table->timestamps();

            $table->foreign('realty_type_id')
                ->references('id')->on('realty_types')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('realty_types_meters_hours');
    }
}
