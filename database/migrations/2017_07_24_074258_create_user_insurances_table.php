<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInsurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_insurance', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('insurance_id');

            $table->primary([
                'user_id',
                'insurance_id',
            ], 'uipk');

            $table->foreign('user_id', 'ufkds')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('insurance_id', 'ifkds')
                ->references('id')->on('insurances')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_insurance');
    }
}
