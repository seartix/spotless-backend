<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CleanersReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cleaners_review', function (Blueprint $table) {
            $table->increments('id');
            $table->string('text');
            $table->unsignedInteger('cleaner_id');
            $table->unsignedInteger('user_id');

            $table->foreign('cleaner_id', 'cufku')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->foreign('user_id', 'uufku')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
