<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAvailableDateForCleanersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('available_date_for_cleaners', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('date');
            $table->time('time_start');
            $table->time('time_end');
            $table->unsignedInteger('cleaner_id');

            $table->foreign('cleaner_id')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('available_date_for_cleaners');
    }
}
