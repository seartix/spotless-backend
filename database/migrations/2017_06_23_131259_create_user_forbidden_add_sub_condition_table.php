<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserForbiddenAddSubConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_forbidden_add_sub_condition', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('forbidden_add_sub_condition_id');

            $table->primary([
                'user_id',
                'forbidden_add_sub_condition_id'
            ], 'pksc');

            $table->foreign('user_id', 'ufksc')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('forbidden_add_sub_condition_id', 'afksc')
                ->references('id')->on('sub_conditions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_forbidden_add_sub_condition');
    }
}
