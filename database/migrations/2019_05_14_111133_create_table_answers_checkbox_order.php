<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnswersCheckboxOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_answer_checkbox', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('answer_checkbox_id');

            $table->primary([
                'order_id',
                'answer_checkbox_id',
            ]);

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('answer_checkbox_id')
                ->references('id')->on('answers_checkbox_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_answer_checkbox');
    }
}
