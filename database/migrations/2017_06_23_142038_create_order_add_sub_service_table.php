<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderAddSubServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_add_sub_service', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('add_sub_service_id');

            $table->primary([
                'order_id',
                'add_sub_service_id',
            ]);

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('add_sub_service_id')
                ->references('id')->on('add_sub_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_add_sub_service');
    }
}
