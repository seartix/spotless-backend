<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAddSubServiceIdInAddSubServiceHours extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('add_sub_services_hours', function (Blueprint $table) {
            $table->renameColumn('add_sub_service_id', 'add_service_id')->nullable();
            $table->renameColumn('count', 'meters')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
