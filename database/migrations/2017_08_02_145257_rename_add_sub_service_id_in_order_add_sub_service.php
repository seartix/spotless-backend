<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameAddSubServiceIdInOrderAddSubService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_add_sub_service', function (Blueprint $table) {
            $table->dropForeign(['add_sub_service_id']);
            $table->renameColumn('add_sub_service_id', 'add_service_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
