<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAddSubServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_sub_services', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('add_service_id')->index();
            $table->string('title')->unique()->index();
            $table->double('price');
            $table->timestamps();

            $table->foreign('add_service_id')
                ->references('id')->on('add_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_sub_services');
    }
}
