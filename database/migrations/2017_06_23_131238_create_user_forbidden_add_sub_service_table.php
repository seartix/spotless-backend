<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserForbiddenAddSubServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_forbidden_add_sub_service', function (Blueprint $table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('forbidden_add_sub_service_id');

            $table->primary([
                'user_id',
                'forbidden_add_sub_service_id',
            ], 'pkss');

            $table->foreign('user_id', 'ufkss')
                ->references('id')->on('users')
                ->onDelete('cascade');

            $table->foreign('forbidden_add_sub_service_id', 'afkss')
                ->references('id')->on('add_sub_services')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_forbidden_add_sub_service');
    }
}
