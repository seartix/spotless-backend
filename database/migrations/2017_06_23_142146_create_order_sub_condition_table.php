<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderSubConditionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_sub_condition', function (Blueprint $table) {
            $table->unsignedInteger('order_id');
            $table->unsignedInteger('sub_condition_id');

            $table->primary([
                'order_id',
                'sub_condition_id',
            ]);

            $table->foreign('order_id')
                ->references('id')->on('orders')
                ->onDelete('cascade');

            $table->foreign('sub_condition_id')
                ->references('id')->on('sub_conditions')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_sub_condition');
    }
}
