<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('type', ['persona', 'physical'])->index();
            $table->string('fiscal_code', 16)->nullable();
            $table->string('surname')->nullable();
            $table->enum('gender', ['male', 'female', 'not important']);
            $table->date('date_birthday')->nullable();
            $table->string('phone')->nullable();
            $table->string('address');
            $table->string('house_number')->nullable();
            $table->integer('city_id');
            $table->integer('province_id');
            $table->integer('code')->nullable();
            $table->string('domofon', 50)->nullable();
            $table->string('domofon_text', 255)->nullable();

            $table->string('firm_name')->nullable();
            $table->integer('post_index')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::table('users', function (Blueprint $table) {
//            $table->removeColumn('role');
//        });
    }
}
