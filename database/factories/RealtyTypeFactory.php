<?php

$factory->define(App\Models\RealtyType::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->words(3, true),
        'price' => $faker->randomNumber()
    ];

});