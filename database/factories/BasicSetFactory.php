<?php

$factory->define(App\Models\BasicSet::class, function (Faker\Generator $faker) {

    return [
        'title' => str_random(),
        'sets' => $faker->words(3),
        'icon' => $faker->image()
    ];

});
