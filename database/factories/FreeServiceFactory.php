<?php

$factory->define(App\Models\FreeService::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->words(3, true),
        'text' => $faker->text,
        'icon' => $faker->image()
    ];

});
