<?php

$factory->define(App\Models\Subscription::class, function (Faker\Generator $faker) {

    return [
        'title' => str_random(),
        'price' => $faker->randomNumber(),
        'description' => $faker->text(100)
    ];

});
