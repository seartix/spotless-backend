<?php

$factory->define(App\Models\Insurance::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->words(2, true),
        'description' => $faker->text(),
        'text' => $faker->text(),
        'price' => $faker->randomNumber(),
    ];


});
