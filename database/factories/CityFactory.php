<?php

$factory->define(App\Models\City::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->unique()->city(),
    ];

});
