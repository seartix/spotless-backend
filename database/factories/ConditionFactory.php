<?php

$factory->define(App\Models\Condition::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->unique()->text(10),
        'icon' => $faker->image()
    ];

});
