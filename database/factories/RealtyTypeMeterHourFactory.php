<?php

$factory->define(App\Models\RealtyTypeMeterHour::class, function (Faker\Generator $faker) {

    return [
        'meters' => $faker->numberBetween(1, 100),
        'hours' => $faker->randomNumber(),
        'realty_type_id' => factory(\App\Models\RealtyType::class)->create()->id,
    ];

});