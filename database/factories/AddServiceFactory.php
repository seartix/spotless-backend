<?php

$factory->define(App\Models\AddService::class, function (Faker\Generator $faker) {

    return [
        'title' => str_random(10),
        'help' => $faker->word,
        'icon' => $faker->image()
    ];

});
