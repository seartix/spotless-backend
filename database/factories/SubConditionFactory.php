<?php

$factory->define(App\Models\SubCondition::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->unique()->text(10),
    ];

});
