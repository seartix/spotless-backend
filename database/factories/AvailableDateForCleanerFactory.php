<?php

$factory->define(App\Models\AvailableDateForCleaner::class, function (Faker\Generator $faker) {

    return [
        'date' => $faker->date(),
        'time_start' => $faker->time(),
        'time_end' => $faker->time(),
        'cleaner_id' => factory(\App\User::class)->create()->id,
    ];

});
