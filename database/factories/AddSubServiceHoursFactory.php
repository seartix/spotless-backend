<?php

$factory->define(App\Models\AddSubServiceHour::class, function (Faker\Generator $faker) {

    return [
        'meters' => $faker->numberBetween(1, 10),
        'hours' => $faker->randomNumber(),
        'add_sub_service_id' => factory(\App\Models\AddSubService::class)->create()->id,
    ];

});
