<?php

$factory->define(App\Models\BaseService::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->words(2, true),
        'text' => $faker->text,
        'image' => $faker->image()
    ];

});
