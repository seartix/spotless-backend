<?php

$factory->define(App\Models\Surface::class, function (Faker\Generator $faker) {

    return [
        'title' => $faker->words(3, true)
    ];

});