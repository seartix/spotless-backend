<?php

$factory->define(App\Models\AddSubService::class, function (Faker\Generator $faker) {

    return [
        'title' => str_random(),
        'price' => $faker->randomNumber(),
        'add_service_id' => factory(\App\Models\AddService::class)->create()->id,
    ];

});
