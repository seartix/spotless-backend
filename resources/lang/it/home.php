<?php

return [
    'order' => 'Ordini',
    'reviews' => 'Recensioni addetti',
    'base_services' => 'Servizi inclusi',
    'add_services' => 'Servizi extra',
    'add_services_house' => 'Servizi extra case',
    'add_services_office' => 'Servizi extra business',
    'add_services_hours' => 'Ore per i servizi extra',
    'free_services' => 'Servizi gratuiti',
    'surfaces' => 'Superficie',
    'conditions' => 'Condizioni (Bambini, anziani, animali)',
    'insurance' => 'Assicurazione',
    'users' => 'Utenti',
    'date' => 'Calendario addetti',
    'cities' => 'Città',
    'provinces' => 'Provincie',
    'basic_sets' => 'Kit pulizia',
    'subscriptions' => 'Abbonamenti',
    'realty_type' => 'Tipo di ambiente da pulire',
    'realty_type_meters' => 'Prezzo per quadratura',
    'step' => 'Passo',
    'additional' => 'Homepage vantaggi',
    'button' => 'Homepage pulsante',
    'text_content' => 'Contenuto di testo',
    'key' => 'Chiave',
    'style' => 'Stile',
    'value' => 'Valore',
    'partners' => 'Perché diventare un partner',
    'about' => 'Chi siamo',
    'contact' => 'Contatti',
    'places' => 'Domanda area commento',
    'employers' => 'Candidature',
    'cleaners' => 'Addetti di pulizia',
    'user_languages' => 'Lingue di addett',
    'faq_categories' => 'Categorie FAQ',
    'faq_question' => 'Domande FAQ',
    'coupons' => 'Buoni sconto',

    'orders' => 'Ordini',
    'status' => 'Stato',
    'realty type' => 'Tipo di ambiente da pulire',
    'clear_datetime' => 'Data e ora di pulizia',
    'created At' => 'Creato il',
    'meters' => 'Metri',
    'hours' => 'Ore',
    'amount' => 'Quantità',
    'add_sub_conditions' => 'Condizioni aggiuntive',
    'clear_kit' => 'Kit pulizia',
    'user' => 'Utente',
    'cleaner' => 'Addetto di pulizia',
    'comment' => 'Commento',

    'process' => 'in corso',
    'empty' => 'vuoto',
    'not_selected' => 'non selezionato',


    'satus' => 'Stato',
    'accepted' => 'accettato',
    'canceled' => 'cancellato',
    'successed' => 'completato',
    'archived' => 'archiviato',

    'text' => 'Testo',
    'show' => 'Mostrare',


    'title' => 'Titolo',
    'Title' => 'Titolo',
    'Realty type' => 'Tipo di ambiente da pulire',
    'clean type'=>' Tipo di pulizia',
    'wait for pay'=>'Ordine non completato',

    'some_description' => 'Descrizione',
    'image' => 'Immagine',
    'browse' => 'Scegli',


    'price' => 'Prezzo',
    'hours' => 'Ore',

    'icon' => 'Icona',

    'add_service_id' => 'Servizio extra id',


    'sub_conditions' => 'Condizioni aggiuntive',

    'description' => 'Descrizione',

    'Price' => 'Prezzo',
    'time_start' => 'Ora di inizio',
    'time_end' => 'Ora di  fine',
    'date_start' => 'Data di inizio',
    'date_end' => 'Data di  fine',
    'sets_text' => 'Descrizione',
    'link' => 'Collegamento',
    'name' => 'Nome',
    'password' => 'parola d\'ordine',
    'surname' => 'Cognome',
    'gender' => 'Sesso',
    'date_birthday' => 'Data di nascita',

    'role' => 'Ruolo',
    'subscription' => 'Abbonamento',
    'type' => 'Tipo',
    'fiscal_code' => 'Codice fiscale',
    'male' => 'Maschio',
    'female' => 'Femmina',
    'phone' => 'Numero di cellulare',
    'address' => 'Indirizzo',
    'house_number' => 'Numero civico',
    'city' => 'Città',
    'procince_id' => 'Provincia id',
    'code' => 'CAP',
    'recommend' => 'Raccomandato',
    'domofon' => 'Citofono',
    'domofon_text' => 'Commento per citofono',
    'can_clean_home' => 'Può pulire la casa',
    'can_clean_office' => 'Può pulire l\'ufficio',
    'can_iron_clothes' => 'Può stirare',
    'firm_name' => 'Nome dell\'azienda',
    'post_index' => 'Etichetta',
    'percent' => 'Per cento',
    'count_using' => 'Quante persone possono usarlo?',
    'start_price' => 'Prezzo da',
    'iva_code' => 'Codice iva',
    'skill' => 'Anni d\'esperienza',
    'answer' => 'Risposta',
    'coupon_code' => 'coupon',
    'surface' => 'Superficie',
    'avatar' => 'Avatar',
    'rating' => 'Valutazione',
    'cv_path' => 'CARICA CURRICULUM',
    'reference_path' => 'CARICA REFERENZE',
    'identificate_path' => 'CARICA DOCUMENTO D\'IDENTITA',
    'avatar_path' => 'CARICA FOTO SU SFONDO BIANCO',
    'add_info' => 'ALTRE INFORMAZIONI',
    'express_price' => 'Prezzo espresso',
    'buy_clear_kit_in_sub' => 'Cancellare kit in abbonamento',
    'forbidden_services' => 'Servizi extra che addetto non può fare',
    'forbidden_condition' => 'Condizioni in cui addetto non può lavorare',
    'w8pay' => 'ordine non completato',
    'post-code'=>'Codice Postale',
    'articles'=>'Articoli',
    'category'=>'Categoria di articoli',
    'reviews-site'=>'Recensioni sito',
    'comments'=>'Commentare',
    'miscalculation'=>'Errore di Calcolo',
    'subscribe'=>'Sottoscrivi il contenuto',
    'checkbox-services'=>'Servizi Checkbox',
    'answers-checkbox-services'=>'Servizi checkbox delle risposte',
    'clean_type_meters'=>'Contatori di tipo pulito',
    'clean_types'=>'Tipi puliti',
    'label_clean_type'=>'Etichetta',

    'Checkbox Services'=>'Servizi Checkbox',
    'Answer Checkbox Services'=>'Servizi Checkbox Delle Risposte',
    'Article'=>'Articolo',
    'Comments'=>'Commentare',
    'Miscalculations'=>'Errori di calcolo',
    'Clean Type'=>'Tipo Pulizia',
    'Article category'=>'Articolo categoria',
    'Post codes'=>'Codici postali',
    'Not selected'=>'Non selezionato',
    'Clean type'=>'Tipo Pulizia',
    'clean-type'=>'Tipo Pulizia',
    'Description for "i"'=>'Designazione delle merci "i" ',
    'question'=>'Domanda',
    'add_price'=>'Prezzo aggiunto',
    'About Us'=>'Su Di Noi',
    'Additional contents'=>'Contenuti aggiuntivi',
    'Add Service'=>'Aggiungi Servizi',
    'Add Service Hour'=>'Aggiungi Ora Di Servizio',
    'Available Date For Cleaners'=>'Data Disponibile Per I Detergenti',
    'Base Service'=>'Servizio Base',
    'Basic Set'=>'Insieme Base',
    'Button Content'=>'Contenuto Pulsante',
    'Cleaner Reviews'=>'Recensioni Più Pulite',
    'Contact Us'=>'Contattare',
    'Free Service'=>'Servizio Gratuito',
    'Dashboard'=>'Cruscotto',
    'Insurance'=>'Assicurazione',
    'Language'=>'Lingua',
    'content'=>'Contenuto',
    'short text'=>'Testo breve',
    'updated_at'=>'Aggiornato a',
    'media'=>'Aggiornato a',
    'on main page'=>'Sulla pagina principale',
    'stars'=>'Stella',
    'email'=>'Email',
    'approved'=>'Approvato',
    'district'=>'Quartiere',
    'zip_code'=>'CAP',
    'Send emails'=>'Invia email',
    'subscribe-content'=>'Sottoscrivi il contenuto',
    'subtitle'=>'Sottotitolo',
    'frequency'=>'Frequenza',


];
