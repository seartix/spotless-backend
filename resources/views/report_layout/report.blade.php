<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Spotless</title>

    <link rel="stylesheet" href="{{ asset('css/fonts.css') }}">
    <link rel="stylesheet" href="{{ asset('css/head-foot.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('css/more-styles.css') }}">
    <!--CSS-->
    <style>
        .invoice-box {
            max-width: 960px;
            margin: auto;
            padding: 30px;
            border: 1px solid #eee;
            box-shadow: 0 0 10px rgba(0, 0, 0, .15);
            font-size: 16px;
            line-height: 24px;
            /*font-family: FiraSans-Regular;*/
            color: #555;
        }

        .invoice-box table {
            width: 100%;
            line-height: inherit;
            text-align: left;
        }

        .invoice-box table td {
            padding: 5px;
            vertical-align: top;
        }

        .invoice-box table tr td:last-child {
            text-align: right;
        }

        .invoice-box table tr.top {
            background: #02447e;
            color: #fff;
            backface-visibility: visible;
        }

        .invoice-box table tr.top img {
            margin-bottom: 30px;
        }

        .invoice-box table tr.top table td {
            padding-bottom: 20px;
        }

        .invoice-box table tr.information table td {
            padding-bottom: 40px;
        }

        .invoice-box table tr.heading {
            padding-top: 30px;
        }

        .invoice-box table tr.heading~tr td {
            padding: 10px;
        }

        .invoice-box table tr.heading td {
            background: #eee;
            font-weight: bold;
            text-align: right;
            vertical-align: bottom;
            padding: 10px;
            height: 75px;
        }

        .invoice-box table tr.heading td:first-of-type {
            text-align: left;
        }

        .invoice-box table tr.details td {
            padding-bottom: 20px;
        }


        .invoice-box table tr.item {
            text-align: right;
        }

        .invoice-box table tr.item td:first-of-type {
            text-align: left;
        }

        .invoice-box table tr.item.last td {
            border-bottom: none;
        }

        .invoice-box table tr.subtotal td {
            text-align: right;
        }

        .invoice-box table tr.total td:first-of-type {
            font-weight: bold;
            text-align: right;
        }

        .invoice-footer {
            background: #02447e;
            backface-visibility: visible;
            color: #fff;
            margin-top: 60px;
            padding-left: 15px;
            padding-right: 15px;
        }


        .invoice-footer-item {
            padding: 25px;
        }

        .invoice-footer-img {
            margin-top: 20px;
        }
        @media print {
            .invoice-footer {
                display: none;
                backface-visibility: visible;
                margin-top: 60px;
            }
            .invoice-box table tr.top {
                backface-visibility: visible;
            }
            .print_button{
                display: none;
                visibility: hidden;
            }
            .heading > td {
                font-size: 14px;
            }
            @page { margin: 0; }
            body { margin: 1.6cm; }
        }
        @media (max-width: 767px) {
            .invoice-footer-item {
                text-align: center;
            }
        }

        @media only screen and (max-width: 575px) {
            .invoice-box table tr.top table td {
                width: 100%;
                display: block;
                text-align: center;
            }

            .invoice-box table tr.information table td {
                width: 100%;
                display: block;
                text-align: center;
            }
        }
        /** RTL **/

        .rtl {
            direction: rtl;
            font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        }

        .rtl table {
            text-align: right;
        }

        .rtl table tr td:nth-child(2) {
            text-align: left;
        }
    </style>
    <!--FONTS-->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" />
</head>
<body>
<div class="invoice-box">
    <button onclick="window.print()" class="btn btn-success print_button">Stampa</button>
    <br>
    <br>
    <table>
        <tr class="top">
            <td colspan="@if($user->type  == 'persona')6 @else 4 @endif">
                <table>
                    <tr>
                        <td>
                            <img src="{{ asset('images/Spotless3.png') }}" alt="logo">
                            <br> Ricevuta nr. {{ $id }}/ {{ $year }}
                            <br>
                            <br>SANY GROUP S.r.l.
                            <br> P.IVA: 09441590966
                            <br> Viale Monte Ceneri 36
                            <br> 20155 Milano (MI)
                            <br> ITALIA
                            <br>
                            <br> Data: {{ $created_at }}
                        </td>
                        <td>
                            Spettabile
                            <br> {{ $name }}
                            @if($user->fiscal_code)
                                @if($user->type == 'azienda')
                                    <br> P.IVA: {{ $user->fiscal_code }}
                                @else
                                    <br> CF: {{ $user->fiscal_code }}
                                @endif
                            @endif
                            <br>{{ $user->address }}&nbsp;{{ $user->house_number }}
                            <br> {{ $user->post_index }} {{ $city }}
                            <br> ITALIA
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="heading">
            <td>
                Descrizione
            </td>
            @if( $user->type  == 'persona' )
                {{--<td>--}}
                    {{--Guadagno del portale--}}
                {{--</td>--}}
                {{--<td>--}}
                    {{--Pulizia--}}
                {{--</td>--}}
            @endif
            <td>
                Costo del servizio
            </td>
            <td>
                IVA 22%
            </td>
            <td>
                Importo totale
            </td>
        </tr>
        <tr class="item">
            <td.>
                Servizio di pulizia
            </td.>
            @if( $user->type  == 'persona' )
                {{--<td>€ {{ number_format($clener_profit, 2) }}</td>--}}
                {{--<td>€ {{ number_format($spot_profit, 2) }}</td>--}}
            @endif
            <td>
                € {{ number_format($netto_price, 2) }}
            </td>
            <td>
                € {{ number_format($IVA_val, 2) }}
            </td>
            <td>
                € {{ number_format($full_price, 2) }}
            </td>
        </tr>
        <tr class="subtotal">
            <td colspan="3">Imponibile</td>
            <td>
                € {{ number_format($netto_price, 2) }}
            </td>
        </tr>
        <tr class="subtotal">
            <td colspan="3">IVA 22% su € {{ number_format($netto_price, 2) }}</td>
            <td>
                € {{ number_format($IVA_val, 2) }}
            </td>
        </tr>
        <tr class="total">
            <td colspan="3">Totale:</td>
            <td>
                € {{ number_format($full_price, 2) }}
            </td>
        </tr>
    </table>
    <div class="invoice-footer">
        <div class="row">
            <div class="col-xs-6 invoice-footer-item">
                SANY GROUP S.r.l.
                <br> P.IVA: 09441590966
                <br> Viale Monte Ceneri 36
                <br> 20155 Milano (MI)
                <br> ITALIA
            </div>
            <div class="col-xs-6 invoice-footer-item invoice-footer-img">
                <img src="{{ asset('images/Spotless3.png') }}" alt="logo" class="img-responsive pull-right">
            </div>
        </div>
    </div>
    <div class="text-center">
        Documento non valido ai fini fiscali
    </div>
</div>
</body>

</html>
