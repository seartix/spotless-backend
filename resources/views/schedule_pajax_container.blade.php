<br>
<form action="/schedule" method="post">
    {{ csrf_field() }}
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-2">
                <div class="form-group">
                    <label for="">Seleziona addetto alle pulizie</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <select  id="schedule" name="choosenCleaner" class="form-control">
                        <option value=""></option>
                        @foreach($cleaners as $cleaner)
                            <option value="{{ $cleaner->id }}">
                                {{ $cleaner->name }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <style>
            div.row.hide-ajax{
                display: none ;
            }
            #loader{
                display: block;
                margin: 0 auto;
                width: 50px;
            }
            #loader img {
                width: 50px;
            }
            .hide-ajax label{
                margin-left: 10px;
            }
        </style>
        <div id="loader" style="display: none;">
            <img src="{{ asset('images/loader.svg') }}" alt="logo">
        </div>
        <?php generateDays() ?>

        <div class="row hide-ajax" >
            <div class="col-md-2 col-md-offset-3 ">
                <div class="form-group">
                    <input type="checkbox" value="1" class="" id="checkAllDays">
                    <label for="" >Tutti i giorni</label>
                </div>
            </div>


        </div>

        <div class="row hide-ajax">
            <div class="col-md-2 col-md-offset-3 ">
                <div class="form-group">
                    <input type="hidden" value="0" name="allTimes">
                    <input type="checkbox" value="1" class="" name="allTimes">
                    <label for="" >24 ore</label>
                </div>
            </div>


        </div>
        <div class="row">
            <div class="col-md-offset-3 col-md-2">
                <button type="submit" class="btn btn-primary">
                    Creare
                </button>
            </div>
        </div>
    </div>
</form>

<script
        src="https://code.jquery.com/jquery-3.3.1.min.js"
        integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
<script>
    $("#checkAllDays").change(function () {
        $("input:checkbox").not('[name=allTimes]').prop('checked', $(this).prop("checked"));
    });

    $("input[type=checkbox][name=allTimes]").change(function () {
        if ($(this).prop('checked')) {
            $('select[data-start]').val('06:00');
            $('select[data-end]').val('23:00');
        } else {
            $('select[data-start]').val('06:00');
            $('select[data-end]').val('06:00');
        }
    })
    $('#schedule').change(function(){
        var europeDays = [
            6,0,1,2,3,4,5
        ]
        $('.hide-ajax').hide();
        $("input:checkbox").not('[name=allTimes]').attr('checked', false);
        $('select[data-start]').val('06:00');
        $('select[data-end]').val('06:00');
        $("#checkAllDays").prop('checked', false);
        $('input[name="allTimes"]').prop('checked', false);
        $.ajax({
            url:'{{route('getSchedule','')}}' +'/'+   $(this).val(),
            method:'GET',
            beforeSend: function(){

                $('#loader').css('display','block');

            },
            success: function(data) {
                var times = 0
                $.each(data,function(key,value){
                $('.day-' + europeDays[key]).attr('checked',true)
                    if(value['start'] == '06:00' && value['end']=='23:00'){
                        times++
                    }
                    $('select[name="from['+ europeDays[key] +']"] > option[value = "'+ value['start'] +'"]').attr('selected',true)
                    $('select[name="to['+ europeDays[key] +']"] > option[value = "'+ value['end'] +'"]').attr('selected',true)
                })
                if(Object.keys(data).length == 7){
                    $("#checkAllDays").prop('checked', true);
                }
                if(times ==7){
                    $('input[name="allTimes"]').prop('checked', true);

                }
                $('#loader').css('display','none');
                $('.hide-ajax').show();
            }
        });
    })
</script>

<br>

<?php
function generateDays() {
    $days = [
        0 => 'Lunedi',
        1 => 'Martedi',
        2 => 'Mercoledi',
        3 => 'Giovedi',
        4 => 'Venerdi',
        5 => 'Sabato',
        6 => 'Domenica',
    ];

    foreach ($days as $index => $name) {
        generateDay($name, $index);
    }
}
?>

<?php function generateDay($name, $index) { ?>
    <div class="row hide-ajax">
        <div class="col-md-offset-3 col-md-2">
            <div class="form-group">
                <input type="hidden" value="0" name="days[{{ $index }}]">
                <input type="checkbox" value="1" class="day-{{$index}}" name="days[{{ $index }}]">
                <label for="">{{ $name }}</label>
            </div>
        </div>

        <div class="col-md-2">
            <div class="form-group">
                <select data-start class="form-control" name="from[{{ $index }}]">
                    <?php for ($i = 0; $i < 35; $i++) { ?>
                        <?php $current = \Carbon\Carbon::createFromTime(6, 0, 0)->addMinutes(30 * $i); ?>
                        <option value="{{ $current->format('H:i') }}">
                            {{ $current->format('H:i') }}
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <select data-end class="form-control" name="to[{{ $index }}]">
                    <?php for ($i = 0; $i < 35; $i++) { ?>
                        <?php $current = \Carbon\Carbon::createFromTime(6, 0, 0)->addMinutes(30 * $i); ?>
                        <option value="{{ $current->format('H:i') }}">
                            {{ $current->format('H:i') }}
                        </option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>
<?php } ?>
